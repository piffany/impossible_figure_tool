package cubeManip;

import java.util.Vector;

import util.Face;
import util.FaceArray;
import util.LayeredFace;

public class CubeSwapper {

	private FaceArray faces;
	
	public CubeSwapper(FaceArray faces) {
		this.faces = faces;
	}

	public boolean containsParadoxWithNeighbours(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			for(int i=0; i<neighbours.length; i++) {
				if(neighbours[i].getTop()!=null && containsParadox(ci, neighbours[i].getTop().getCubeIndex())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean containsParadoxWithNeighbours(Vector<CubeIndex> cis) {
		for(int i=0; i<cis.size(); i++) {
			if(containsParadoxWithNeighbours(cis.get(i))) return true;
		}
		return false;
	}
	
	public boolean containsParadox(CubeIndex ci1, CubeIndex ci2) {
		if(faces.isValidIndex(ci1) && faces.isValidIndex(ci2)) {
			LayeredFace[] shared = faces.getSharedFaces(ci1, ci2);
			if(shared.length==2) {
				Face face1 = shared[0].getTop(), face2 = shared[1].getTop();
				int contains1 = 0, contains2 = 0;
				contains1 += face1.belongsTo(ci1)?1:0;
				contains1 += face2.belongsTo(ci1)?1:0;
				contains2 += face1.belongsTo(ci2)?1:0;
				contains2 += face2.belongsTo(ci2)?1:0;
				return contains1==1 && contains2==1; 
			}
			else return false;
		}
		return false;
	}
	
	private void naiveHide(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			// make sure the shared faces are all visible
			for(int i=0; i<neighbours.length; i++) {
				neighbours[i].setHidden(ci, true);
			}
			faces.updateEdgesAroundCube(ci, false);
		}
	}
	
	public void hide(CubeIndex ci) {
		FaceArray facesCopy = faces.clone();
		if(faces.isValidIndex(ci)) {
			naiveHide(ci);
			if(containsParadoxWithNeighbours(ci)) {
				faces.replaceFaces(facesCopy);
				System.out.println("did not hide due to paradox");
			}
		}
	}
	
	private void naiveBringToFront(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			// make sure the shared faces are all visible
			for(int i=0; i<neighbours.length; i++) {
				neighbours[i].setAsTop(ci);
				neighbours[i].setHidden(ci, false);
			}
			faces.updateEdgesAroundCube(ci, false);
		}
	}
	
	public void bringToFront(CubeIndex ci) {
		FaceArray facesCopy = faces.clone();
		if(faces.isValidIndex(ci)) {
			naiveBringToFront(ci);
			if(containsParadoxWithNeighbours(ci)) {
				faces.replaceFaces(facesCopy);
				System.out.println("did not bring to front back due to paradox");
			}
		}
	}
	
	private void naiveSendToBack(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			// make sure the shared faces are all visible
			for(int i=0; i<neighbours.length; i++) {
				neighbours[i].setAsBottom(ci);
				neighbours[i].setHidden(ci, false);
			}
			faces.updateEdgesAroundCube(ci, false);
		}
	}
	
	public void sendToBack(CubeIndex ci) {
		FaceArray facesCopy = faces.clone();
		if(faces.isValidIndex(ci)) {
			naiveSendToBack(ci);
			if(containsParadoxWithNeighbours(ci)) {
				faces.replaceFaces(facesCopy);
				System.out.println("did not send to back due to paradox");
			}
		}
	}
	
	private void naivePlaceBehind(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			// make sure the shared faces are all visible
			for(int i=0; i<neighbours.length; i++) {
				neighbours[i].setAsSecondToTop(ci);
				neighbours[i].setHidden(ci, false);
			}
			faces.updateEdgesAroundCube(ci, false);
		}
	}
	
	public void placeBehind(CubeIndex ci) {
		FaceArray facesCopy = faces.clone();
		if(faces.isValidIndex(ci)) {
			naivePlaceBehind(ci);
			if(containsParadoxWithNeighbours(ci)) {
				faces.replaceFaces(facesCopy);
				System.out.println("did not place behind due to paradox");
			}
		}
	}

	private void naiveSwap(CubeIndex ci1, CubeIndex ci2) {
		if(faces.isValidIndex(ci1) && faces.isValidIndex(ci2)) {
			LayeredFace[] shared = faces.getSharedFaces(ci1, ci2);
			// make sure the shared faces are all visible
			for(int i=0; i<shared.length; i++) {
				Face face = shared[i].getTop();
				// both faces must belong to either ci1 or ci2
				if(face==null) return;
				if(!face.belongsTo(ci1) && !face.belongsTo(ci2)) return;
			}
			for(int i=0; i<shared.length; i++) {
				LayeredFace layeredFace = shared[i];
				int ind1 = layeredFace.getFirstFaceBelongingToIndex(ci1);
				int ind2 = layeredFace.getFirstFaceBelongingToIndex(ci2);
				if(ind1!=-1 && ind2!=-1) {
					layeredFace.swapFaceOrder(ind1, ind2);
				}
			}
			faces.updateEdgesAroundCube(ci1, false);
			faces.updateEdgesAroundCube(ci2, false);
		}
	}
	
	public void swap(CubeIndex ci1, CubeIndex ci2) {
		FaceArray facesCopy = faces.clone();
		naiveSwap( ci1, ci2);
		if(containsParadoxWithNeighbours(ci1) ||
				containsParadoxWithNeighbours(ci2)) {
			//naiveSwap(cubeData, ci1, ci2);
			faces.replaceFaces(facesCopy);
			System.out.println("swapped back due to paradox");
		}
	}

	private void naiveBringFaceForward(CubeIndex ci) {
		if(faces.isValidIndex(ci)) {
			LayeredFace[] neighbours = faces.getAllNeighbouringFaces(ci);
			// make sure the shared faces are all visible
			for(int i=0; i<neighbours.length; i++) {
				neighbours[i].setAsTop(ci);
				neighbours[i].setHidden(ci, false);
			}
			faces.updateEdgesAroundCube(ci, true);
		}
	}
	
	public void bringFaceForward(CubeIndex ci) {
		FaceArray facesCopy = faces.clone();
		if(faces.isValidIndex(ci)) {
			naiveBringFaceForward(ci);
			if(containsParadoxWithNeighbours(ci)) {
				faces.replaceFaces(facesCopy);
				System.out.println("did not bring to front back due to paradox");
			}
		}
	}

	
}
