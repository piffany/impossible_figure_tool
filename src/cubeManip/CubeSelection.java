package cubeManip;

import cubeManip.CubeData;
import util.FaceArray;

public class CubeSelection {

	private int numSelected;
	private int maxSelectable;
	private CubeIndex lastSelected;
	private CubeIndex lastUnderCursor;
	private FaceArray faces;
	private boolean[][][] selected;
	//private boolean[][][] underCursor;
	private int maxX, maxY, maxZ;
	public static int DIR_NONE = 0, DIR_X = 1, DIR_Y = 2, DIR_Z = 3;
	private int lastSelectedDir = DIR_NONE;

	public CubeSelection(FaceArray faces, int maxSelectable, int maxX, int maxY, int maxZ) {
		this.faces = faces;
		this.maxSelectable = maxSelectable;
		numSelected = 0;
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
		selected = new boolean[maxX][maxY][maxZ];
		//underCursor = new boolean[maxX][maxY][maxZ];
	}
	
	public boolean isValidIndex(CubeIndex ci) {
		if(ci!=null) {
			int x = ci.getX();
			int y = ci.getY();
			int z = ci.getZ();
			return x>=0 && x<maxX && y>=0 && y<maxY && z>=0 && z<maxZ;
		}
		return false;
	}
	
	public CubeIndex[] getFirstNSelected(int n) {
		int m = Math.min(n, numSelected);
		CubeIndex[] firstM = new CubeIndex[m];
		int found = 0;
		if(found >= m) return firstM;
		for(int i=0; i<maxX; i++) {
			for(int j=0; j<maxY; j++) {
				for(int k=0; k<maxZ; k++) {
					if((i==0 || j==0 || k==0) && selected[i][j][k]) {
						CubeIndex ci = new CubeIndex(i,j,k);
						firstM[found] = ci.clone();
						found++;
						if(found >= m) return firstM;
					}
				}
			}
		}
		return firstM;
	}
	
	public CubeIndex[] getAllSelected() {
		return getFirstNSelected(numSelected);
	}
	
	public CubeIndex getUnderCursor() {
		return lastUnderCursor;
	}
	
	public int getNumSelected() {
		return numSelected;
	}
	
	public void setMaxSelectable(int maxSelectable) {
		this.maxSelectable = Math.max(maxSelectable, -1);
		deselectAll();
	}
	
	public void setSelected(CubeIndex ci, boolean isSelected) {
		if(isValidIndex(ci) && (isSelected(ci) != isSelected)) {
			toggleSelection(ci);
		}
	}
	
	public void setUnderCursor(CubeIndex ci, boolean isUnderCursor) {
		if(lastUnderCursor!=null) faces.setUnderCursor(lastUnderCursor, false);
		if(isUnderCursor) {
			lastUnderCursor = ci.clone();
			faces.setUnderCursor(ci, true);
		}
		else {
			lastUnderCursor = null;
		}
	}
	
	public boolean isSelected(CubeIndex ci) {
		return (isValidIndex(ci) && selected[ci.getX()][ci.getY()][ci.getZ()]);
	}
	
	public boolean isUnderCursor(CubeIndex ci) {
		return (isValidIndex(ci) && lastUnderCursor!=null && ci.equals(lastUnderCursor));
	}
	
	public void deselectAll() {
		for(int i=0; i<maxX; i++) {
			for(int j=0; j<maxY; j++) {
				for(int k=0; k<maxZ; k++) {
					if((i==0 || j==0 || k==0)) {
						setSelected(new CubeIndex(i,j,k), false);
					}
				}
			}
		}
		numSelected = 0;
		lastSelected = null;
	}
	
	public void deselectAllUnderCursor() {
		for(int i=0; i<maxX; i++) {
			for(int j=0; j<maxY; j++) {
				for(int k=0; k<maxZ; k++) {
					if((i==0 || j==0 || k==0)) {
						setUnderCursor(new CubeIndex(i,j,k), false);
					}
				}
			}
		}
		lastUnderCursor = null;
	}
	
	public void toggleSelection(CubeIndex picked) {
		// unrestricted multiple selection
		if(maxSelectable == -1) {
			basicToggle(picked);
			if(isSelected(picked)) lastSelected = picked.clone();
			else if(numSelected==0) lastSelected = null;
		}
		// no selection allowed
		else if(maxSelectable == 0) {
			
		}
		// single selection; new selection does not require deselecting old ones
		else if (maxSelectable == 1) {
			basicToggle(picked);
			if(isSelected(picked)) {
				setSelected(lastSelected, false);
				lastSelected = picked.clone();
			}
			else if(numSelected==0) lastSelected = null;
			if(isSelected(picked)) {
				if(numSelected > maxSelectable) {
					faces.setSelected(lastSelected, false);
				}
				lastSelected = picked.clone();
			}
			if(numSelected==0) lastSelected = null;
		}
		// multiple selection; new selection requires deselecting old ones
		else {
			int newNumSelected = numSelected + ((isSelected(picked)) ? -1 : 1);
			if(newNumSelected >= 0 && newNumSelected <= maxSelectable) {
				basicToggle(picked);
				if(isSelected(picked)) lastSelected = picked.clone();
			}
		}
	}
		
	private void printAllSelected() {
		System.out.println("numSelected = " + numSelected);
		CubeIndex[] allSelected = getAllSelected();
		for(int i=0; i<allSelected.length; i++) {
			String s = (allSelected[i]==null) ? "null" : allSelected[i].toString();
			System.out.println(i + " : " + s);
		}
	}
	
	// ignores single or multiple selection rules
	public void basicToggle(CubeIndex ci) {
		if(!isValidIndex(ci)) return;
		boolean wasSelected = faces.isSelected(ci);
		faces.toggleSelection(ci);
		//boolean isSelected = faces.isSelected(ci);

		selected[ci.getX()][ci.getY()][ci.getZ()] = !wasSelected;
		numSelected += wasSelected ? -1 : 1;
	}
	
	/*
	public void moveSelectedCubes(int dir, boolean positive) {
		int[] dataDims = data.size();
		int maxX = dataDims[0], maxY = dataDims[1], maxZ = dataDims[2];
		boolean[][][] moved = new boolean[maxX][maxY][maxZ];
		for(int i=0; i<maxX; i++) {
			for(int j=0; j<maxY; j++) {
				for(int k=0; k<maxZ; k++) {
					CubeIndex ijk = new CubeIndex(i,j,k);
					if(!moved[i][j][k] && data.isSelected(ijk)) {
						boolean isLastSelected = false;
						if(lastSelected!=null) {
							isLastSelected = 	(i==lastSelected.getX()) && 
												(j==lastSelected.getY()) &&
												(k==lastSelected.getZ());
						}
						CubeIndex index = new CubeIndex(i,j,k);
						CubeIndex newDims = CubeMover.moveCube(data, index, dir, positive, true);
						moved[newDims.getX()][newDims.getY()][newDims.getZ()] = true;
						if(isLastSelected) lastSelected = data.get(newDims);
					}
				}
			}
		}
	}
	*/

	public int getMaxSelectable() {
		return maxSelectable;
	}

	public int getLastSelectedDir() {
		return lastSelectedDir;
	}

	public void setLastSelectedDir(int lastSelectedDir) {
		this.lastSelectedDir = lastSelectedDir;
	}

}
