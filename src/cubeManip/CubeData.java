package cubeManip;

import java.util.Vector;

import util.FaceArray;

public class CubeData {

	private int maxX, maxY, maxZ;
	private FaceArray faces;
	private CubeSelection selection;
	private int maxSelectable;
	private CubeSwapper swapper;
	
	public CubeData(int maxX, int maxY, int maxZ, int maxSelectable, boolean hidden) {
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
		this.maxSelectable = maxSelectable;
		faces = new FaceArray(maxX, maxY, maxZ, hidden);
		selection = new CubeSelection(faces, maxSelectable, maxX, maxY, maxZ);
		swapper = new CubeSwapper(faces);
	}

	public FaceArray getFaces() {
		return faces;
	}
	
	public void replaceFaces(FaceArray faces) {
		this.faces.replaceFaces(faces);
	}
	
	public int getNumSelected() {
		return selection.getNumSelected();
	}
	
	public int getMaxSelectable() {
		return maxSelectable;
	}
	
	public void setMaxSelectable(int max) {
		maxSelectable = max;
		selection.setMaxSelectable(max);
	}

	public void toggleSelection(CubeIndex ci) {
		selection.toggleSelection(ci);
	}
	
	public CubeIndex[] getFirstNSelected(int n) {
		return selection.getFirstNSelected(n);
	}
	
	public CubeIndex[] getAllSelected() {
		return selection.getAllSelected();
	}
	
	public CubeIndex getUnderCursor() {
		return selection.getUnderCursor();
	}
	
	public void setSelected(CubeIndex ci, boolean toSelect) {
		selection.setSelected(ci, toSelect);
	}
	
	public void setUnderCursor(CubeIndex ci, boolean toSelect) {
		selection.setUnderCursor(ci, toSelect);
	}

	public boolean isSelected(CubeIndex ci) {
		return selection.isSelected(ci);
	}
	
	public boolean isUnderCursor(CubeIndex ci) {
		return selection.isUnderCursor(ci);
	}

	public boolean isValidIndex(CubeIndex ci) {
		if(ci!=null) {
			int x = ci.getX();
			int y = ci.getY();
			int z = ci.getZ();
			return x>=0 && x<maxX && y>=0 && y<maxY && z>=0 && z<maxZ;
		}
		return false;
	}
	
	public Vector<CubeIndex> getNeighbouringCubeIndices(CubeIndex ci) {
		Vector<CubeIndex> vec = new Vector<CubeIndex>();
		if(isValidIndex(ci.getPosXNeighbour())) vec.add(ci.getPosXNeighbour());
		if(isValidIndex(ci.getNegXNeighbour())) vec.add(ci.getNegXNeighbour());
		if(isValidIndex(ci.getPosYNeighbour())) vec.add(ci.getPosYNeighbour());
		if(isValidIndex(ci.getNegYNeighbour())) vec.add(ci.getNegYNeighbour());
		if(isValidIndex(ci.getPosZNeighbour())) vec.add(ci.getPosZNeighbour());
		if(isValidIndex(ci.getNegZNeighbour())) vec.add(ci.getNegZNeighbour());
		return vec;
	}
	
	public boolean areNeighbours(CubeIndex ci1, CubeIndex ci2) {
		return faces.areNeighbours(ci1, ci2);
	}

	public void deselectAll() {
		selection.deselectAll();
	}

	public void deselectAllUnderCursor() {
		selection.deselectAllUnderCursor();
	}

	// swapper
	
	public void hide(CubeIndex ci) {
		swapper.hide(ci);
		setSelected(ci, false);
	}
	
	public void bringToFront(CubeIndex ci) {
		swapper.bringToFront(ci);
		setSelected(ci, false);
	}
		
	public void placeBehind(CubeIndex ci) {
		swapper.placeBehind(ci);
		setSelected(ci, false);
	}

	public void sendToBack(CubeIndex ci) {
		swapper.sendToBack(ci);
		setSelected(ci, false);
	}
	
	public void swap(CubeIndex ci1, CubeIndex ci2) {
		swapper.swap(ci1, ci2);
		setSelected(ci1, false);
		setSelected(ci2, false);
	}

	public void bringFaceForward(CubeIndex ci) {
		swapper.bringFaceForward(ci);
		setSelected(ci, false);
	}
}
