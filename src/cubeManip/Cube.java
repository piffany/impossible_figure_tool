package cubeManip;


public class Cube implements Comparable<Cube> {

	private CubeIndex index;
	private boolean selected = false;
	
	public Cube(CubeIndex index) {
		this.setIndex(index.clone());
	}
	
	public Cube(Cube other) {
		setIndex(other.getIndex());
		setSelected(other.isSelected());
	}
	
	public Cube clone() {
		return new Cube(this);
	}

	public CubeIndex getIndex() {
		return index.clone();
	}

	public int getX() {
		return index.getX();
	}
	
	public int getY() {
		return index.getY();
	}

	public int getZ() {
		return index.getZ();
	}

	public void setIndex(CubeIndex index) {
		this.index = index.clone();
	}

	@Override
	public int compareTo(Cube o) {
		return getIndex().compareTo(o.getIndex());
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void toggleSelection() {
		setSelected(!isSelected());
	}
}
