package cubeManip;

import java.util.Random;

import util.Face;
import util.MyMath;

public class CubeIndex implements Comparable<CubeIndex> {

	private int x = 0, y = 0, z = 0;
	private static Random random = new Random(); 
	
	public CubeIndex(int x, int y, int z) {
		setX(x);
		setY(y);
		setZ(z);
	}
	
	public CubeIndex(CubeIndex other) {
		setX(other.getX());
		setY(other.getY());
		setZ(other.getZ());
	}
	
	public CubeIndex(int[] dims) {
		setX(dims[0]);
		setY(dims[1]);
		setZ(dims[2]);
	}

	public boolean matches(int[] index) {
		return (index[0]==x && index[1]==y && index[2]==z);
	}
	
	public CubeIndex clone() {
		return new CubeIndex(this);
	}
	
	// inclusive
	public static CubeIndex getRandom(int min, int max) {
		int rx = min + random.nextInt(max-min+1);
		int ry = min + random.nextInt(max-min+1);
		int rz = min + random.nextInt(max-min+1);
		return new CubeIndex(rx, ry, rz);
	}
	
	public static CubeIndex getRandom(int xmin, int xmax, int ymin, int ymax, int zmin, int zmax) {
		int rx = xmin + random.nextInt(xmax-xmin+1);
		int ry = ymin + random.nextInt(ymax-ymin+1);
		int rz = zmin + random.nextInt(zmax-zmin+1);
		return new CubeIndex(rx, ry, rz);
	}
	
	@Override
	// < means current index drawn first
	public int compareTo(CubeIndex o) {
		int dx = MyMath.sgn(x-o.getX());
		int dy = MyMath.sgn(y-o.getY());
		int dz = MyMath.sgn(z-o.getZ());
		if(dx!=0) return dx;
		else if(dy!=0) return dy;
		else return dz;
		/*
		int x2 = o.getX();
		int y2 = o.getY();
		int z2 = o.getZ();
		if(z > z2) return 1;
		else if(z < z2) return -1;
		else {
			int xy = x+y;
			int xy2 = x2+y2;
			if(xy > xy2) return 1;
			else if(xy < xy2) return -1;
			else return 0;
		}
		*/
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public String toString() {
		String s = "(" + getX() + ", " + getY() + ", " + getZ() + ")";
		return s;
	}
	
	public boolean equals(CubeIndex other) {
		return getX()==other.getX() && getY()==other.getY() && getZ()==other.getZ();
	}
	
	public int[] getArray() {
		int[] dims = {getX(), getY(), getZ()};
		return dims;
	}
	
	public void addToX(int val) {
		x+=val;
	}
	
	public void addToY(int val) {
		y+=val;
	}
	
	public void addToZ(int val) {
		z+=val;
	}

	public CubeIndex getPosXNeighbour() {
		if(y==0 || z==0) return new CubeIndex(x+1, y, z);
		else return new CubeIndex(x, y-1, z-1);
	}
	
	public CubeIndex getPosYNeighbour() {
		if(x==0 || z==0) return new CubeIndex(x, y+1, z);
		else return new CubeIndex(x-1, y, z-1);
	}

	public CubeIndex getPosZNeighbour() {
		if(x==0 || y==0) return new CubeIndex(x, y, z+1);
		else return new CubeIndex(x-1, y-1, z);
	}
	
	public CubeIndex getNegXNeighbour() {
		if(x==0) return new CubeIndex(x, y+1, z+1);
		else return new CubeIndex(x-1, y, z);
	}

	public CubeIndex getNegYNeighbour() {
		if(y==0) return new CubeIndex(x+1, y, z+1);
		else return new CubeIndex(x, y-1, z);
	}

	public CubeIndex getNegZNeighbour() {
		if(z==0) return new CubeIndex(x+1, y+1, z);
		else return new CubeIndex(x, y, z-1);
	}
}
