package colours;


import java.awt.Color;
import java.util.Random;

@SuppressWarnings("serial")
public class Colour extends Color {
	
	public static final Colour CORNFLOWER_BLUE = new Colour(100, 149, 237,255);
	public static final Colour TRANSPARENT = new Colour(255, 255, 255, 0);
	public static final Colour ROYAL_BLUE = new Colour(79, 128, 255, 255);
	public static final Colour SEMI_TRANSPARENT = new Colour(0, 255, 0, 100);
	public static final Colour FAKE_TRANSPARENT = new Colour(23,68,21);

	public static final Colour WHITE = new Colour(255, 255, 255, 255);
	public static final Colour BLACK = new Colour(0, 0, 0, 255);
	public static final Colour RED = new Colour(255, 0, 0, 255);
	public static final Colour GREEN = new Colour(0, 255, 0, 255);
	public static final Colour BLUE = new Colour(0, 0, 255, 255);

	
	public Colour(int r, int g, int b) {
		super(r, g, b);
	}

	public Colour(int r, int g, int b, int a) {
		super(r, g, b, a);
	}
		
	public Colour(Color c) {
		super(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
	}
	
	public Colour clone() {
		return new Colour(getRed(), getGreen(), getBlue(), getAlpha());
	}
	
	public static Colour rgbInt2Color(int rgba) {
		Colour c = new Colour(getRed(rgba), getGreen(rgba), getBlue(rgba), getAlpha(rgba));
		return c;
	}
	
	public static int getRed(int rgba) {
		int red = (rgba & 0x00ff0000) >> 16;
		return red;
	}
	
	public static int getGreen(int rgba) {
		int green = (rgba & 0x0000ff00) >> 8;
		return green;
	}

	public static int getBlue(int rgba) {
		int blue = rgba & 0x000000ff;
		return blue;
	}

	public static int getAlpha(int rgba) {
		int alpha = (rgba>>24) & 0xff;
		return alpha;
	}

	public static Colour getSolidColor(Color c) {
		return new Colour(c.getRed(), c.getGreen(), c.getBlue());
	}
	
	public static Colour changeRed(Color c, int red) {
		return new Colour(red, c.getGreen(), c.getBlue(), c.getAlpha());
	}
	
	public static int changeRed(int rgb, int red) {
		Colour oldColor = Colour.rgbInt2Color(rgb);
		Colour newColor = Colour.changeRed(oldColor, red);
		return newColor.getRGB();
	}
	
	public static Colour changeGreen(Color c, int green) {
		return new Colour(c.getRed(), green, c.getBlue(), c.getAlpha());
	}

	public static int changeGreen(int rgb, int green) {
		Colour oldColor = Colour.rgbInt2Color(rgb);
		Colour newColor = Colour.changeGreen(oldColor, green);
		return newColor.getRGB();
	}

	public static Colour changeBlue(Color c, int blue) {
		return new Colour(c.getRed(), c.getGreen(), blue, c.getAlpha());
	}
	
	public static int changeBlue(int rgb, int blue) {
		Colour oldColor = Colour.rgbInt2Color(rgb);
		Colour newColor = Colour.changeBlue(oldColor, blue);
		return newColor.getRGB();
	}

	public static Colour changeAlpha(Color c, int alpha) {
		return new Colour(c.getRed(), c.getGreen(), c.getBlue(), alpha);
	}

	public static int changeAlpha(int rgb, int alpha) {
		Colour oldColor = Colour.rgbInt2Color(rgb);
		Colour newColor = Colour.changeAlpha(oldColor, alpha);
		return newColor.getRGB();
	}
	
	public static int color2rgbInt(Color c) {
		return c.getRGB();
	}
	
	public static Colour RGB2Color(double[] RGB) {
		return new Colour((int) RGB[0], (int) RGB[1], (int) RGB[2]);
	}

	public static double[] rgbInt2RGB(int rgba) {
		double[] RGB = {getRed(rgba), getGreen(rgba), getBlue(rgba)};
		return RGB;
	}
	
	public static int RGB2rgbInt(double[] RGB) {
		Colour c = new Colour((int) RGB[0], (int) RGB[1], (int) RGB[2]);
		int rgb = color2rgbInt(c);
		return rgb;
	}

	public static Color getRandomColour(int seed) {
		Random random = new Random(seed);
		Colour c = new Colour(random.nextInt(255), random.nextInt(255), random.nextInt(255));
		return c;
	}
	
	public static Color getRandomColour(int a, int b, int c) {
		return getRandomColour(255*255*a + 255*b + c);
	}

	public static Color getOppositeColour(Color c) {
		return new Colour((128+c.getRed())%255, (128+c.getGreen())%255, (128+c.getBlue())%255);
	}

	public static int getIntensity(Color c) {
		return (c.getRed()+c.getGreen()+c.getBlue())/3;
	}
}
