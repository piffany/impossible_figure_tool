package colours;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Vector;

public class MyMath {

	public static final AffineTransform identityTransform = 
			AffineTransform.getTranslateInstance(0, 0); 

	public static double round(double d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(d);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public static int sgn(double val) {
		return (val < 0) ? -1 : (val == 0) ? 0 : 1;
	}

	// inclusive
	public static boolean inRange(double val, double a, double b) {
		return (b-val)*(val-a) >= 0;
	}

	public static int gcd(int a, int b) {
		return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).intValue();
	}

	public static int floor(double val) {
		return (int) Math.floor(val);
	}
	
	public static int ceil(double val) {
		return (int) Math.ceil(val);
	}
	
	public static double getEuclideanDistance(double[] p1, double[] p2) {
		assert(p1.length == p2.length);
		double sum = 0;
		for(int i=0; i<p1.length; i++) {
			sum += Math.pow(p1[i]-p2[i], 2);
		}
		sum = Math.sqrt(sum);
		return sum;
	}
	
	public static double getNorm(double[] p1) {
		double sum = 0;
		for(int i=0; i<p1.length; i++) {
			sum += Math.pow(p1[i], 2);
		}
		sum = Math.sqrt(sum);
		return sum;
	}
	
	// return positive remainder
	public static double rem(double a, double b) {
		double q = Math.floor(a/b);
		double r = a-q*b;
		if(r<0) r+=a;
		return a;
	}

	// roots of ax^2 + bx + c = 0
	public static Vector<Double> getQuadraticRoots(double a, double b, double c) {
		Vector<Double> roots = new Vector<Double>();
		// if a==0, x = -c/b
		if(a==0) {
			roots.add(-c/b);
		}
		// if a!=0
		else {
			double discrim = b*b-4*a*c;
			// one real root
			if(discrim == 0) {
				roots.add(-b/(2*a));
			}
			// two real roots
			else if(discrim > 0) {
				roots.add((-b-Math.sqrt(discrim))/(2*a));
				roots.add((-b+Math.sqrt(discrim))/(2*a));
			}
		}
		return roots;

	}

	// invert affineTransform
	public static AffineTransform getInverse(AffineTransform at) {
		AffineTransform invAt = new AffineTransform(at);
		try {
			invAt.invert();
		} catch (NoninvertibleTransformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invAt;
	}

	public static double rescaleToInterval(double val, double a, double b) {
		assert(a != b);
		return (val-a)/(b-a);
	}
	
	public static int getClosestMatch(double val, double[] vals) {
		assert(vals.length > 0);
		double minDiff = Math.abs(val - vals[0]);
		int minInd = 0;
		for(int i=1; i<vals.length; i++) {
			double diff = Math.abs(val - vals[i]);
			if(diff < minDiff) {
				minDiff = diff;
				minInd = i;
			}
		}
		return minInd;
	}
	
	public static int getClosestMatch(int val, int[] vals) {
		assert(vals.length > 0);
		double minDiff = Math.abs(val - vals[0]);
		int minInd = 0;
		for(int i=1; i<vals.length; i++) {
			double diff = Math.abs(val - vals[i]);
			if(diff < minDiff) {
				minDiff = diff;
				minInd = i;
			}
		}
		return minInd;
	}
	
	public static int truncateToInterval(double val, int min, int max) {
		int newVal = Math.min(max, Math.max(min, (int) Math.round(val)));
		return newVal;
	}
	
	public static double[][] truncateToInterval(double[][] val, int min, int max) {
		int w = val.length, h = (w==0) ? 0 : val[0].length;
		double[][] newVals = new double[w][h];
		for(int i=0; i<w; i++) {
			for(int j=0; j<h; j++) {
				newVals[i][j] = truncateToInterval(val[i][j], min, max);
			}
		}
		return newVals;
	}

}
