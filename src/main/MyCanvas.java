package main;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import cubeManip.CubeData;
import colours.Colour;
import cubeManip.CubeIndex;
import util.Face;
import util.FaceArray;
import util.FaceDraw;
import util.DrawHelper;


@SuppressWarnings("serial")
public class MyCanvas extends JPanel {

	private int width = 100, height = 100;
	private int[] origin = {width/2, height/2};
	private int boxSize = 1;
	//private CubeDraw cubeDraw;
	private FaceDraw faceDrawNormal, faceDrawDebug;
	private BufferedImage normalView;
	private BufferedImage pickingMask, voronoiMask;
	private CubeData cubeData;
	private Color selectionOutlineColour = Color.red;
	private Color underCursorOutlineColour = Color.red;
	public static final int PICK_VISIBLE = 0, PICK_CENTRE = 1, PICK_EDGE = 2;
	private boolean showEdges = true;
	private boolean showFaces = true;
	public static final int DRAWMODE_NORMAL = 0, DRAWMODE_VORONOI = 1, DRAWMODE_PICKMASK = 2;
	private int drawMode = DRAWMODE_NORMAL;
	public static final int COLOURMODE_NORMAL = 0, COLOURMODE_DEBUG = 1;
	private int colourMode = COLOURMODE_NORMAL;
	private static final int NO_GRID = 0, GRID_IN_FRONT = 1, GRID_BEHIND = 2;
	private int gridMode = NO_GRID;
	private Color backgroundColour = Color.black;
	private boolean showLegend = false;
	private int fillStyle = DrawHelper.FILL_COLOUR;
	
	//private FaceArray faces;

	public MyCanvas(String filename, int width, int height, int boxSize, CubeData cubeData) {
		super();
		this.width = width;
		this.height = height;
		this.boxSize = boxSize;
		origin[0] = width/2; origin[1] = height/2;
		normalView = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		pickingMask = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		voronoiMask = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		this.cubeData = cubeData;
		//this.faces = faces;
		faceDrawNormal = new FaceDraw(origin[0], origin[1], boxSize, false, fillStyle);
		faceDrawDebug = new FaceDraw(origin[0], origin[1], boxSize, true, fillStyle);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		//g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Graphics2D normalG2 = (Graphics2D) normalView.getGraphics();
		//normalG2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Graphics2D pickingG2 = (Graphics2D) pickingMask.getGraphics();
		Graphics2D voronoiG2 = (Graphics2D) voronoiMask.getGraphics();
		drawAllBuffers(normalG2, pickingG2, voronoiG2);
		switch(drawMode) {
		case DRAWMODE_NORMAL:
			DrawHelper.drawImage(g2, normalView, 0, 0);
			break;
		case DRAWMODE_VORONOI:
			DrawHelper.drawImage(g2, voronoiMask, 0, 0);
			break;
		case DRAWMODE_PICKMASK:
			DrawHelper.drawImage(g2, pickingMask, 0, 0);
			break;
		}
		if(showLegend) drawLegend(g2, backgroundColour, Colour.getIntensity(backgroundColour)<128 ? Color.white : Color.black);
	}
	
	public void drawLegend(Graphics2D g, Color background, Color lines) {
		int mid = boxSize*2;
		int[] pos = {mid, mid};
		int[] offset = {3, 3};
		faceDrawNormal.edgeDraw.drawLabeledAxes(g, pos, offset, background, lines);
	}

	public void drawGrid(Graphics2D g, FaceArray faces, int[] dims) {
		g.setColor(Color.white);
		int margin = 1;
		int maxX = dims[0]+margin;
		int maxY = dims[1]+margin;
		int maxZ = dims[2]+margin;
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						if(x<maxX) faceDrawNormal.edgeDraw.drawPosXEdge(g, new CubeIndex(x,y,z));
						if(y<maxY) faceDrawNormal.edgeDraw.drawPosYEdge(g, new CubeIndex(x,y,z));
						if(z<maxZ) faceDrawNormal.edgeDraw.drawPosZEdge(g, new CubeIndex(x,y,z));
					}
				}
			}
		}
	}

	public void drawVoronoiMask(Graphics2D g, FaceArray faces, int[] dims) {
		boolean rainbow = (drawMode==DRAWMODE_VORONOI) && (colourMode==COLOURMODE_DEBUG);
		Color background = rainbow ? Color.lightGray : Color.white;
		g.setColor(background);
		if(drawMode==DRAWMODE_VORONOI) backgroundColour = background;
		g.fillRect(0, 0, getWidth(), getHeight());
		if(rainbow) {
			if(gridMode==GRID_BEHIND) drawGrid(g, faces, dims);
		}
		g.setClip(faceDrawNormal.getVoronoiClippingMask(g, dims, background));
		for(int x=0; x<dims[0]; x++) {
			for(int y=0; y<dims[1]; y++) {
				for(int z=0; z<dims[2]; z++) {
					if(x==0 || y==0 || z==0) {
						if(rainbow) g.setColor(Colour.getRandomColour(x,y,z));
						else g.setColor(new Color(x,y,z));
						boolean[] extremes = {x==0, x==dims[0]-1, y==0, y==dims[1]-1, z==0, z==dims[2]-1};
						faceDrawNormal.drawVoronoiCell(g, new CubeIndex(x,y,z), extremes);
					}
				}
			}
		}
		if(rainbow) {
			if(gridMode==GRID_IN_FRONT) drawGrid(g, faces, dims);
		}
	}

	public void drawPickingMask(Graphics2D g, FaceArray faces, int[] dims) {
		boolean rainbow = (drawMode==DRAWMODE_PICKMASK) && (colourMode==COLOURMODE_DEBUG);
		Color background = rainbow ? Color.lightGray : Color.white;
		g.setColor(background);
		if(drawMode==DRAWMODE_PICKMASK) backgroundColour = background;
		g.fillRect(0, 0, getWidth(), getHeight());
		if(rainbow) {
			if(gridMode==GRID_BEHIND) drawGrid(g, faces, dims);
		}
		for(int x=0; x<=dims[0]; x++) {
			for(int y=0; y<=dims[1]; y++) {
				for(int z=0; z<=dims[2]; z++) {
					if(x==0 || y==0 || z==0) {
						faceDrawNormal.drawLeftRightFaceMasks(g, new CubeIndex(x,y,z), faces, rainbow);
					}
				}				
			}
		}
		if(rainbow) {
			if(gridMode==GRID_IN_FRONT) drawGrid(g, faces, dims);
		}
	}
	
	public void useSolidFaceRendering() {
		showEdges = true;
		fillStyle = DrawHelper.FILL_COLOUR;
		repaint();
	}
	
	public void useTexturedFaceRendering() {
		showEdges = false;
		fillStyle = DrawHelper.FILL_TEXTURE;
		repaint();
	}
	
	public void drawNormalView(Graphics2D g, FaceArray faces, int[] dims) {
		boolean rainbow = (drawMode==DRAWMODE_NORMAL) && (colourMode==COLOURMODE_DEBUG);
		FaceDraw faceDraw = rainbow ? faceDrawDebug : faceDrawNormal;
		Color background = rainbow ? FaceDraw.DEBUG_BACKGROUND : FaceDraw.NORMAL_BACKGROUND;
		g.setColor(background);
		if(drawMode==DRAWMODE_NORMAL) backgroundColour = background;
		g.fillRect(0, 0, getWidth(), getHeight());
		if(gridMode==GRID_BEHIND) drawGrid(g, faces, dims);
		if(showFaces) {
				for(int x=0; x<=dims[0]; x++) {
					for(int y=0; y<=dims[1]; y++) {
						for(int z=0; z<=dims[2]; z++) {
							if(x==0 || y==0 || z==0) {
								CubeIndex ci = new CubeIndex(x,y,z);
								faceDraw.drawLeftRightFaces(g, ci, faces, fillStyle);
							}
						}
					}
				}
		}
		if(showEdges) {
			// draw these in order
			int[] types = {	Face.EDGE_FLAT_BOUNDARY,
							Face.EDGE_BACKWARD,
							Face.EDGE_FORWARD,
							Face.EDGE_SILHOUETTE};
			for(int x=0; x<=dims[0]; x++) {
				for(int y=0; y<=dims[1]; y++) {
					for(int z=0; z<=dims[2]; z++) {
						if(x==0 || y==0 || z==0) {
							CubeIndex ci = new CubeIndex(x,y,z);
							faceDraw.drawShadedEdges(g, ci, faces, types);
						}
					}				
				}
			}
		}
		if(gridMode==GRID_IN_FRONT) drawGrid(g, faces, dims);
		drawUnderCursorOutlines(g);
		drawSelectionOutlines(g);
	}
	
	public void drawAllBuffers(Graphics2D g, Graphics2D gMask, Graphics2D gVoronoi) {
		FaceArray faces = cubeData.getFaces();
		int[] dims = faces.getDimensions();
		drawNormalView(g, faces, dims);
		drawPickingMask(gMask, faces, dims);
		drawVoronoiMask(gVoronoi, faces, dims);
	}

	public void drawCubeOutline(Graphics2D g, CubeIndex ci) {
		CubeIndex ciX = ci.getPosXNeighbour();
		faceDrawNormal.edgeDraw.drawPosYEdge(g, ciX);
		faceDrawNormal.edgeDraw.drawPosZEdge(g, ciX);
		CubeIndex ciY = ci.getPosYNeighbour();
		faceDrawNormal.edgeDraw.drawPosXEdge(g, ciY);
		faceDrawNormal.edgeDraw.drawPosZEdge(g, ciY);
		CubeIndex ciZ = ci.getPosZNeighbour();
		faceDrawNormal.edgeDraw.drawPosXEdge(g, ciZ);
		faceDrawNormal.edgeDraw.drawPosYEdge(g, ciZ);
	}

	public void drawSelectionOutlines(Graphics2D g) {
		g.setColor(selectionOutlineColour);
		CubeIndex[] allSelected = cubeData.getAllSelected();
		for(int i=0; i<allSelected.length; i++) {
			drawCubeOutline(g, allSelected[i]);
		}
	}

	public void drawUnderCursorOutlines(Graphics2D g) {
		g.setColor(underCursorOutlineColour);
		CubeIndex underCursor = cubeData.getUnderCursor();
		if(underCursor!=null) drawCubeOutline(g, underCursor);
	}

	public void drawCubesPickingMask(Graphics2D g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		//cubeDraw.drawCubeMasks(g, cubeData);
	}

	private void drawBackground(Graphics2D g2, Graphics2D unscaledG2) {
		g2.setColor(backgroundColour);
		g2.fillRect(0, 0, getWidth(), getHeight());
		unscaledG2.setColor(backgroundColour);
		unscaledG2.fillRect(0, 0, getWidth(), getHeight());
	}

	public int[] getPixelCoordinates(int x, int y) {
		int[] p = {x+1, y+1};
		return p;
	}

	public CubeIndex getCubeIndexPicked(int[] p, int pickingMode) {		
		switch(pickingMode) {
		case PICK_VISIBLE: return pickByVisibleFace(p);
		case PICK_CENTRE: return pickByClosestCentre(p);
		}
		return null;
	}

	public CubeIndex pickByClosestCentre(int[] p) {
		int x = p[0], y = p[1];
		if(x<0 || x>=width || y<0 || y>=height) return null;
		//boolean hasCube = pickingMask.getRGB(x,y)!=Color.white.getRGB();
		//if(!hasCube) return null;
		int rgb = voronoiMask.getRGB(x, y);
		int r = Colour.getRed(rgb);
		int g = Colour.getGreen(rgb);
		int b = Colour.getBlue(rgb);
		if(r==255 && g==255 && b==255) return null;
		else {
			CubeIndex index = new CubeIndex(r,g,b);
			return index;
		}
	}

	public CubeIndex pickByVisibleFace(int[] p) {
		int x = p[0], y = p[1];
		if(x<0 || x>=width || y<0 || y>=height) return null;
		int rgb = pickingMask.getRGB(x, y);
		int r = Colour.getRed(rgb);
		int g = Colour.getGreen(rgb);
		int b = Colour.getBlue(rgb);
		if(r==255 && g==255 && b==255) return null;
		else {
			CubeIndex index = new CubeIndex(r,g,b);
			return index;
		}
	}
}
