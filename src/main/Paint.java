package main;


import image.ImageManip;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cubeManip.CubeData;
import cubeManip.CubeSwapper;
import cubeManip.CubeIndex;
import util.ArrayUtil;
import util.FaceArray;
import util.FaceRenderingComboBox;
import util.LabelManager;
import util.ActionModeComboBox;
import util.DrawHelper;
import util.PickingModeComboBox;



@SuppressWarnings("serial")
public class Paint
	extends JFrame
	implements MouseListener, MouseMotionListener, KeyListener, ActionListener {

	private JScrollPane scrollPane;
	private MyCanvas canvas;
	private DrawCanvas drawCanvas;
	private JLabel cursorLabel;
	private JLabel cubeLabel;
	private int maxSelectable = 1;
	private JLabel selectionLabel;
	private ActionModeComboBox actionModeCombo;
	private PickingModeComboBox pickingModeCombo;
	private FaceRenderingComboBox faceRenderingCombo;
	private int width = 700, height = 550;
	//BufferedImage image;
	private String filename = "bin\\levels\\basic.png";
	//Vector<CubeIndex> cubeIndices = new Vector<CubeIndex>();
	private CubeData cubeData, drawCubeData;
	private boolean mouseDown = false;
	private boolean ctrlDown = false;
	private CubeIndex picked = null;
	//private int moverMode = CubeMover.ZDIR;
	private int[] cursor = {-1,-1};
	private int maxX = 40, maxY = 40, maxZ = 40;
	private boolean facesHiddenAtStart = true;
	private int boxSize = 28;//42;
	//private FaceArray faces;
	
	public Paint() {
		super("Isometric Game");
		setSize(width+width/2+19, height+114);
		
		// menu
		JMenuBar mb = new JMenuBar();
		setJMenuBar(mb);
		
		// load stuff
		//cubeData = loadLevelMap(filename);
		//cubeData = LevelLoader2.loadLevel(maxX, maxY, maxZ, maxSelectable);
		//FaceArray faces = LevelLoader2.loadFaces(maxX, maxY, maxZ, maxSelectable);
		cubeData = new CubeData(maxX, maxY, maxZ, maxSelectable, facesHiddenAtStart);

		// canvas
		canvas = new MyCanvas(filename, width, height, boxSize, cubeData);
		canvas.setLayout(new BorderLayout());
		canvas.setPreferredSize(canvas.getPreferredSize());
		scrollPane = new JScrollPane(canvas);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(20);
		scrollPane.getVerticalScrollBar().setUnitIncrement(20);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		addKeyListener(this);
		
		// draw canvas
		drawCubeData = new CubeData(1, 1, 1, 0, false);
		drawCanvas = new DrawCanvas(width/2, height, boxSize, drawCubeData);
		add(drawCanvas, BorderLayout.WEST);
		drawCanvas.addMouseListener(this);
		drawCanvas.addMouseMotionListener(this);

		// labels and combo box
		cursorLabel = new JLabel();
		LabelManager.updateCursorLabel(cursorLabel, null);
		cubeLabel = new JLabel();
		LabelManager.updateCubeLabel(cubeLabel, null);
		selectionLabel = new JLabel();
		LabelManager.updateSelectionLabel(selectionLabel, cubeData);
		actionModeCombo = new ActionModeComboBox(this);
		pickingModeCombo = new PickingModeComboBox(this);
		faceRenderingCombo = new FaceRenderingComboBox(this);
		JPanel labelPanel = new JPanel(new GridLayout(0,3));
		labelPanel.add(cursorLabel);
		labelPanel.add(cubeLabel);
		labelPanel.add(selectionLabel);
		labelPanel.add(actionModeCombo);
		labelPanel.add(pickingModeCombo);
		labelPanel.add(faceRenderingCombo);
		add(labelPanel, BorderLayout.SOUTH);
				
		// exit program
		addWindowListener(new WindowAdapter() { 
			public void windowClosing(WindowEvent e) { 
				exit(); 
			} 
		});
	}	

	private BufferedImage loadImage(String filename) {
		try {
			BufferedImage im = ImageIO.read(new File(filename));
			return im;
		} catch (IOException e) {
			System.out.println("failed to load image");
			return null;
		}
	}
	/*
	public CubeData loadLevelMap(String filename) {
		int[][] Is = ImageManip.getIntensity(loadImage(filename));
		CubeData cd = LevelLoader.loadLevel(Is, maxSelectable);
		return cd;
	}
	*/
		
	public static void main(String[] args) {
		Paint paintApp = new Paint();
		paintApp.setVisible(true);
	}
	
	public void exit() { 
		setVisible(false);
		dispose(); 
		System.exit(0); 
	}
	
	private void hoverTo(int x, int y) {
		cursor[0] = x; cursor[1] = y;
		LabelManager.updateCursorLabel(cursorLabel, cursor);
		picked = canvas.getCubeIndexPicked(cursor, pickingModeCombo.getModeSelected());		
		LabelManager.updateCubeLabel(cubeLabel, picked);
		if(picked==null) {
			cubeData.deselectAllUnderCursor();				
		}
		else {
			cubeData.setUnderCursor(picked, true);
		}
		refresh();
	}
	
	private void pick(int x, int y, boolean pressed) {
		cursor[0] = x; cursor[1] = y;
		LabelManager.updateCursorLabel(cursorLabel, cursor);
		picked = canvas.getCubeIndexPicked(cursor, pickingModeCombo.getModeSelected());
		LabelManager.updateCubeLabel(cubeLabel, picked);
		if(pressed) {
		if(!mouseDown) {
			if(picked==null) {
				cubeData.deselectAll();				
			}
			else {
				applyActionToCube(picked);
				//cubeData.toggleSelection(picked);
			}
			LabelManager.updateSelectionLabel(selectionLabel, cubeData);
		}
		mouseDown = true;
		}
		else mouseDown = false;
		refresh();
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(e.getSource()==canvas) hoverTo(e.getX(), e.getY());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(e.getSource()==canvas) hoverTo(e.getX(), e.getY());
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	public void applyActionToCube(CubeIndex ci) {
		int mode = actionModeCombo.getModeSelected();
		switch(mode) {
		case ActionModeComboBox.FRONT:
			cubeData.bringToFront(ci);
			refresh();
			break;
		case ActionModeComboBox.BACK:
			cubeData.sendToBack(ci);
			refresh();
			break;
		case ActionModeComboBox.FACE_FORWARD:
			cubeData.bringFaceForward(ci);
			refresh();
			break;
			/*
		case ModeComboBox.FORWARD:
			CubeSwapper.bringForward(cubeData, ci);
			refresh();
			break;
		case ModeComboBox.BACKWARD:
			CubeSwapper.sendBackward(cubeData, ci);
			refresh();
			break;
			*/
		case ActionModeComboBox.HIDE:
			cubeData.hide(ci);
			refresh();
			break;
		case ActionModeComboBox.SWAP:
			int numSelected = cubeData.getNumSelected();
			// if nothing selected yet
			System.out.println(numSelected);
			if(numSelected ==0) {
				cubeData.setSelected(ci, true);
				refresh();
			}
			// if one already selected
			else if(numSelected == 1) {
				CubeIndex currSelected = cubeData.getFirstNSelected(1)[0];
				if(ci.equals(currSelected)) {
					cubeData.setSelected(ci, false);
					refresh();
				}
				else {
					System.out.println("before: " + cubeData.getNumSelected());
					boolean areNeighbours = cubeData.areNeighbours(currSelected, ci);
					System.out.println(areNeighbours);
					if(areNeighbours) {
						//cubeData.setSelected(ci, true);
						cubeData.swap(currSelected, ci);
						refresh();
					}
					System.out.println("after: " + cubeData.getNumSelected());
				}
			}
			
			break;
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getSource()==canvas) pick(e.getX(), e.getY(), true);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getSource()==canvas) pick(e.getX(), e.getY(), false);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(e.getSource()==canvas) hoverTo(e.getX(), e.getY());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(e.getSource()==canvas) hoverTo(-1, -1);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		/*
		switch(c) {
		case 'f': case 'F':	{
			CubeIndex[] selected = cubeData.getAllSelected();
			CubeSwapper.bringToFront(cubeData, selected);
			refresh();
			return;
		}
		case 'b': case 'B':	{
			CubeIndex[] selected = cubeData.getAllSelected();
			CubeSwapper.sendToBack(cubeData, selected);
			refresh();
			return;
		}
		case 'h': case 'H': {
			CubeIndex[] selected = cubeData.getAllSelected();
			CubeSwapper.hide(cubeData, selected);
			refresh();
			return;
		}
		case 's': case 'S':
			if(cubeData.getNumSelected()==2) {
				CubeIndex[] selectedTwo = cubeData.getFirstNSelected(2);
				CubeSwapper.swap(cubeData, selectedTwo[0], selectedTwo[1]);
				refresh();
			}
			return;	
		}
		*/
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		switch(code) {
		case KeyEvent.VK_CONTROL:
			ctrlDown = true;
			refresh();
			return;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int code = e.getKeyCode();
		switch(code) {
		case KeyEvent.VK_CONTROL:
			ctrlDown = false;
			refresh();
			return;
		}
	}
	
	public void refresh() {
		//System.out.println("refresh");
		canvas.repaint();
		picked = canvas.getCubeIndexPicked(cursor, pickingModeCombo.getModeSelected());
		LabelManager.updateCubeLabel(cubeLabel, picked);
	}

	public void setMaxSelectable(int max) {
		maxSelectable = actionModeCombo.getMaxSelectable();
		cubeData.setMaxSelectable(maxSelectable);
		LabelManager.updateSelectionLabel(selectionLabel, cubeData);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == actionModeCombo.getComboBox()) {
			//cubeData.deselectAll();
			setMaxSelectable(actionModeCombo.getMaxSelectable());
			LabelManager.updateSelectionLabel(selectionLabel, cubeData);
			refresh();
		}
		if(e.getSource() == faceRenderingCombo.getComboBox()) {
			int selected = faceRenderingCombo.getComboBox().getSelectedIndex();
			switch(selected) {
			case FaceRenderingComboBox.SOLID:
				canvas.useSolidFaceRendering();
				drawCanvas.useSolidFaceRendering();
				break;
			case FaceRenderingComboBox.TEXTURE:
				canvas.useTexturedFaceRendering();
				drawCanvas.useTexturedFaceRendering();
				break;
			}
			refresh();
		}
	}
	
}
