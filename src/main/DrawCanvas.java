package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import colours.Colour;
import util.DrawHelper;
import util.Face;
import util.FaceArray;
import util.FaceDraw;
import cubeManip.CubeData;
import cubeManip.CubeIndex;

public class DrawCanvas extends JPanel {

	private int width = 100, height = 100;
	private int[] origin = {width/2, height/2};
	private int boxSize;
	//private CubeDraw cubeDraw;
	private FaceDraw faceDrawNormal, faceDrawDebug;
	private BufferedImage normalView;
	private CubeData cubeData;
	public static final int PICK_VISIBLE = 0, PICK_CENTRE = 1, PICK_EDGE = 2;
	private boolean showEdges = true;
	private boolean showFaces = true;
	public static final int DRAWMODE_NORMAL = 0, DRAWMODE_VORONOI = 1, DRAWMODE_PICKMASK = 2;
	private int drawMode = DRAWMODE_NORMAL;
	public static final int COLOURMODE_NORMAL = 0, COLOURMODE_DEBUG = 1;
	private int colourMode = COLOURMODE_NORMAL;
	private Color backgroundColour = Color.black;
	private boolean firstTime = true;
	private String exportFileName = "textures\\testCube.png";
	private Rectangle cropArea = new Rectangle(0,0,10,10);
	private int fillStyle = DrawHelper.FILL_COLOUR;//.FILL_TEXTURE;
	private int patternStyle = DrawHelper.STYLE_CUSTOM;

	//private FaceArray faces;

	public DrawCanvas(int width, int height, int boxSize, CubeData cubeData) {
		super();
		this.width = width;
		this.height = height;
		this.boxSize = boxSize;
		origin[0] = width/2; origin[1] = height/2;
		normalView = DrawHelper.getTransparentImage(width, height);
		this.cubeData = cubeData;
		//this.faces = faces;
		faceDrawNormal = new FaceDraw(origin[0], origin[1], boxSize, false, fillStyle);
		faceDrawDebug = new FaceDraw(origin[0], origin[1], boxSize, true, fillStyle);
		defineCropArea();
	}

	public void useSolidFaceRendering() {
		showEdges = true;
		fillStyle = DrawHelper.FILL_COLOUR;
		repaint();
	}
	
	public void useTexturedFaceRendering() {
		showEdges = true;
		fillStyle = DrawHelper.FILL_COLOUR_AND_PATTERN;
		patternStyle = DrawHelper.STYLE_CUSTOM;
		repaint();
	}
	
	private void defineCropArea() {
		int[][] vectors = faceDrawNormal.getXYZVectors();
		int[] vx = vectors[0], vy = vectors[1], vz = vectors[2];
		cropArea.x = origin[0]-vy[0];
		cropArea.y = origin[1]-vy[1]-vx[1];
		cropArea.width = -vx[0] + vy[0];
		cropArea.height = vx[1] + vy[1]-vz[1];
		//System.out.println(cropArea.width + ", " + cropArea.height);
		//System.out.println("A: vx = " + vx[0] + ", " + vx[1]);
		//System.out.println(origin[0] + ", " + origin[1]);
		//System.out.println(cropArea);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		//g2.setColor(Color.red);
		//g2.fillRect(0,0,getWidth(), getHeight());
		//g2.setColor(Color.lightGray);
		//g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Graphics2D normalG2 = (Graphics2D) normalView.getGraphics();
		//normalG2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		drawBuffer(normalG2);
		DrawHelper.drawImage(g2, normalView, 0, 0);
		if(firstTime) {
			exportTemplate();
			firstTime = false;
		}
	}

	public void drawBuffer(Graphics2D g) {
		FaceArray faces = cubeData.getFaces();
		int[] dims = faces.getDimensions();
		drawNormalView(g, faces, dims, showEdges);
	}

	public void drawNormalView(Graphics2D g, FaceArray faces, int[] dims, boolean withEdges) {
		boolean rainbow = (drawMode==DRAWMODE_NORMAL) && (colourMode==COLOURMODE_DEBUG);
		FaceDraw faceDraw = rainbow ? faceDrawDebug : faceDrawNormal;
		Color background = rainbow ? FaceDraw.DEBUG_BACKGROUND : FaceDraw.NORMAL_BACKGROUND;
		backgroundColour = background;
		g.setColor(background);
		if(drawMode==DRAWMODE_NORMAL) backgroundColour = background;
		//g.fillRect(0, 0, getWidth(), getHeight());
		if(showFaces) {
			if(fillStyle==DrawHelper.FILL_COLOUR_AND_PATTERN || fillStyle==DrawHelper.FILL_COLOUR) {
				for(int x=0; x<=dims[0]; x++) {
					for(int y=0; y<=dims[1]; y++) {
						for(int z=0; z<=dims[2]; z++) {
							if(x==0 || y==0 || z==0) {
								CubeIndex ci = new CubeIndex(x,y,z);
								faceDraw.drawLeftRightFaces(g, ci, faces, fillStyle);
							}
						}
					}
				}
			}

			if(fillStyle==DrawHelper.FILL_COLOUR_AND_PATTERN || fillStyle==DrawHelper.FILL_PATTERN) {
				for(int x=0; x<=dims[0]; x++) {
					for(int y=0; y<=dims[1]; y++) {
						for(int z=0; z<=dims[2]; z++) {
							if(x==0 || y==0 || z==0) {
								CubeIndex ci = new CubeIndex(x,y,z);
								faceDraw.addPattern(g, ci, faces, patternStyle);
							}
						}
					}
				}
			}
		}
		if(withEdges) {
			// draw these in order
			int[] types = {	Face.EDGE_FLAT_BOUNDARY,
					Face.EDGE_BACKWARD,
					Face.EDGE_FORWARD,
					Face.EDGE_SILHOUETTE};
			for(int x=0; x<=dims[0]; x++) {
				for(int y=0; y<=dims[1]; y++) {
					for(int z=0; z<=dims[2]; z++) {
						if(x==0 || y==0 || z==0) {
							CubeIndex ci = new CubeIndex(x,y,z);
							faceDraw.drawShadedEdges(g, ci, faces, types);
						}
					}				
				}
			}
		}
	}

	private boolean exportTemplate() {
		// Write generated image to a file
		try {
			// Save as PNG
			int margin = 0;
			//BufferedImage im = DrawHelper.getTransparentImage(cropArea.width+2*margin, cropArea.height+2*margin);
			//Graphics2D g2 = (Graphics2D) im.getGraphics();
			//g2.clearRect(0, 0, im.getWidth(), im.getHeight());
			//g2.setColor(backgroundColour);
			//g2.fillRect(0, 0, cropArea.width+2*margin, cropArea.height+2*margin);
			//g2.fillRect(0,0,50,50);
			//g2.drawImage(	normalView,
			//		margin, margin, cropArea.width+margin, cropArea.height+margin, 
			//		cropArea.x, cropArea.y, cropArea.x+cropArea.width, cropArea.y+cropArea.height, null);
			FaceArray faces = cubeData.getFaces();
			int[] dims = faces.getDimensions();
			Graphics2D g2 = (Graphics2D) normalView.getGraphics();
			g2.clearRect(0, 0, normalView.getWidth(), normalView.getHeight());
			DrawHelper.clearTransparentImage(normalView);
			drawNormalView(g2, faces, dims, false);
			BufferedImage im = cropTransparentImage(normalView, margin, cropArea.x, cropArea.y, cropArea.x+cropArea.width, cropArea.y+cropArea.height);
			ImageIO.write(im, "png", new File(exportFileName));
			System.out.println("texture export successful");
		}
		catch (IOException e) {
			System.out.println("saveToFile failed");
			return false;
		}
		return true;
	}
	
	public static BufferedImage cropTransparentImage(BufferedImage src, 
			int margin, int src_x1, int src_y1, int src_x2, int src_y2) {
		int w = src_x2-src_x1+1+2*margin;
		int h = src_y2-src_y1+1+2*margin;
		BufferedImage dest = DrawHelper.getTransparentImage(w, h);
		for(int i=0; i<src_x2-src_x1+1; i++) {
			for(int j=0; j<src_y2-src_y1+1; j++) {
				dest.setRGB(margin+i, margin+j, src.getRGB(src_x1+i, src_y1+j));
				
			}
		}
		return dest;
		
	}
}
