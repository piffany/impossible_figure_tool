package image;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Vector;

import util.MyMath;

import colours.Colour;

public class ImageManip {

	public static BufferedImage deepCopy(BufferedImage bi) {
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
	public static int getIntensity(BufferedImage image, int x, int y) {
		if(x>=0 && x<image.getWidth() && y>=0 && y<image.getHeight()) {
			Color c = Colour.rgbInt2Color(image.getRGB(x,y));
			int intensity = (int) Math.round((double)(c.getRed() + c.getGreen() + c.getBlue())/3.0);
			intensity = MyMath.truncateToInterval(intensity, 0, 255);
			return intensity;
		}
		else return -1;
	}
	
	// 1 = black
	public static int getBinary(BufferedImage image, int x, int y) {
		if(x>=0 && x<image.getWidth() && y>=0 && y<image.getHeight()) {
			Color c = Colour.rgbInt2Color(image.getRGB(x,y));
			int intensity = (int) Math.round((double)(c.getRed() + c.getGreen() + c.getBlue())/3.0);
			intensity = MyMath.truncateToInterval(intensity, 0, 255);
			return (intensity==0) ? 1 : 0;
		}
		else return -1;
	}
	
	public static void setIntensity(BufferedImage image, int x, int y, int intensity) {
		if(x>=0 && x<image.getWidth() && y>=0 && y<image.getHeight() && intensity >=0 && intensity <=255) {
			Color c = new Color(intensity, intensity, intensity);
			image.setRGB(x, y, c.getRGB());
		}		
	}
	
	public static void setBinary(BufferedImage image, int x, int y, int binary) {
		if(x>=0 && x<image.getWidth() && y>=0 && y<image.getHeight()) {
			int intensity = binary==1 ? 0 : 255;
			Color c = new Color(intensity, intensity, intensity);
			image.setRGB(x, y, c.getRGB());
		}		
	}
	
	public static int[][] getIntensity(BufferedImage image) {
		int[][] colors = new int[image.getWidth()][image.getHeight()];
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i,j));
				int intensity = (int) Math.round((double)(c.getRed() + c.getGreen() + c.getBlue())/3.0);
				intensity = MyMath.truncateToInterval(intensity, 0, 255);
				colors[i][j] = intensity;
			}
		}
		return colors;
	}
	
	public static int[][] getBinary(BufferedImage image) {
		int[][] colors = new int[image.getWidth()][image.getHeight()];
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i,j));
				int intensity = (int) Math.round((double)(c.getRed() + c.getGreen() + c.getBlue())/3.0);
				intensity = MyMath.truncateToInterval(intensity, 0, 255);
				colors[i][j] = (intensity==0) ? 1 : 0;
			}
		}
		return colors;
	}
	
	public static int[][] getChannel(BufferedImage image, int channel) {
		int[][] colors = new int[image.getWidth()][image.getHeight()];
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i,j));
				switch(channel) {
				case 0:
					colors[i][j] = c.getRed();
					break;
				case 1:
					colors[i][j] = c.getGreen();
					break;
				case 2:
					colors[i][j] = c.getBlue();
					break;
				}
			}
		}
		return colors;
	}
	
	public static BufferedImage getGrayscaleImage(BufferedImage imageRGB) {
		int[][] Rs = getChannel(imageRGB, 0);
		int[][] Gs = getChannel(imageRGB, 1);
		int[][] Bs = getChannel(imageRGB, 2);
		return getGrayscaleImage(Rs, Gs, Bs);
	}
	
	public static BufferedImage getGrayscaleImage(int[][] Is, int[][] As) {
		int w = Is.length;
		int h = (w==0) ? 0 : Is[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int I = MyMath.truncateToInterval(Is[x][y], 0, 255);
				int A = MyMath.truncateToInterval(As[x][y], 0, 255);
				after.setRGB(x, y, new Color(I, I, I, A).getRGB());
			}
		}	
		return after;
	}
	
	public static BufferedImage getGrayscaleImage(int[][] Is) {
		int w = Is.length;
		int h = (w==0) ? 0 : Is[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int I = MyMath.truncateToInterval(Is[x][y], 0, 255);
				after.setRGB(x, y, new Color(I, I, I, 255).getRGB());
			}
		}	
		return after;
	}
	
	public static BufferedImage getGrayscaleImageFromBinary(int[][] Bs) {
		int w = Bs.length;
		int h = (w==0) ? 0 : Bs[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int I = Bs[x][y]==1 ? 0 : 255;
				after.setRGB(x, y, new Color(I, I, I, 255).getRGB());
			}
		}	
		return after;
	}
	
	public static BufferedImage getGrayscaleImage(int[][] Rs, int[][] Gs, int[][] Bs) {
		int w = Rs.length;
		int h = (w==0) ? 0 : Rs[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int R = MyMath.truncateToInterval(Rs[x][y], 0, 255);
				int G = MyMath.truncateToInterval(Gs[x][y], 0, 255);
				int B = MyMath.truncateToInterval(Bs[x][y], 0, 255);
				int I = (R+G+B)/3;
				after.setRGB(x, y, new Color(I, I, I, 255).getRGB());
			}
		}	
		return after;
	}
	
	public static BufferedImage getRGBImage(int[][] Rs, int[][] Gs, int[][] Bs) {
		int w = Rs.length;
		int h = (w==0) ? 0 : Rs[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int R = MyMath.truncateToInterval(Rs[x][y], 0, 255);
				int G = MyMath.truncateToInterval(Gs[x][y], 0, 255);
				int B = MyMath.truncateToInterval(Bs[x][y], 0, 255);
				after.setRGB(x, y, new Color(R, G, B, 255).getRGB());
			}
		}	
		return after;
	}
	
	public static BufferedImage scaleImage(BufferedImage before, double rx, double ry, int interp) {
		int w = before.getWidth();
		int h = before.getHeight();
		BufferedImage after1 = new BufferedImage((int)(w*rx), (int)(h*ry), BufferedImage.TYPE_INT_ARGB);
		AffineTransform at1 = AffineTransform.getScaleInstance(rx, ry);
		AffineTransformOp scaleOp1 = 
		   new AffineTransformOp(at1, interp);
		after1 = scaleOp1.filter(before, after1);

		return after1;
	}
	
	public static BufferedImage getRGBImage(int[][] Rs, int[][] Gs, int[][] Bs, int[][] As) {
		int w = Rs.length;
		int h = (w==0) ? 0 : Rs[0].length;
		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int R = MyMath.truncateToInterval(Rs[x][y], 0, 255);
				int G = MyMath.truncateToInterval(Gs[x][y], 0, 255);
				int B = MyMath.truncateToInterval(Bs[x][y], 0, 255);
				int A = MyMath.truncateToInterval(As[x][y], 0, 255);
				after.setRGB(x, y, new Color(R, G, B, A).getRGB());
			}
		}	
		return after;
	}
	
	public static Vector<Integer> getRGBInts(BufferedImage image) {
		Vector<Integer> rgb = new Vector<Integer>();
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				boolean hasColor = false;
				int currRGB = image.getRGB(i, j);
				for(int k=0; k<rgb.size(); k++) {
					if(rgb.get(k) == currRGB) {
						hasColor = true;
						break;
					}
				}
				if(!hasColor) rgb.add(currRGB);
			}
		}
		return rgb;
	}
}
