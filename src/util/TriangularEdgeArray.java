package util;

import cubeManip.CubeIndex;

public class TriangularEdgeArray {
	
	private int maxX, maxY, maxZ;
	private int[][][][] posXEdgeTypes;
	private int[][][][] posYEdgeTypes;
	private int[][][][] posZEdgeTypes;
	
	public static int[] NULL_TYPE = {Face.EDGE_NULL, Face.FACE_NONE};

	public TriangularEdgeArray(int maxX, int maxY, int maxZ) {
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
		posXEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		posYEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		posZEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		init();
	}
	
	public TriangularEdgeArray(TriangularEdgeArray other) {
		this.maxX = other.maxX;
		this.maxY = other.maxY;
		this.maxZ = other.maxZ;
		posXEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		posYEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		posZEdgeTypes = new int[maxX+1][maxY+1][maxZ+1][2];
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						setPosXEdgeType(x, y, z, other.getPosXEdgeType(x,y,z));
						setPosYEdgeType(x, y, z, other.getPosYEdgeType(x,y,z));
						setPosZEdgeType(x, y, z, other.getPosZEdgeType(x,y,z));
					}
				}
			}
		}
	}
	
	public boolean isValidIndex(CubeIndex ci) {
		int x = ci.getX(), y = ci.getY(), z = ci.getZ();
		boolean withinBounds = (x>=0 && x<=maxX) && (y>=0 && y<=maxY) && (z>=0 && z<=maxZ);
		boolean onFaces = (x==0 || y==0 || z==0) && (x>=0 && y>=0 && z>=0);
		return withinBounds && onFaces;
	}
	
	public boolean isValidIndex(int x, int y, int z) {
		return isValidIndex(new CubeIndex(x,y,z));
	}
	
	public int[] getPosXEdgeType(int x, int y, int z) {
		if(!isValidIndex(x,y,z)) return NULL_TYPE;
		int[] type = {posXEdgeTypes[x][y][z][0], posXEdgeTypes[x][y][z][1]};
		return type;
	}

	public int[] getPosYEdgeType(int x, int y, int z) {
		if(!isValidIndex(x,y,z)) return NULL_TYPE;
		int[] type = {posYEdgeTypes[x][y][z][0], posYEdgeTypes[x][y][z][1]};
		return type;
	}

	public int[] getPosZEdgeType(int x, int y, int z) {
		if(!isValidIndex(x,y,z)) return NULL_TYPE;
		int[] type = {posZEdgeTypes[x][y][z][0], posZEdgeTypes[x][y][z][1]};
		return type;
	}
	
	public int[] getPosXEdgeType(CubeIndex ci) {
		return getPosXEdgeType(ci.getX(), ci.getY(), ci.getZ());
	}

	public int[] getPosYEdgeType(CubeIndex ci) {
		return getPosYEdgeType(ci.getX(), ci.getY(), ci.getZ());
	}

	public int[] getPosZEdgeType(CubeIndex ci) {
		return getPosZEdgeType(ci.getX(), ci.getY(), ci.getZ());
	}

	public void setPosXEdgeType(int x, int y, int z, int[] val) {
		if(!isValidIndex(x,y,z)) return;
		posXEdgeTypes[x][y][z][0] = val[0];
		posXEdgeTypes[x][y][z][1] = val[1];
	}

	public void setPosYEdgeType(int x, int y, int z, int[] val) {
		if(!isValidIndex(x,y,z)) return;
		posYEdgeTypes[x][y][z][0] = val[0];
		posYEdgeTypes[x][y][z][1] = val[1];
	}

	public void setPosZEdgeType(int x, int y, int z, int[] val) {
		if(!isValidIndex(x,y,z)) return;
		posZEdgeTypes[x][y][z][0] = val[0];
		posZEdgeTypes[x][y][z][1] = val[1];
	}
	
	public void setPosXEdgeType(int x, int y, int z, int val) {
		if(!isValidIndex(x,y,z)) return;
		posXEdgeTypes[x][y][z][0] = val;
	}

	public void setPosYEdgeType(int x, int y, int z, int val) {
		if(!isValidIndex(x,y,z)) return;
		posYEdgeTypes[x][y][z][0] = val;
	}

	public void setPosZEdgeType(int x, int y, int z, int val) {
		if(!isValidIndex(x,y,z)) return;
		posZEdgeTypes[x][y][z][0] = val;
	}

	public void setPosXEdgeType(CubeIndex ci, int[] val) {
		setPosXEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setPosYEdgeType(CubeIndex ci, int[] val) {
		setPosYEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setPosZEdgeType(CubeIndex ci, int[] val) {
		setPosZEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setNegXEdgeType(CubeIndex ci, int[] val) {
		setPosXEdgeType(ci.getNegXNeighbour(), val);
	}

	public void setNegYEdgeType(CubeIndex ci, int[] val) {
		setPosYEdgeType(ci.getNegYNeighbour(), val);
	}

	public void setNegZEdgeType(CubeIndex ci, int[] val) {
		setPosZEdgeType(ci.getNegZNeighbour(), val);
	}

	public void setPosXEdgeType(CubeIndex ci, int val) {
		setPosXEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setPosYEdgeType(CubeIndex ci, int val) {
		setPosYEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setPosZEdgeType(CubeIndex ci, int val) {
		setPosZEdgeType(ci.getX(), ci.getY(), ci.getZ(), val);
	}

	public void setNegXEdgeType(CubeIndex ci, int val) {
		setPosXEdgeType(ci.getNegXNeighbour(), val);
	}

	public void setNegYEdgeType(CubeIndex ci, int val) {
		setPosYEdgeType(ci.getNegYNeighbour(), val);
	}

	public void setNegZEdgeType(CubeIndex ci, int val) {
		setPosZEdgeType(ci.getNegZNeighbour(), val);
	}

	public void init() {
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						posXEdgeTypes[x][y][z][0] = Face.EDGE_NULL;
						posYEdgeTypes[x][y][z][0] = Face.EDGE_NULL;
						posZEdgeTypes[x][y][z][0] = Face.EDGE_NULL;
						posXEdgeTypes[x][y][z][1] = Face.FACE_NONE;
						posYEdgeTypes[x][y][z][1] = Face.FACE_NONE;
						posZEdgeTypes[x][y][z][1] = Face.FACE_NONE;
					}
				}
			}
		}
	}
	
}
