package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Paint;

public class FaceRenderingComboBox extends JPanel {

	public static final int SOLID = 0, TEXTURE = 1;
	public static final String[] modeNames = {"Solid colour", "Texture"};
	private JLabel label = new JLabel("Face Rendering: ");
	private JComboBox<String> combo;
	
	public FaceRenderingComboBox(ActionListener listener) {
		super();
		combo = new JComboBox<String>(modeNames);
		combo.setSelectedIndex(0);
		combo.addActionListener(listener);
		add(label);
		add(combo);
		combo.setFocusable(false);
	}
	
	public JComboBox getComboBox() {
		return combo;
	}
}
