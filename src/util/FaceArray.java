package util;

import java.awt.Graphics2D;
import java.util.Vector;

import cubeManip.CubeIndex;

public class FaceArray {

	private int maxX, maxY, maxZ;
	// arrays of layered faces
	private TriangularArray triArray1, triArray2;
	// arrays of edge types
	private TriangularEdgeArray edgeTypeArray;
	private EdgeValidator edgeValidator;
	private CubeOrderChecker orderChecker;
	
	public FaceArray(int maxX, int maxY, int maxZ, boolean hidden) {
		this.setMaxX(maxX);
		this.setMaxY(maxY);
		this.setMaxZ(maxZ);
		triArray1 = new TriangularArray(maxX, maxY, maxZ);
		triArray2 = new TriangularArray(maxX, maxY, maxZ);
		edgeTypeArray = new TriangularEdgeArray(maxX, maxY, maxZ);
		edgeValidator = new EdgeValidator(edgeTypeArray);
		orderChecker = new CubeOrderChecker(this);
		for(int x=0; x<maxX; x++) {
			for(int y=0; y<maxY; y++) {
				for(int z=0; z<maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						CubeIndex ci = new CubeIndex(x,y,z);
						addCube(ci, false, hidden);
					}
				}
			}
		}
	}
	
	public FaceArray(FaceArray faces) {
		this.setMaxX(faces.getMaxX());
		this.setMaxY(faces.getMaxY());
		this.setMaxZ(faces.getMaxZ());
		triArray1 = new TriangularArray(faces.getTriArray1());
		triArray2 = new TriangularArray(faces.getTriArray2());
		edgeTypeArray = new TriangularEdgeArray(faces.getEdgeTypeArray());
		edgeValidator = new EdgeValidator(edgeTypeArray);
		orderChecker = new CubeOrderChecker(this);
	}
	
	public void replaceFaces(FaceArray faces) {
		triArray1.replaceFaces(faces.getTriArray1());
		triArray2.replaceFaces(faces.getTriArray2());
		edgeTypeArray = new TriangularEdgeArray(faces.getEdgeTypeArray());
		edgeValidator = new EdgeValidator(edgeTypeArray);
		orderChecker = new CubeOrderChecker(this);

	}

	public FaceArray clone() {
		return new FaceArray(this);
	}

	private TriangularArray getTriArray1() {
		return triArray1;
	}

	private TriangularArray getTriArray2() {
		return triArray2;
	}
	
	private TriangularEdgeArray getEdgeTypeArray() {
		return edgeTypeArray;
	}

	public void addCube(CubeIndex ci, boolean selected, boolean hidden) {
		addFaceXy(ci, new Face(ci, Face.FACE_XY, selected, hidden));
		addFaceXz(ci, new Face(ci, Face.FACE_XZ, selected, hidden));
		addFaceYx(ci, new Face(ci, Face.FACE_YX, selected, hidden));
		addFaceYz(ci, new Face(ci, Face.FACE_YZ, selected, hidden));
		addFaceZx(ci, new Face(ci, Face.FACE_ZX, selected, hidden));
		addFaceZy(ci, new Face(ci, Face.FACE_ZY, selected, hidden));
		updateEdgesAroundCube(ci, false);
	}
	
	public void updateEdgesAroundCube(CubeIndex ci, boolean favourSilhouette) {
		CubeIndex ciX = ci.getNegXNeighbour();
		CubeIndex ciY = ci.getNegYNeighbour();
		CubeIndex ciZ = ci.getNegZNeighbour();
		updatePosXEdgeAndFaceType(ci, favourSilhouette);
		updatePosYEdgeAndFaceType(ci, favourSilhouette);
		updatePosZEdgeAndFaceType(ci, favourSilhouette);
		updatePosXEdgeAndFaceType(ciX, favourSilhouette);
		updateNegYEdgeAndFaceType(ciX, favourSilhouette);
		updateNegZEdgeAndFaceType(ciX, favourSilhouette);
		updateNegXEdgeAndFaceType(ciY, favourSilhouette);
		updatePosYEdgeAndFaceType(ciY, favourSilhouette);
		updateNegZEdgeAndFaceType(ciY, favourSilhouette);
		updateNegXEdgeAndFaceType(ciZ, favourSilhouette);
		updateNegYEdgeAndFaceType(ciZ, favourSilhouette);
		updatePosZEdgeAndFaceType(ciZ, favourSilhouette);
		
		//fixSilhouetteEdgeProblems(ci);
		//orderChecker.checkCubeOrderingWithNeighbours(ci);
		/*
		fixSilhouetteEdgeProblems(ciX);
		fixSilhouetteEdgeProblems(ciY);
		fixSilhouetteEdgeProblems(ciZ);
		*/
		/*
		if(!edgeValidator.validateAroundVertex(ciX, ci)) {
			System.out.println("invalid");
			updatePosXEdgeAndFaceType(ciX, true);
			updateNegYEdgeAndFaceType(ciX, true);
			updateNegZEdgeAndFaceType(ciX, true);
		}
		*/
		
		//edgeValidator.validateCube(ci);
	}
	
	public boolean isValidIndex(CubeIndex ci) {
		if(ci!=null) {
			int x = ci.getX();
			int y = ci.getY();
			int z = ci.getZ();
			return x>=0 && x<maxX && y>=0 && y<maxY && z>=0 && z<maxZ;
		}
		return false;
	}
	
	public LayeredFace[] getSharedFaces(CubeIndex ci1, CubeIndex ci2) {
		if(isValidIndex(ci1) && isValidIndex(ci2)) {
			if(ci1.getPosXNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceXy(ci1), getFaceXz(ci1)};
				return shared;
			}
			else if(ci1.getNegXNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceYz(ci1), getFaceZy(ci1)};
				return shared;
			}
			else if(ci1.getPosYNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceYx(ci1), getFaceYz(ci1)};
				return shared;
			}
			else if(ci1.getNegYNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceXz(ci1), getFaceZx(ci1)};
				return shared;
			}
			else if(ci1.getPosZNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceZx(ci1), getFaceZy(ci1)};
				return shared;
			}
			else if(ci1.getNegZNeighbour().equals(ci2)) {
				LayeredFace[] shared = {getFaceXy(ci1), getFaceYx(ci1)};
				return shared;
			}
		}
		LayeredFace[] empty = {};
		return empty;
	}
	
	public boolean areNeighbours(CubeIndex ci1, CubeIndex ci2) {
		LayeredFace[] sharedFaces = getSharedFaces(ci1, ci2);
		return sharedFaces.length > 0;
	}
	
	private void fixSilhouetteEdgeProblems(CubeIndex ci) {
		CubeIndex ciX = ci.getNegXNeighbour();
		CubeIndex ciY = ci.getNegYNeighbour();
		CubeIndex ciZ = ci.getNegZNeighbour();
		int[] posXType = getPosXEdgeAndFaceType(ci);
		int[] negXType = getPosXEdgeAndFaceType(ciX);
		int[] posYType = getPosYEdgeAndFaceType(ci);
		int[] negYType = getPosYEdgeAndFaceType(ciY);
		int[] posZType = getPosZEdgeAndFaceType(ci);
		int[] negZType = getPosZEdgeAndFaceType(ciZ);

		if(negXType[0]==Face.EDGE_SILHOUETTE) {
			if(posZType[0]==Face.EDGE_FORWARD && negYType[0]==Face.EDGE_FLAT_BOUNDARY) negYType[0] = Face.EDGE_SILHOUETTE;
			else if(posZType[0]==Face.EDGE_FORWARD && posXType[0]==Face.EDGE_FORWARD && negZType[0]==Face.EDGE_FLAT_BOUNDARY) negZType[0] = Face.EDGE_SILHOUETTE;
			if(posYType[0]==Face.EDGE_FORWARD && negZType[0]==Face.EDGE_FLAT_BOUNDARY) negZType[0] = Face.EDGE_SILHOUETTE;
			else if(posYType[0]==Face.EDGE_FORWARD && posXType[0]==Face.EDGE_FORWARD && negYType[0]==Face.EDGE_FLAT_BOUNDARY) negYType[0] = Face.EDGE_SILHOUETTE;
		}
		if(negYType[0]==Face.EDGE_SILHOUETTE) {
			if(posZType[0]==Face.EDGE_FORWARD && negXType[0]==Face.EDGE_FLAT_BOUNDARY) negXType[0] = Face.EDGE_SILHOUETTE;
			else if(posZType[0]==Face.EDGE_FORWARD && posYType[0]==Face.EDGE_FORWARD && negZType[0]==Face.EDGE_FLAT_BOUNDARY) negZType[0] = Face.EDGE_SILHOUETTE;
			if(posXType[0]==Face.EDGE_FORWARD && negZType[0]==Face.EDGE_FLAT_BOUNDARY) negZType[0] = Face.EDGE_SILHOUETTE;
			else if(posXType[0]==Face.EDGE_FORWARD && posYType[0]==Face.EDGE_FORWARD && negXType[0]==Face.EDGE_FLAT_BOUNDARY) negXType[0] = Face.EDGE_SILHOUETTE;
		}
		if(negZType[0]==Face.EDGE_SILHOUETTE) {
			if(posYType[0]==Face.EDGE_FORWARD && negXType[0]==Face.EDGE_FLAT_BOUNDARY) negXType[0] = Face.EDGE_SILHOUETTE;
			else if(posYType[0]==Face.EDGE_FORWARD && posZType[0]==Face.EDGE_FORWARD && negYType[0]==Face.EDGE_FLAT_BOUNDARY) negYType[0] = Face.EDGE_SILHOUETTE;
			if(posXType[0]==Face.EDGE_FORWARD && negYType[0]==Face.EDGE_FLAT_BOUNDARY) negYType[0] = Face.EDGE_SILHOUETTE;
			else if(posXType[0]==Face.EDGE_FORWARD && posZType[0]==Face.EDGE_FORWARD && negXType[0]==Face.EDGE_FLAT_BOUNDARY) negXType[0] = Face.EDGE_SILHOUETTE;
		}
		edgeTypeArray.setPosXEdgeType(ci, posXType);
		edgeTypeArray.setPosYEdgeType(ci, posYType);
		edgeTypeArray.setPosZEdgeType(ci, posZType);
		edgeTypeArray.setPosXEdgeType(ciX, negXType);
		edgeTypeArray.setPosYEdgeType(ciY, negYType);
		edgeTypeArray.setPosZEdgeType(ciZ, negZType);
		//int[][] fixedEdgeTypes = {posXType, negXType, posYType, negYType, posZType, negZType};
		//return fixedEdgeTypes;
		
	}
	
	public int[] getDimensions() {
		int[] dims = {maxX, maxY, maxZ};
		return dims;
	}
	
	// left face
	public LayeredFace getFaceXz(CubeIndex ci) {
		return triArray1.get(ci);
	}
	public void addFaceXz(CubeIndex ci, Face face) {
		triArray1.add(ci, face);
	}
	
	// top right face
	public LayeredFace getFaceZy(CubeIndex ci) {
		return triArray1.getNegXNeighbour(ci);
	}
	public void addFaceZy(CubeIndex ci, Face face) {
		triArray1.add(ci.getNegXNeighbour(), face);
	}
	
	// bottom right face
	public LayeredFace getFaceYx(CubeIndex ci) {
		return triArray1.getPosYNeighbour(ci);
	}
	public void addFaceYx(CubeIndex ci, Face face) {
		triArray1.add(ci.getPosYNeighbour(), face);
	}

	// right face
	public LayeredFace getFaceYz(CubeIndex ci) {
		return triArray2.get(ci);
	}
	public void addFaceYz(CubeIndex ci, Face face) {
		triArray2.add(ci, face);
	}
	
	// top left face
	public LayeredFace getFaceZx(CubeIndex ci) {
		return triArray2.getNegYNeighbour(ci);
	}
	public void addFaceZx(CubeIndex ci, Face face) {
		triArray2.add(ci.getNegYNeighbour(), face);
	}
	
	// bottom left face
	public LayeredFace getFaceXy(CubeIndex ci) {
		return triArray2.getPosXNeighbour(ci);
	}
	public void addFaceXy(CubeIndex ci, Face face) {
		triArray2.add(ci.getPosXNeighbour(), face);
	}
	
	public boolean isFaceXySelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceXy(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}

	public boolean isFaceXzSelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceXz(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}

	public boolean isFaceYxSelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceYx(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}

	public boolean isFaceYzSelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceYz(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}

	public boolean isFaceZxSelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceZx(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}

	public boolean isFaceZySelected(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceZy(ci);
		return face!=null && (top ? face.isTopSelected(ci) : face.isSelected(ci));
	}
	
	public boolean isFaceXyUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceXy(ci);
		return face!=null && face.isUnderCursor(ci);
	}

	public boolean isFaceXzUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceXz(ci);
		return face!=null && face.isUnderCursor(ci);
	}

	public boolean isFaceYxUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceYx(ci);
		return face!=null && face.isUnderCursor(ci);
	}

	public boolean isFaceYzUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceYz(ci);
		return face!=null && face.isUnderCursor(ci);
	}

	public boolean isFaceZxUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceZx(ci);
		return face!=null && face.isUnderCursor(ci);
	}

	public boolean isFaceZyUnderCursor(CubeIndex ci, boolean top) {
		LayeredFace face = getFaceZy(ci);
		return face!=null && face.isUnderCursor(ci);
	}
	
	public boolean isPosXEdgeSelected(CubeIndex ci) {
		return isFaceXySelected(ci, true) && isFaceXzSelected(ci, true);
	}

	public boolean isNegXEdgeSelected(CubeIndex ci) {
		return isFaceYzSelected(ci, true) && isFaceZySelected(ci, true);
	}

	public boolean isPosYEdgeSelected(CubeIndex ci) {
		return isFaceYxSelected(ci, true) && isFaceYzSelected(ci, true);
	}

	public boolean isNegYEdgeSelected(CubeIndex ci) {
		return isFaceXzSelected(ci, true) && isFaceZxSelected(ci, true);
	}

	public boolean isPosZEdgeSelected(CubeIndex ci) {
		return isFaceZxSelected(ci, true) && isFaceZySelected(ci, true);
	}

	public boolean isNegZEdgeSelected(CubeIndex ci) {
		return isFaceXySelected(ci, true) && isFaceYxSelected(ci, true);
	}
	
	public boolean isPosXEdgeUnderCursor(CubeIndex ci) {
		return isFaceXyUnderCursor(ci, true) && isFaceXzUnderCursor(ci, true);
	}

	public boolean isNegXEdgeUnderCursor(CubeIndex ci) {
		return isFaceYzUnderCursor(ci, true) && isFaceZyUnderCursor(ci, true);
	}

	public boolean isPosYEdgeUnderCursor(CubeIndex ci) {
		return isFaceYxUnderCursor(ci, true) && isFaceYzUnderCursor(ci, true);
	}

	public boolean isNegYEdgeUnderCursor(CubeIndex ci) {
		return isFaceXzUnderCursor(ci, true) && isFaceZxUnderCursor(ci, true);
	}

	public boolean isPosZEdgeUnderCursor(CubeIndex ci) {
		return isFaceZxUnderCursor(ci, true) && isFaceZyUnderCursor(ci, true);
	}

	public boolean isNegZEdgeUnderCursor(CubeIndex ci) {
		return isFaceXyUnderCursor(ci, true) && isFaceYxUnderCursor(ci, true);
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}

	public int getMaxZ() {
		return maxZ;
	}

	public void setMaxZ(int maxZ) {
		this.maxZ = maxZ;
	}

	public void toggleSelection(CubeIndex ci) {
		if(ci==null) return;
		LayeredFace faceXy = getFaceXy(ci);
		faceXy.toggleSelection(ci);
		LayeredFace faceXz = getFaceXz(ci);
		faceXz.toggleSelection(ci);
		LayeredFace faceYx = getFaceYx(ci);
		faceYx.toggleSelection(ci);
		LayeredFace faceYz = getFaceYz(ci);
		faceYz.toggleSelection(ci);
		LayeredFace faceZx = getFaceZx(ci);
		faceZx.toggleSelection(ci);
		LayeredFace faceZy = getFaceZy(ci);
		faceZy.toggleSelection(ci);
	}
	
	public void setSelected(CubeIndex ci, boolean selected) {
		if(ci==null) return;
		LayeredFace faceXy = getFaceXy(ci);
		faceXy.setSelected(ci, selected);
		LayeredFace faceXz = getFaceXz(ci);
		faceXz.setSelected(ci, selected);
		LayeredFace faceYx = getFaceYx(ci);
		faceYx.setSelected(ci, selected);
		LayeredFace faceYz = getFaceYz(ci);
		faceYz.setSelected(ci, selected);
		LayeredFace faceZx = getFaceZx(ci);
		faceZx.setSelected(ci, selected);
		LayeredFace faceZy = getFaceZy(ci);
		faceZy.setSelected(ci, selected);
	}
	
	public void setUnderCursor(CubeIndex ci, boolean selected) {
		if(ci==null) return;
		LayeredFace faceXy = getFaceXy(ci);
		faceXy.setUnderCursor(ci, selected);
		LayeredFace faceXz = getFaceXz(ci);
		faceXz.setUnderCursor(ci, selected);
		LayeredFace faceYx = getFaceYx(ci);
		faceYx.setUnderCursor(ci, selected);
		LayeredFace faceYz = getFaceYz(ci);
		faceYz.setUnderCursor(ci, selected);
		LayeredFace faceZx = getFaceZx(ci);
		faceZx.setUnderCursor(ci, selected);
		LayeredFace faceZy = getFaceZy(ci);
		faceZy.setUnderCursor(ci, selected);
	}
	
	public boolean isSelected(CubeIndex ci) {
		if(ci==null) return false;		
		LayeredFace faceXy = getFaceXy(ci);
		if(faceXy.isSelected(ci)) return true;
		LayeredFace faceXz = getFaceXz(ci);
		if(faceXz.isSelected(ci)) return true;
		LayeredFace faceYx = getFaceYx(ci);
		if(faceYx.isSelected(ci)) return true;
		LayeredFace faceYz = getFaceYz(ci);
		if(faceYz.isSelected(ci)) return true;
		LayeredFace faceZx = getFaceZx(ci);
		if(faceZx.isSelected(ci)) return true;
		LayeredFace faceZy = getFaceZy(ci);
		if(faceZy.isSelected(ci)) return true;
		return false;
	}

	public LayeredFace[] getAllNeighbouringFaces(CubeIndex ci) {
		Vector<LayeredFace> faceVec = new Vector<LayeredFace>();
		LayeredFace faceXy = getFaceXy(ci);
		if(faceXy!=null) faceVec.add(faceXy);
		LayeredFace faceXz = getFaceXz(ci);
		if(faceXz!=null) faceVec.add(faceXz);
		LayeredFace faceYx = getFaceYx(ci);
		if(faceYx!=null) faceVec.add(faceYx);
		LayeredFace faceYz = getFaceYz(ci);
		if(faceYz!=null) faceVec.add(faceYz);
		LayeredFace faceZx = getFaceZx(ci);
		if(faceZx!=null) faceVec.add(faceZx);
		LayeredFace faceZy = getFaceZy(ci);
		if(faceZy!=null) faceVec.add(faceZy);
		LayeredFace[] faceArray = new LayeredFace[faceVec.size()];
		for(int i=0; i<faceVec.size(); i++) {
			faceArray[i] = faceVec.get(i);
		}
		return faceArray;
	}

	public int calculatePosXEdgeType(CubeIndex ci) {
		Face faceXy = getFaceXy(ci).getTop();
		Face faceXz = getFaceXz(ci).getTop();
		int typeXy = (getFaceXy(ci).isHidden() || faceXy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXy.getType());
		int typeXz = (getFaceXz(ci).isHidden() || faceXz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXz.getType());
		int edgeType = calculateXEdgeType(typeXz, typeXy);
		return edgeType;
	}

	public int calculatePosYEdgeType(CubeIndex ci) {
		Face faceYx = getFaceYx(ci).getTop();
		Face faceYz = getFaceYz(ci).getTop();
		int typeYx = (getFaceYx(ci).isHidden() || faceYx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYx.getType());
		int typeYz = (getFaceYz(ci).isHidden() || faceYz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYz.getType());
		int edgeType = calculateYEdgeType(typeYz, typeYx);
		return edgeType;
	}

	public int calculatePosZEdgeType(CubeIndex ci) {
		Face faceZx = getFaceZx(ci).getTop(); 
		Face faceZy = getFaceZy(ci).getTop();
		int typeZx = (getFaceZx(ci).isHidden() || faceZx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZx.getType());
		int typeZy = (getFaceZy(ci).isHidden() || faceZy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZy.getType());
		int edgeType = calculateZEdgeType(typeZx, typeZy);
		return edgeType;
	}

	private int calculateXEdgeType(int typeAbove, int typeBelow) {
		if(typeAbove==Face.FACE_NONE && typeBelow==Face.FACE_NONE) return Face.EDGE_NULL;
		else if (typeAbove==Face.FACE_NONE || typeBelow==Face.FACE_NONE) return Face.EDGE_SILHOUETTE;
		else if(typeAbove==typeBelow) {
			return 	(typeAbove==Face.FACE_X) ? Face.EDGE_FLAT_INTERIOR : Face.EDGE_FLAT_BOUNDARY;
		}
		else if(typeAbove==Face.FACE_X || typeBelow==Face.FACE_X) return Face.EDGE_SILHOUETTE;
		else if(typeAbove==Face.FACE_Z && typeBelow==Face.FACE_Y) return Face.EDGE_FORWARD;
		else if(typeAbove==Face.FACE_Y && typeBelow==Face.FACE_Z) return Face.EDGE_BACKWARD;
		else return Face.EDGE_IMPOSSIBLE;
	}

	private int calculateYEdgeType(int typeAbove, int typeBelow) {
		if(typeAbove==Face.FACE_NONE && typeBelow==Face.FACE_NONE) return Face.EDGE_NULL;
		else if (typeAbove==Face.FACE_NONE || typeBelow==Face.FACE_NONE) return Face.EDGE_SILHOUETTE;
		else if(typeAbove==typeBelow) {
			return 	(typeAbove==Face.FACE_Y) ? Face.EDGE_FLAT_INTERIOR : Face.EDGE_FLAT_BOUNDARY;
		}
		else if(typeAbove==Face.FACE_Y || typeBelow==Face.FACE_Y) return Face.EDGE_SILHOUETTE;
		else if(typeAbove==Face.FACE_Z && typeBelow==Face.FACE_X) return Face.EDGE_FORWARD;
		else if(typeAbove==Face.FACE_X && typeBelow==Face.FACE_Z) return Face.EDGE_BACKWARD;
		else return Face.EDGE_IMPOSSIBLE;
	}

	private int calculateZEdgeType(int typeLeft, int typeRight) {
		if(typeLeft==Face.FACE_NONE && typeRight==Face.FACE_NONE) return Face.EDGE_NULL;
		else if (typeLeft==Face.FACE_NONE || typeRight==Face.FACE_NONE) return Face.EDGE_SILHOUETTE;
		else if(typeLeft==typeRight) {
			return 	(typeLeft==Face.FACE_Z) ? Face.EDGE_FLAT_INTERIOR : Face.EDGE_FLAT_BOUNDARY;
		}
		else if(typeLeft==Face.FACE_Z || typeRight==Face.FACE_Z) return Face.EDGE_SILHOUETTE;
		else if(typeLeft==Face.FACE_X && typeRight==Face.FACE_Y) return Face.EDGE_FORWARD;
		else if(typeLeft==Face.FACE_Y && typeRight==Face.FACE_X) return Face.EDGE_BACKWARD;
		else return Face.EDGE_IMPOSSIBLE;
	}
	
	public boolean edgeIsAmbiguous(int edgeType) {
		return edgeType==Face.EDGE_FLAT_BOUNDARY;
	}
	
	public void updatePosXEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		int[] types = {calculatePosXEdgeType(ci), calculateFacePosXEdgeIsOn(ci)};
		if(favourSilhouette && edgeIsAmbiguous(types[0])) types[0] = Face.EDGE_SILHOUETTE;
		edgeTypeArray.setPosXEdgeType(ci, types);
	}
	
	public void updatePosYEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		int[] types = {calculatePosYEdgeType(ci), calculateFacePosYEdgeIsOn(ci)};
		if(favourSilhouette && edgeIsAmbiguous(types[0])) types[0] = Face.EDGE_SILHOUETTE;
		edgeTypeArray.setPosYEdgeType(ci, types);
	}

	public void updatePosZEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		int[] types = {calculatePosZEdgeType(ci), calculateFacePosZEdgeIsOn(ci)};
		if(favourSilhouette && edgeIsAmbiguous(types[0])) types[0] = Face.EDGE_SILHOUETTE;
		edgeTypeArray.setPosZEdgeType(ci, types);
	}
	
	public void updateNegXEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		updatePosXEdgeAndFaceType(ci.getNegXNeighbour(), favourSilhouette);
	}
	
	public void updateNegYEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		updatePosYEdgeAndFaceType(ci.getNegYNeighbour(), favourSilhouette);
	}

	public void updateNegZEdgeAndFaceType(CubeIndex ci, boolean favourSilhouette) {
		updatePosZEdgeAndFaceType(ci.getNegZNeighbour(), favourSilhouette);
	}

	public int[] getPosXEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosXEdgeType(ci);
	}
	
	public int[] getPosYEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosYEdgeType(ci);
	}

	public int[] getPosZEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosZEdgeType(ci);
	}

	public int[] getNegXEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosXEdgeType(ci.getNegXNeighbour());
	}

	public int[] getNegYEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosYEdgeType(ci.getNegYNeighbour());
	}

	public int[] getNegZEdgeAndFaceType(CubeIndex ci) {
		return edgeTypeArray.getPosZEdgeType(ci.getNegZNeighbour());
	}

	private int getFaceType(int type1, int type2) {
		if(type1==Face.FACE_X) {
			if(type2==Face.FACE_X) return Face.FACE_X;
			else if(type2==Face.FACE_Y) return Face.FACE_XY;
			else if(type2==Face.FACE_Z) return Face.FACE_XZ;
		}
		else if(type1==Face.FACE_Y) {
			if(type2==Face.FACE_X) return Face.FACE_YX;
			else if(type2==Face.FACE_Y) return Face.FACE_Y;
			else if(type2==Face.FACE_Z) return Face.FACE_YZ;
		}
		else if(type1==Face.FACE_Z) {
			if(type2==Face.FACE_X) return Face.FACE_ZX;
			else if(type2==Face.FACE_Y) return Face.FACE_ZY;
			else if(type2==Face.FACE_Z) return Face.FACE_Z;
		}
		return Face.FACE_NONE;
	}
	
	// get the type of face the x-edge is on
	private int calculateFacePosXEdgeIsOn(CubeIndex ci) {
		Face faceXy = getFaceXy(ci).getTop();
		Face faceXz = getFaceXz(ci).getTop();
		int typeXy = (getFaceXy(ci).isHidden() || faceXy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXy.getType());
		int typeXz = (getFaceXz(ci).isHidden() || faceXz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXz.getType());
		return getFaceType(typeXy, typeXz);
	}
	
	private int calculateFacePosYEdgeIsOn(CubeIndex ci) {
		Face faceYx = getFaceYx(ci).getTop();
		Face faceYz = getFaceYz(ci).getTop();
		int typeYx = (getFaceYx(ci).isHidden() || faceYx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYx.getType());
		int typeYz = (getFaceYz(ci).isHidden() || faceYz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYz.getType());
		return getFaceType(typeYx, typeYz);
	}

	private int calculateFacePosZEdgeIsOn(CubeIndex ci) {
		Face faceZx = getFaceZx(ci).getTop(); 
		Face faceZy = getFaceZy(ci).getTop();
		int typeZx = (getFaceZx(ci).isHidden() || faceZx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZx.getType());
		int typeZy = (getFaceZy(ci).isHidden() || faceZy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZy.getType());
		return getFaceType(typeZx, typeZy);
	}

	public int[][] getAllEdgeTypes(CubeIndex ci, FaceArray faces) {
		int[] posXType = faces.getPosXEdgeAndFaceType(ci);
		int[] negXType = faces.getNegXEdgeAndFaceType(ci);
		int[] posYType = faces.getPosYEdgeAndFaceType(ci);
		int[] negYType = faces.getNegYEdgeAndFaceType(ci);
		int[] posZType = faces.getPosZEdgeAndFaceType(ci);
		int[] negZType = faces.getNegZEdgeAndFaceType(ci);
		
		//int[][] edgeTypesPrelim = {posXType, negXType, posYType, negYType, posZType, negZType};
		//int[][] edgeTypes = fixSilhouetteEdgeProblems(edgeTypesPrelim);
		int[][] edgeTypes = {posXType, negXType, posYType, negYType, posZType, negZType};
		return edgeTypes;
	}

	public void setPosXEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setPosXEdgeType(ci, edgeType);
	}
	public void setPosYEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setPosYEdgeType(ci, edgeType);
	}
	public void setPosZEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setPosZEdgeType(ci, edgeType);
	}
	public void setNegXEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setNegXEdgeType(ci, edgeType);
	}
	public void setNegYEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setNegYEdgeType(ci, edgeType);
	}
	public void setNegZEdgeType(CubeIndex ci, int edgeType) {
		edgeTypeArray.setNegZEdgeType(ci, edgeType);
	}
	
	/*
	public CubeIndex[] getAllNeighbouringFaceIndices(CubeIndex ci) {
		Vector<CubeIndex> indsVec = new Vector<CubeIndex>();
		Face faceXy = getFaceXy(ci);
		if(faceXy!=null) indsVec.add(faceXy.getCubeIndex());
		Face faceXz = getFaceXz(ci);
		if(faceXz!=null) indsVec.add(faceXz.getCubeIndex());
		Face faceYx = getFaceYx(ci);
		if(faceYx!=null) indsVec.add(faceYx.getCubeIndex());
		Face faceYz = getFaceYz(ci);
		if(faceYz!=null) indsVec.add(faceYz.getCubeIndex());
		Face faceZx = getFaceZx(ci);
		if(faceZx!=null) indsVec.add(faceZx.getCubeIndex());
		Face faceZy = getFaceZy(ci);
		if(faceZy!=null) indsVec.add(faceZy.getCubeIndex());
		CubeIndex[] indsArray = new CubeIndex[indsVec.size()];
		for(int i=0; i<indsVec.size(); i++) {
			indsArray[i] = indsVec.get(i);
		}
		return indsArray;
	}
	*/
	
	
}
