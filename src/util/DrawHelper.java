package util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import colours.Colour;

public class DrawHelper {

	// cube settings
	private int side;
	private int[] vx = new int[2] , vy = new int[2], vz = new int[2];
	public double[] mx = new double[2] , my = new double[2], mz = new double[2];
	private String importFileName = "textures\\custom.png";
	private BufferedImage bi = DrawHelper.getTransparentImage(1, 1);
	private TexturePaint tp  = new TexturePaint(bi, new Rectangle(0,0,1,1));;
	public static final int FILL_COLOUR = 0, FILL_COLOUR_AND_PATTERN = 1, FILL_PATTERN = 2, FILL_TEXTURE = 3;
	private int fillStyle = FILL_TEXTURE;
	private boolean reloadTexture = true;
	public static final int STYLE_OP_ART = 0, STYLE_SPAGHETTI = 1, STYLE_KNOT = 2, STYLE_HOLEY_CHAIN = 3,
				STYLE_OP_ART_2 = 4, STYLE_SHAPES = 5, STYLE_STAR = 6, STYLE_CURVY_STAR = 7, STYLE_CUBISM = 8,
				STYLE_CUSTOM = 9;
	private String cubismFileName = "textures\\cubism.png";
	private String customFileName = "textures\\custom.png";

	public DrawHelper(int side, double[] mx, double[] my, double[] mz, int fillStyle) {
		this.side = side;
		this.mx[0] = mx[0]; this.mx[1] = mx[1];
		this.my[0] = mx[0]; this.my[1] = my[1];
		this.mz[0] = mx[0]; this.mz[1] = mz[1];
		double totalX = -mx[0]+my[0];
		double totalY = mx[1]+my[1]-mz[1];
		double hypotenuse = -mz[1];
		vx[0] = (int) Math.round(mx[0]/hypotenuse*(double)side);
		vx[1] = (int) Math.round(mx[1]/hypotenuse*(double)side);
		vy[0] = (int) Math.round(my[0]/hypotenuse*(double)side);
		vy[1] = (int) Math.round(my[1]/hypotenuse*(double)side);
		vz[0] = (int) Math.round(mz[0]/hypotenuse*(double)side);
		vz[1] = (int) Math.round(mz[1]/hypotenuse*(double)side);
		//System.out.println("C: vx = " + vx[0] + ", " + vx[1]);
		this.fillStyle = fillStyle;
	}
	
	public static BufferedImage getTransparentImage(int width, int height) {
		BufferedImage im = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		clearTransparentImage(im);
		return im;
	}
	
	public static void clearTransparentImage(BufferedImage im) {
		int tw = new Color(255,255,255,0).getRGB() ;
		for (int x = 0 ; x < im.getWidth() ; x++) {
			for (int y = 0 ; y < im.getHeight() ; y++) {
					im.setRGB(x, y, tw) ;
			}
		}
	}
	
	public void addPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType, int style) {
		Shape clip0 = g.getClip();
		Polygon clip = this.getSingleCubeClippingMask(g, xx0, yy0, faceType);
		g.fill(clip);
		g.setClip(clip);
		switch(style) {
		case STYLE_OP_ART:
			g.setColor(Color.white);
			g.fill(clip);
			addOpArtPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_SPAGHETTI:
			g.setColor(Color.white);
			//g.fill(clip);
			addSpaghettiPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_KNOT:
			g.setColor(Color.white);
			g.fill(clip);
			addKnotPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_HOLEY_CHAIN:
			addHoleyChainPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_OP_ART_2:
			g.setColor(Color.white);
			g.fill(clip);
			addOpArt2PatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_SHAPES:
			g.setColor(Color.white);
			g.fill(clip);
			addShapePatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_STAR:
			g.setColor(new Color(255,255,255,40));
			g.fill(clip);
			addStarPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_CURVY_STAR:
			g.setColor(new Color(255,255,255,80));
			g.fill(clip);
			addCurvyStarPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_CUBISM:
			//g.setColor(new Color(255,255,255,80));
			//g.fill(clip);
			addCubismPatternToFace(g, xx0, yy0, side, faceType);
			break;
		case STYLE_CUSTOM:
			//g.setColor(new Color(255,255,255,80));
			//g.fill(clip);
			addCustomPatternToFace(g, xx0, yy0, side, faceType);
			break;
		}		
		g.setClip(clip0);
	}

	public static void drawScaledImage(Graphics2D g, BufferedImage image, int scale, int x, int y) {
		if(image != null) {
			AffineTransform at = AffineTransform.getScaleInstance(scale, scale);
			AffineTransformOp atOp = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			g.drawImage(image, atOp, x, y);
		}
		else {
			System.out.println("null");
		}
	}
	
	public int[][] getXAndYVectors() {
		int[][] vectors = {vx, vy};
		return vectors;
	}
	
	public static void drawImage(Graphics2D g, BufferedImage image, int x, int y) {
		if(image != null) {
			g.drawImage(image, null, x, y);
		}
		else {
			System.out.println("null");
		}
	}

	public void drawRightTriangularFaceWithColour(Graphics2D g, int x0, int y0, int length, Color c, int faceType) {
		g.setColor(c);
		Polygon triangle = new Polygon();
		if(faceType == Face.FACE_YZ) {
			triangle.addPoint(x0, y0);
			int x1 = x0-vx[0], y1 = y0-vx[1];
			triangle.addPoint(x1, y1);
			int x2 = x1-vz[0], y2 = y1-vz[1];
			triangle.addPoint(x2, y2);
		}
		else if(faceType == Face.FACE_XY) {
			triangle.addPoint(x0, y0);
			int x1 = x0+vy[0], y1 = y0+vy[1];
			triangle.addPoint(x1, y1);
			int x2 = x1+vz[0], y2 = y1+vz[1];
			triangle.addPoint(x2, y2);
		}
		else if(faceType == Face.FACE_ZX) {
			triangle.addPoint(x0, y0);
			int x1 = x0+vy[0], y1 = y0+vy[1];
			triangle.addPoint(x1, y1);
			int x2 = x0-vx[0], y2 = y0-vx[1];
			triangle.addPoint(x2, y2);
		}
		g.fillPolygon(triangle);
		// cover seams
		for(int i=0; i<0; i++) {
			g.drawPolygon(triangle);
		}
	}
	
	public void drawLeftTriangularFaceWithColour(Graphics2D g, int x0, int y0, int length, Color c, int faceType) {
		g.setColor(c);
		Polygon triangle = new Polygon();
		if(faceType==Face.FACE_XZ) {
			triangle.addPoint(x0, y0);
			int x1 = x0-vy[0], y1 = y0-vy[1];
			triangle.addPoint(x1, y1);
			int x2 = x1-vz[0], y2 = y1-vz[1];
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_YX) {
			triangle.addPoint(x0, y0);
			int x1 = x0+vx[0], y1 = y0+vx[1];
			triangle.addPoint(x1, y1);
			int x2 = x1+vz[0], y2 = y1+vz[1];
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_ZY) {
			triangle.addPoint(x0, y0);
			int x1 = x0+vx[0], y1 = y0+vx[1];
			triangle.addPoint(x1, y1);
			int x2 = x0-vy[0], y2 = y0-vy[1];
			triangle.addPoint(x2, y2);
		}
		g.fillPolygon(triangle);
		// cover seams
		for(int i=0; i<0; i++) {
			g.drawPolygon(triangle);
		}
	}
	
	private Polygon getFacePolygon(int xx0, int yy0, int faceType, boolean inclusive) {
		Polygon triangle = new Polygon();
		if(faceType==Face.FACE_XZ) {
			int x0 = xx0, y0 = yy0;
			int x1 = x0-vy[0]-vz[0], y1 = y0-vy[1]-vz[1];
			int x2 = x1+vz[0], y2 = y1+vz[1];
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_XY) {
			int x0 = xx0+vy[0]+vz[0], y0 = yy0+vy[1]+vz[1];
			int x1 = xx0, y1 = yy0;
			int x2 = x1+vy[0], y2 = y1+vy[1];
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_YX) {
			int x0 = xx0+vx[0]+vz[0], y0 = yy0+vx[1]+vz[1];
			int x1 = xx0, y1 = yy0;
			int x2 = x1+vx[0], y2 = y1+vx[1];
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_YZ) {
			int x0 = xx0, y0 = yy0;
			int x1 = x0-vx[0], y1 = y0-vx[1];
			int x2 = x1-vz[0], y2 = y1-vz[1];
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_ZX) {
			int x0 = xx0+vy[0], y0 = yy0+vy[1];
			int x1 = xx0, y1 = yy0;
			int x2 = x1-vx[0], y2 = y1-vx[1];
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		else if(faceType==Face.FACE_ZY) {
			int x0 = xx0+vx[0], y0 = yy0+vx[1];
			int x1 = xx0, y1 = yy0;
			int x2 = x1-vy[0], y2 = y1-vy[1];
			if(inclusive) {
			//	x1+=1;
			//	y2+=1;
			}
			triangle.addPoint(x0, y0);
			triangle.addPoint(x1, y1);
			triangle.addPoint(x2, y2);
		}
		return triangle;
	}
	
	private int[] getCubeCentreOffset() {
		int[] offset = {Math.abs(vx[0])+1, Math.abs(vx[1])+Math.abs(vy[1])+1};
		return offset;
	}
	
	public void drawLeftTriangularFace(Graphics2D g, int xx0, int yy0, int length, int faceType, Color c) {
		if(fillStyle == FILL_COLOUR)
			drawLeftTriangularFaceWithColour(g, xx0, yy0, length, c, faceType);
		else if(fillStyle == FILL_TEXTURE)
			drawLeftTriangularFaceWithTexture(g, xx0, yy0, length, faceType);
	}
	
	public void drawRightTriangularFace(Graphics2D g, int xx0, int yy0, int length, int faceType, Color c) {
		if(fillStyle == FILL_COLOUR)
			drawRightTriangularFaceWithColour(g, xx0, yy0, length, c, faceType);
		else if(fillStyle == FILL_TEXTURE)
			drawRightTriangularFaceWithTexture(g, xx0, yy0, length, faceType);
	}

	public void addOpArtPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=0; i<=6; i++) {
			Polygon p = new Polygon();
			p.addPoint((int) -Math.round((double)vx[0]*(2.0*i+1.0)/6.0), (int) -Math.round((double)vx[1]*(2.0*i+1.0)/6.0));
			p.addPoint((int) -Math.round((double)vy[0]*(2.0*i+1.0)/6.0), (int) -Math.round((double)vy[1]*(2.0*i+1.0)/6.0));
			p.addPoint((int) -Math.round((double)vz[0]*(2.0*i+1.0)/6.0), (int) -Math.round((double)vz[1]*(2.0*i+1.0)/6.0));
			g.drawPolygon(p);
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addSpaghettiPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		Color[] blues2 = {IsoColours.medBlue, IsoColours.brightestBlue, IsoColours.white};
		Color[] blues = {IsoColours.medBlue, IsoColours.darkBlue, IsoColours.brightBlue};

		for(int i=6; i>=1; i--) {
			int radiusY = (int) -Math.round((double)vz[1]*(2.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(2.0*i)/12.0*1.02);
/*
			if(i<10) {
		g.setColor(blues[(int)Math.min(i, 7-i)-1]);
		//if(i==1) g.setColor(Color.black);
		// top left
		g.fillOval(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
			}
			
		if(i<6) {
				//g.setColor(Color.black);
				g.setColor(blues2[(int)Math.min(Math.min(i, 6-i)-1, blues2.length-1)]);
				// top left
				g.drawOval(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
			}
			*/
		}
		
		/*
		for(int i=5; i>=1; i--) {
			int radiusY = (int) -Math.round((double)vz[1]*(2.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(2.0*i)/12.0*1.02);
			// centre
			g.setColor(blues[(int)Math.min(i, 7-i)-1]);
			if(i==1) g.setColor(Color.black);
			g.fillOval(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
			g.setColor(blues2[(int)Math.min(Math.min(i, 6-i), blues2.length-1)]);
			g.drawOval(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
			//if(i==1) g.drawOval(-1, -1, 2, 2);
		}
		*/
		/*
		g.setColor(Color.white);
		for(int i=4; i>=4; i--) {
			int radiusY = (int) -Math.round((double)vz[1]*(2.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(2.0*i)/12.0*1.02);
			// top left
			g.fillOval(vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// top right
			g.fillOval(-vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// bottom
			g.fillOval(-vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY);
		}
		*/
		
		//g.setColor(Color.black);
		for(int i=6; i>=1; i--) {
			int radiusY = (int) -Math.round((double)vz[1]*(2.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(2.0*i)/12.0*1.02);

			if(i<6) {
		g.setColor(blues[(int)Math.min(i, 7-i)-1]);
		if(i==1) g.setColor(Color.black);
		// top left
		g.fillOval(vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
		//if(i==1) g.drawOval(vx[0]-1, vx[0]-1, 2, 2);
		// top right
		g.fillOval(-vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
		//if(i==1) g.drawOval(-vx[0]-1, -vx[0]-1, 2, 2);
		// bottom
		g.fillOval(-vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY);
		//if(i==1) g.drawOval(-vx[0]-vy[0]-1, vx[1]+vy[1]-1, 2, 2);
			}
			
		if(i<6) {
				//g.setColor(Color.black);
				g.setColor(blues2[(int)Math.min(Math.min(i, 6-i)-1, blues2.length-1)]);
				// top left
				g.drawOval(vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
				//if(i==1) g.drawOval(vx[0]-1, vx[0]-1, 2, 2);
				// top right
				g.drawOval(-vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
				//if(i==1) g.drawOval(-vx[0]-1, -vx[0]-1, 2, 2);
				// bottom
				g.drawOval(-vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY);
				//if(i==1) g.drawOval(-vx[0]-vy[0]-1, vx[1]+vy[1]-1, 2, 2);
			}
		}
		g.setColor(Color.black);
		
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addCubismPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		BufferedImage texture = getTransparentImage(1,1);
		try{
			texture = ImageIO.read(new File(cubismFileName));
        }
		catch (IOException ex) {
			System.out.println("exception in DrawHelper");
        }
		g.drawImage(texture, vx[0]-1, vz[1]-1,  null);
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addCustomPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		BufferedImage texture = getTransparentImage(1,1);
		try{
			texture = ImageIO.read(new File(customFileName));
        }
		catch (IOException ex) {
			System.out.println("exception in DrawHelper");
        }
		g.drawImage(texture, vx[0], vz[1],  null);
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addHoleyChainPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=6; i<=6; i++) {
			int radiusY = (int) -Math.round((double)vz[1]*(1.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(1.0*i)/12.0*1.001);
			int radiusY2 = (int) (radiusY*1.7);
			int radiusX2 = (int) (radiusX*1.7);
			// centre
			g.setColor(Color.black);
			fillOvalFromCentre(g, 5*radiusX, 5*radiusY);
			
			g.setColor(Color.white);
			// top left and bottom right
			fillOvalFromCentre(g, vx[0], -vx[1], radiusX2, radiusY2);
			// top right and bottom left
			fillOvalFromCentre(g, -vx[0], -vx[1], radiusX2, radiusY2);
			// top and bottom
			fillOvalFromCentre(g, vz[0], -vz[1], radiusX2, radiusY2);
			
			g.setColor(Color.black);
			// top left and bottom right
			fillOvalFromCentre(g, vx[0], -vx[1], radiusX/2, radiusY/2);
			// top right and bottom left
			fillOvalFromCentre(g, -vx[0], -vx[1], radiusX/2, radiusY/2);
			// top and bottom
			fillOvalFromCentre(g, vz[0], -vz[1], radiusX/2, radiusY/2);

			// top left and bottom right
			fillOvalFromCentre(g, vx[0]/2, -vx[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, vx[0]/2+vx[0]/2, -vx[1]/2+vx[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, vx[0]/2+vz[0]/2, -vx[1]/2+vz[1]/2, radiusX/3, radiusY/3);
			// top right and bottom left
			fillOvalFromCentre(g, -vx[0]/2, -vx[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, -vx[0]/2+vy[0]/2, -vx[1]/2+vy[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, -vx[0]/2+vz[0]/2, -vx[1]/2+vz[1]/2, radiusX/3, radiusY/3);
			// top and bottom
			fillOvalFromCentre(g, vz[0]/2, -vz[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, vz[0]/2+vx[0]/2, -vz[1]/2+vx[1]/2, radiusX/3, radiusY/3);
			fillOvalFromCentre(g, vz[0]/2+vy[0]/2, -vz[1]/2+vy[1]/2, radiusX/3, radiusY/3);
			
			
			//g.drawLine(0, 0, 2*vx[0], 2*vx[1]);
			//g.drawLine(0, 0, 2*vy[0], 2*vy[1]);
			//g.drawLine(0, 0, 2*vz[0], 2*vz[1]);
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public static void fillOvalFromCentre(Graphics2D g, int radiusX, int radiusY) {
		g.fillOval(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
	}
	
	public static void fillOvalFromCentre(Graphics2D g, int x, int y, int radiusX, int radiusY) {
		g.fillOval(x-radiusX, y-radiusY, 2*radiusX, 2*radiusY);
	}

	public static void fillRectFromCentre(Graphics2D g, int radiusX, int radiusY) {
		g.fillRect(-radiusX, -radiusY, 2*radiusX, 2*radiusY);
	}
	
	public static void fillRectFromCentre(Graphics2D g, int x, int y, int radiusX, int radiusY) {
		g.fillRect(x-radiusX, y-radiusY, 2*radiusX, 2*radiusY);
	}

	public static void fillDiamondFromCentre(Graphics2D g, int radiusX, int radiusY) {
		Polygon p = new Polygon();
		p.addPoint(- radiusX, 	0);
		p.addPoint(0, 			- radiusY);
		p.addPoint(radiusX, 	0);
		p.addPoint(0, 			+ radiusY);
		g.fillPolygon(p);
	}
	
	public static void fillDiamondFromCentre(Graphics2D g, int x, int y, int radiusX, int radiusY) {
		Polygon p = new Polygon();
		p.addPoint(x- radiusX, 	y);
		p.addPoint(x, 			y- radiusY);
		p.addPoint(x+radiusX, 	y);
		p.addPoint(x, 			y+ radiusY);
		g.fillPolygon(p);
	}

	
	public void addOpArt2PatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		double d = 8.0;
		for(int i=0; i<=(int)d-1; i++) {
			Polygon p = new Polygon();
			int x1 = (int) Math.round(-(double)vx[0]*(2.0*i+1.0)/d);
			int y1 = (int) Math.round(-(double)vx[1]*(2.0*i+1.0)/d);
			int x2 = 0;//(int) Math.round((double)vz[0]*(2.0*i+1.0)/d);
			int y2 = (int) Math.round((double)vz[1]*(2.0*i+1.0)/d);
			int x3 = -x1;//(int) Math.round(-(double)vy[0]*(2.0*i+1.0)/d);
			int y3 = y1;//(int) Math.round(-(double)vy[1]*(2.0*i+1.0)/d);
			p.addPoint(x1, y1);
			p.addPoint(x2, y2);
			p.addPoint(x3, y3);
			p.addPoint(-x1, -y1);
			p.addPoint(-x2, -y2);
			p.addPoint(-x3, -y3);
			g.translate(-2*vx[0], -2*vx[1]); g.drawPolygon(p); g.translate(2*vx[0], 2*vx[1]);
			g.translate(-2*vy[0], -2*vy[1]); g.drawPolygon(p); g.translate(2*vy[0], 2*vy[1]);
			g.translate(-2*vz[0], -2*vz[1]); g.drawPolygon(p); g.translate(2*vz[0], 2*vz[1]);
			
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addShapePatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(3.1f));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		boolean drawLines = true;
		boolean drawShapes = true;
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=6; i<=6; i++) {
			int radiusY = (int) -Math.round((double)vz[1]*(1.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(1.0*i)/12.0*1.001);
			int radiusY2 = (int) (radiusY/2.5);
			int radiusX2 = (int) (radiusX/2.5);
			// centre
			g.setColor(Color.black);
			fillOvalFromCentre(g, 5*radiusX, 5*radiusY);
			
			// top left and bottom right
			if(drawLines) {
			g.setColor(Color.pink.darker().darker().darker());
			g.drawLine(vx[0]/2, -vx[1]/2, vx[0]/2-vx[0], -vx[1]/2-vx[1]);
			g.drawLine(vx[0]/2, -vx[1]/2, vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			}
			if(drawShapes) {
			g.setColor(Color.pink);
			g.rotate(Math.asin(0.5), vx[0]/2, -vx[1]/2);
			fillOvalFromCentre(g, vx[0]/2, -vx[1]/2, radiusX2, radiusY2);
			g.rotate(-Math.asin(0.5), vx[0]/2, -vx[1]/2);
			g.rotate(Math.asin(0.5), vx[0]/2-vx[0], -vx[1]/2-vx[1]);
			fillRectFromCentre(g, vx[0]/2-vx[0], -vx[1]/2-vx[1], radiusX2, radiusY2);
			g.rotate(-Math.asin(0.5), vx[0]/2-vx[0], -vx[1]/2-vx[1]);
			g.rotate(Math.asin(0.5), vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			fillDiamondFromCentre(g, vx[0]/2-vz[0], -vx[1]/2-vz[1], radiusX2, radiusY2);
			g.rotate(-Math.asin(0.5), vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			}
			// top right and bottom left
			if(drawLines) {
			g.setColor(Color.yellow.darker().darker().darker());
			g.drawLine(-vx[0]/2, -vx[1]/2, -vx[0]/2-vy[0], -vx[1]/2-vy[1]);
			g.drawLine(-vx[0]/2, -vx[1]/2, -vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			}
			if(drawShapes) {
			g.setColor(Color.yellow);
			g.rotate(-Math.asin(0.5), -vx[0]/2, -vx[1]/2);
			fillDiamondFromCentre(g, -vx[0]/2, -vx[1]/2, radiusX2, radiusY2);
			g.rotate(Math.asin(0.5), -vx[0]/2, -vx[1]/2);
			g.rotate(-Math.asin(0.5), -vx[0]/2-vy[0], -vx[1]/2-vy[1]);
			fillDiamondFromCentre(g, -vx[0]/2-vy[0], -vx[1]/2-vy[1], radiusX2, radiusY2);
			g.rotate(Math.asin(0.5), -vx[0]/2-vy[0], -vx[1]/2-vy[1]);
			g.rotate(-Math.asin(0.5), -vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			fillOvalFromCentre(g, -vx[0]/2-vz[0], -vx[1]/2-vz[1], radiusX2, radiusY2);
			g.rotate(Math.asin(0.5), -vx[0]/2-vz[0], -vx[1]/2-vz[1]);
			}

			// top and bottom
			if(drawLines) {
			g.setColor(Color.cyan.darker().darker().darker());
			g.drawLine(vz[0]/2, -vz[1]/2, vz[0]/2-vx[0], -vz[1]/2-vx[1]);
			g.drawLine(vz[0]/2, -vz[1]/2, vz[0]/2-vy[0], -vz[1]/2-vy[1]);
			}
			if(drawShapes) {
			g.setColor(Color.cyan);
			fillDiamondFromCentre(g, vz[0]/2, -vz[1]/2, radiusX2, radiusY2);
			fillOvalFromCentre(g, vz[0]/2-vx[0], -vz[1]/2-vx[1], radiusX2, radiusY2);
			fillRectFromCentre(g, vz[0]/2-vy[0], -vz[1]/2-vy[1], radiusX2, radiusY2);
			}
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addStarPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g.setColor(Color.black);
		g.setColor(new Color(0,0,0,64));
		g.setStroke(new BasicStroke(3.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=6; i<=6; i++) {
			int x1 = (int) -Math.round((double)vx[0]*(1.0*i)/6.0);
			int y1 = (int) -Math.round((double)vx[1]*(1.0*i)/6.0);
			int x2 = -x1;
			int y2 = y1;
			int x3 = 0;
			int y3 = (int) -Math.round((double)vz[1]*(1.0*i)/6.0);
			Polygon p = new Polygon();
			p.addPoint(x1, y1);
			p.addPoint(x2, y2);
			p.addPoint(x3, y3);
			g.fillPolygon(p);
			Polygon q = new Polygon();
			q.addPoint(x1, -y1);
			q.addPoint(x2, -y2);
			q.addPoint(x3, -y3);
			g.fillPolygon(q);
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	public void addCurvyStarPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g.setColor(Color.black);
		g.setColor(new Color(0,0,0,64));
		g.setStroke(new BasicStroke(3.1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=6; i<=6; i++) {
			int x1 = (int) -Math.round((double)vx[0]*(1.0*i)/6.0);
			int y1 = (int) -Math.round((double)vx[1]*(1.0*i)/6.0);
			int x2 = -x1;
			int y2 = y1;
			int x3 = 0;
			int y3 = (int) -Math.round((double)vz[1]*(1.0*i)/6.0);
			GeneralPath p = new GeneralPath();
			p.moveTo(x1, y1);
			p.curveTo(0, 0, -x3, -y3, x2, y2);
			p.curveTo(0, 0, -x1, -y1, x3, y3);
			p.curveTo(0, 0, -x2, -y2, x1, y1);
			//g.setColor(new Color(255,128,0,64));
			g.fill(p);
			GeneralPath q = new GeneralPath();
			q.moveTo(-x1, -y1);
			q.curveTo(0, 0, x3, y3, -x2, -y2);
			q.curveTo(0, 0, x1, y1, -x3, -y3);
			q.curveTo(0, 0, x2, y2, -x1, -y1);
			//g.setColor(new Color(0,255,255,64));
			g.fill(q);

		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}
	
	private void drawHaloArc(Graphics2D g, int x, int y, int width, int height, int startAngle, int arcAngle) {
		float thickness = 4.1f;
		
		g.setColor(Color.white);
		g.setStroke(new BasicStroke(thickness*2.0f));
		g.drawArc(x, y, width, height, startAngle, arcAngle);
		
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(thickness));
		g.drawArc(x, y, width, height, startAngle, arcAngle);
	}
	
	private void drawHaloCircle(Graphics2D g, int x, int y, int width, int height) {
		float thickness = 4.1f;
		
		g.setColor(Color.white);
		g.setStroke(new BasicStroke(thickness*2.0f));
		g.drawOval(x, y, width, height);
		
		g.setColor(Color.black);
		g.setStroke(new BasicStroke(thickness));
		g.drawOval(x, y, width, height);		
	}
	
	public void addKnotPatternToFace(Graphics2D g, int xx0, int yy0, int side, int faceType) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		g.translate(triangle.xpoints[0], triangle.ypoints[0]);
		for(int i=9; i<=9; i++) {
			int radiusY = (int) -Math.round((double)vz[1]*(1.0*i)/12.0);
			int radiusX = (int) Math.round((double)Math.sqrt((double)(vy[0]*vy[0]+vy[1]*vy[1]))*(1.0*i)/12.0*1.02);
			/*
			// centre
			drawHaloCircle(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY);
			// top left
			drawHaloCircle(g, vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// top
			drawHaloCircle(g, -vx[0]-vy[0]-radiusX, -vx[1]-vy[1]-radiusY, 2*radiusX, 2*radiusY);
			// top right
			drawHaloCircle(g, -vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// bottom left
			drawHaloCircle(g, vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// bottom right
			drawHaloCircle(g, -vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY);
			// bottom
			drawHaloCircle(g, -vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY);
			 */
			int angle = 30;
			
			// centre
			/*
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 60, angle);
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 120, angle);
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 180, angle);
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 240, angle);
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 300, angle);
			drawHaloArc(g, -radiusX, -radiusY, 2*radiusX, 2*radiusY, 0, angle);
			*/
			
			// top left
			drawHaloArc(g, vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY, 0-10, angle+10);
			// top
			drawHaloArc(g, -vx[0]-vy[0]-radiusX, -vx[1]-vy[1]-radiusY, 2*radiusX, 2*radiusY, 300-10, angle+10);
			// top right
			drawHaloArc(g, -vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY, 240-10, angle+10);
			// bottom right
			drawHaloArc(g, -vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY, 180-10, angle+10);
			// bottom
			drawHaloArc(g, -vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY, 120-10, angle+10);
			// bottom left
			drawHaloArc(g, vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY, 60-10, angle+10);

			// top left
			drawHaloArc(g, vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY, 300-angle, 2*angle);
			// top
			drawHaloArc(g, -vx[0]-vy[0]-radiusX, -vx[1]-vy[1]-radiusY, 2*radiusX, 2*radiusY, 240-angle, 2*angle);
			// top right
			drawHaloArc(g, -vx[0]-radiusX, -vx[1]-radiusY, 2*radiusX, 2*radiusY, 180-angle, 2*angle);
			// bottom right
			drawHaloArc(g, -vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY, 120-angle, 2*angle);
			// bottom
			drawHaloArc(g, -vx[0]-vy[0]-radiusX, vx[1]+vy[1]-radiusY, 2*radiusX, 2*radiusY, 60-angle, 2*angle);
			// bottom left
			drawHaloArc(g, vx[0]-radiusX, vx[1]-radiusY, 2*radiusX, 2*radiusY, 0-angle, 2*angle);
		}
		g.translate(-triangle.xpoints[0], -triangle.ypoints[0]);
		g.setStroke(new BasicStroke(1));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
	}

	public void drawLeftTriangularFaceWithTexture(Graphics2D g, int xx0, int yy0, int length, int faceType) {
		//g.setColor(rect);
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		int[] offset2 = getCubeCentreOffset();
		if(reloadTexture) updateTextureSource();
   		tp = new TexturePaint(bi, new Rectangle(triangle.xpoints[0]+offset2[0],triangle.ypoints[0]+offset2[1],bi.getWidth(),bi.getHeight()));
		g.setPaint(tp);
		g.fillPolygon(triangle);
		
	}

	public void drawRightTriangularFaceWithTexture(Graphics2D g, int xx0, int yy0, int length, int faceType) {
		//g.setColor(rect);
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		int[] offset2 = getCubeCentreOffset();
		if(reloadTexture) updateTextureSource();
   		tp = new TexturePaint(bi, new Rectangle(triangle.xpoints[0]+offset2[0],triangle.ypoints[0]+offset2[1],bi.getWidth(),bi.getHeight()));
		g.setPaint(tp);
		g.fillPolygon(triangle);
	}
	
	private void updateTextureSource() {
		bi = getTransparentImage(1,1);
		try{
            bi = ImageIO.read(new File(importFileName));
    		//g.setStroke(new BasicStroke(2));
    		//g.drawPolygon(triangle);
    		//g.setStroke(new BasicStroke(1));
            reloadTexture = false;
        }
		catch (IOException ex) {
			System.out.println("exception in DrawHelper");
        }
	}
	
	public void drawVoronoiCell(Graphics2D g, int x0, int y0, int length, boolean[] extremes) {
		Polygon p = new Polygon();
		boolean ex0 = extremes[0], ex1 = extremes[1],
				ey0 = extremes[2], ey1 = extremes[3],
				ez0 = extremes[4], ez1 = extremes[5];
		
		int x1 = (int) Math.round((double)(vx[0]-vy[0])/3.0);
		int y1 = (int) Math.round((double)(vx[1]-vy[1])/3.0);
		int x2 = (int) Math.round((double)(vy[0]-vz[0])/3.0);
		int y2 = (int) Math.round((double)(vy[1]-vz[1])/3.0);
		int x3 = (int) Math.round((double)(vz[0]-vx[0])/3.0);
		int y3 = (int) Math.round((double)(vz[1]-vx[1])/3.0);
		//p.addPoint(x0+xx, y0+xy); // posX
		p.addPoint(x0+x1, y0+y1);
		//p.addPoint(x0-yx, y0-yy); // negY
		p.addPoint(x0-x2, y0-y2);
		//p.addPoint(x0+zx, y0+zy); // posZ
		p.addPoint(x0+x3, y0+y3);
		//p.addPoint(x0-xx, y0-xy); // negX
		p.addPoint(x0-x1, y0-y1);
		//p.addPoint(x0+yx, y0+yy); // posY
		p.addPoint(x0+x2, y0+y2);
		//p.addPoint(x0-zx, y0-zy); // negZ
		p.addPoint(x0-x3, y0-y3);
		g.fillPolygon(p);
		if(ex1) {
			if(ey0) {
				g.translate(x1, y1);
				g.fillPolygon(p);
				g.translate(-x1, -y1);
			}
			if(ez0) {
				g.translate(-x3, -y3);
				g.fillPolygon(p);
				g.translate(x3, y3);
			}
		}
		if(ey1) {
			if(ex0) {
				g.translate(-x1, -y1);
				g.fillPolygon(p);
				g.translate(x1, y1);
			}
			if(ez0) {
				g.translate(x2, y2);
				g.fillPolygon(p);
				g.translate(-x2, -y2);
			}
		}
		if(ez1) {
			if(ex0) {
				g.translate(x3, y3);
				g.fillPolygon(p);
				g.translate(-x3, -y3);
			}
			if(ey0) {
				g.translate(-x2, -y2);
				g.fillPolygon(p);
				g.translate(x2, y2);
			}
		}
	}
	
	public Polygon getVoronoiClippingMask(Graphics2D g, int x0, int y0, int[] dims, Color background) {
		Polygon p = new Polygon();
		int maxX = dims[0], maxY = dims[1], maxZ = dims[2];
		int x = x0+maxX*vx[0], y = y0+maxX*vx[1];
		p.addPoint(x, y);
		x += maxZ*vz[0]; y += maxZ*vz[1];
		p.addPoint(x, y);
		x -= maxX*vx[0]; y -= maxX*vx[1];
		p.addPoint(x, y);
		x += maxY*vy[0]; y += maxY*vy[1];
		p.addPoint(x, y);
		x -= maxZ*vz[0]; y -= maxZ*vz[1];
		p.addPoint(x, y);
		x += maxX*vx[0]; y += maxX*vx[1];
		p.addPoint(x, y);
		return p;		
	}
	
	public Polygon getSingleCubeClippingMask(Graphics2D g, int xx0, int yy0, int faceType) {
		Polygon p = new Polygon();
		Polygon triangle = getFacePolygon(xx0, yy0, faceType, true);
		int x = triangle.xpoints[0], y = triangle.ypoints[0];
		p.addPoint(x+vx[0], y+vx[1]);
		p.addPoint(x-vy[0], y-vy[1]);
		p.addPoint(x+vz[0], y+vz[1]);
		p.addPoint(x-vx[0], y-vx[1]);
		p.addPoint(x+vy[0], y+vy[1]);
		p.addPoint(x-vz[0], y-vz[1]);
		return p;		
	}
	
	// slope 1/2
	public void drawXEdge(Graphics2D g, int x0, int y0, int dir) {
		g.drawLine(x0, y0, x0+vx[0]*dir, y0+vx[1]*dir);
	}

	public void drawXEdge(Graphics2D g, int[] p0, int dir) {
		drawXEdge(g, p0[0], p0[1], dir);
	}
	
	// slope -1/2
	public void drawYEdge(Graphics2D g, int x0, int y0, int dir) {
		g.drawLine(x0, y0, x0+vy[0]*dir, y0+vy[1]*dir);
	}
	
	public void drawYEdge(Graphics2D g, int[] p0, int dir) {
		drawYEdge(g, p0[0], p0[1], dir);
	}
	
	// vertical up
	public void drawZEdge(Graphics2D g, int x0, int y0, int dir) {
		g.drawLine(x0, y0, x0+vz[0]*dir, y0+vz[1]*dir);
	}
	
	public void drawZEdge(Graphics2D g, int[] p0, int dir) {
		drawZEdge(g, p0[0], p0[1], dir);
	}
	
	public static void setPixel(Graphics2D g, int x, int y) {
		g.fillRect(x, y, 1, 1);
	}

	public int getFillStyle() {
		return fillStyle;
	}

	public void setFillStyle(int fillStyle) {
		this.fillStyle = fillStyle;
	}

}
