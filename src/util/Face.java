package util;

import cubeManip.CubeIndex;

public class Face {

	private CubeIndex ci;
	private int type;
	public static final int FACE_NONE = 0, 
			FACE_XY = 1, FACE_XZ = 2,
			FACE_YX = 3, FACE_YZ = 4,
			FACE_ZX = 5, FACE_ZY = 6,
			FACE_X = 7, FACE_Y = 8, FACE_Z = 9;
	public static final int LEFT_FACE = 10, RIGHT_FACE = 11;
	public int orientation;
	private boolean selected = false;
	private boolean underCursor = false;
	private CubeIndex position;
	private boolean hidden = true;
	
	// edge type
	// (flat interior is basically invisible, whereas flat boundary are the visible flat edges)
	public static final int EDGE_NULL = 100, EDGE_FLAT_INTERIOR = 101, EDGE_FLAT_BOUNDARY = 102, 
			EDGE_BACKWARD = 103, EDGE_FORWARD = 104, EDGE_SILHOUETTE = 105, EDGE_IMPOSSIBLE = 106;

	
	public Face(CubeIndex ci, int type, boolean selected, boolean hidden) {
		setCubeIndex(ci.clone());
		setType(type);
		orientation = getOrientationFromType(type);
		this.hidden = hidden;
		setSelected(selected);
		position = calculatePosition(ci, type);
	}
	
	public CubeIndex calculatePosition(CubeIndex ci, int type) {
		switch(type) {
		case Face.FACE_XY: return ci.getPosXNeighbour().clone();
		case Face.FACE_XZ: case Face.FACE_YZ: return ci.clone();
		case Face.FACE_YX: return ci.getPosYNeighbour().clone();
		case Face.FACE_ZX: return ci.getNegYNeighbour().clone();
		case Face.FACE_ZY: return ci.getNegXNeighbour().clone();
		}
		return null;
	}
	
	public Face(Face other) {
		setCubeIndex(other.getCubeIndex());
		setType(other.getType());
		setSelected(other.isSelected());
		setUnderCursor(other.isUnderCursor());
		orientation = other.getOrientation();
		position = other.getPosition().clone();
		hidden = other.isHidden();
	}
	
	public void replaceContent(Face other) {
		setCubeIndex(other.getCubeIndex());
		setType(other.getType());
		setSelected(other.isSelected());
		setUnderCursor(other.isUnderCursor());
		orientation = other.getOrientation();
		position = other.getPosition().clone();
		hidden = other.isHidden();
	}

	public Face clone() {
		return new Face(this);
	}

	// does face belong to cube
	public boolean belongsTo(CubeIndex ci) {
		return getCubeIndex().equals(ci);
	}
	
	public int getOrientationFromType(int type) {
		if(type == FACE_XZ || type==FACE_ZY || type == FACE_YX) return LEFT_FACE;
		else if(type == FACE_ZX || type==FACE_YZ || type == FACE_XY) return RIGHT_FACE;
		else return FACE_NONE;
	}
	
	public int getOrientation() {
		return orientation;
	}
	
	public int getRelativePositionToCubeCentre(CubeIndex cc) {
		if(orientation == LEFT_FACE) {
			if(cc.equals(position)) return FACE_XZ;
			else if(cc.getNegXNeighbour().equals(position)) return FACE_ZY;
			else if(cc.getPosYNeighbour().equals(position)) return FACE_YX;
		}
		else if(orientation == RIGHT_FACE) {
			if(cc.equals(position)) return FACE_YZ;
			else if(cc.getNegYNeighbour().equals(position)) return FACE_ZX;
			else if(cc.getPosXNeighbour().equals(position)) return FACE_XY;
		}
		return FACE_NONE;
	}
	
	public CubeIndex getCubeIndex() {
		return ci.clone();
	}


	public void setCubeIndex(CubeIndex ci) {
		this.ci = ci.clone();
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}
	
	public static int getFullFaceType(int halfFaceType) {
		switch(halfFaceType) {
		case FACE_XY: case FACE_XZ: return FACE_X;
		case FACE_YX: case FACE_YZ: return FACE_Y;
		case FACE_ZX: case FACE_ZY: return FACE_Z;
		}
		return FACE_NONE;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isUnderCursor() {
		return underCursor;
	}

	public void setUnderCursor(boolean underCursor) {
		this.underCursor = underCursor;
	}

	public void toggleSelection() {
		setSelected(!isSelected());
	}

	public CubeIndex getPosition() {
		return position.clone();
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

}
