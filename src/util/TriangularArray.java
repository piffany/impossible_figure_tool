package util;

import cubeManip.CubeIndex;

// array of faces in a configuration of cubes
public class TriangularArray {

	private int maxX, maxY, maxZ;
	private LayeredFace[][][] array;
	
	public TriangularArray(int maxX, int maxY, int maxZ) {
		this.maxX = maxX;
		this.maxY = maxY;
		this.maxZ = maxZ;
		array = new LayeredFace[maxX+1][maxY+1][maxZ+1];
		init();
	}
	
	public TriangularArray(TriangularArray other) {
		this.maxX = other.maxX;
		this.maxY = other.maxY;
		this.maxZ = other.maxZ;
		array = new LayeredFace[maxX+1][maxY+1][maxZ+1];
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						array[x][y][z] = other.get(new CubeIndex(x, y, z)).clone();
					}
				}
			}
		}
	}
	
	public void replaceFaces(TriangularArray other) {
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						array[x][y][z].replaceFaces(other.get(new CubeIndex(x, y, z)));
					}
				}
			}
		}
	}
	
	public void init() {
		for(int x=0; x<=maxX; x++) {
			for(int y=0; y<=maxY; y++) {
				for(int z=0; z<=maxZ; z++) {
					if(x==0 || y==0 || z==0) {
						//array[x][y][z] = new Face(new CubeIndex(0,0,0),0, false);
						array[x][y][z] = new LayeredFace();
					}
				}
			}
		}
	}
	
	public void add(CubeIndex pos, Face face) {
		//array[pos.getX()][pos.getY()][pos.getZ()] = (face==null) ? null : face.clone();
		if(face!=null) {
			array[pos.getX()][pos.getY()][pos.getZ()].add(face);
		}
	}
	
	public LayeredFace getPosXNeighbour(CubeIndex ci) {
		return get(ci.getPosXNeighbour());
	}
	
	public LayeredFace getPosYNeighbour(CubeIndex ci) {
		return get(ci.getPosYNeighbour());
	}

	public LayeredFace getPosZNeighbour(CubeIndex ci) {
		return get(ci.getPosZNeighbour());
	}
	
	public LayeredFace getNegXNeighbour(CubeIndex ci) {
		return get(ci.getNegXNeighbour());
	}

	public LayeredFace getNegYNeighbour(CubeIndex ci) {
		return get(ci.getNegYNeighbour());
	}

	public LayeredFace getNegZNeighbour(CubeIndex ci) {
		return get(ci.getNegZNeighbour());
	}
	
	public boolean isValidIndex(CubeIndex ci) {
		int x = ci.getX(), y = ci.getY(), z = ci.getZ();
		boolean withinBounds = (x>=0 && x<=maxX) && (y>=0 && y<=maxY) && (z>=0 && z<=maxZ);
		boolean onFaces = (x==0 || y==0 || z==0) && (x>=0 && y>=0 && z>=0);
		return withinBounds && onFaces;
	}

	public LayeredFace get(CubeIndex ci) {
		int x = ci.getX(), y = ci.getY(), z = ci.getZ();
		if(isValidIndex(ci)) return array[x][y][z];
		else return new LayeredFace();
	}
}
