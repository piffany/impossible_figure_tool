package util;

import java.awt.Color;

import colours.Colour;

public class IsoColours {

	private static int alpha = 255;

	public static final Color black = new Color(0, 0, 0, alpha);
	public static final Color white = new Color(255, 255, 255, alpha);

	// greens
	public static final Color darkestGreen = new Color(12, 85, 16, alpha);
	public static final Color darkGreen = new Color(23, 169, 30, alpha);
	public static final Color medGreen = new Color(30, 220, 39, alpha);
	public static final Color brightGreen = new Color(117, 233, 120, alpha);
	public static final Color brightestGreen = new Color(208, 248, 209, alpha);
	public static final Color[] greens = {darkestGreen, darkGreen, medGreen, brightGreen, brightestGreen};

	// reds
	public static final Color darkestRed = new Color(95, 21, 1, alpha);
	public static final Color darkRed = new Color(128, 37, 14, alpha);
	public static final Color medRed = new Color(232, 48, 18, alpha);
	public static final Color brightRed = new Color(240, 125, 110, alpha);
	public static final Color brightestRed = new Color(249, 212, 207, alpha);
	public static final Color[] reds = {darkestRed, darkRed, medRed, brightRed, brightestRed};
	
	// blues
	public static final Color darkestBlue = new Color(0, 66, 94, alpha);
	public static final Color darkBlue = new Color(17, 128, 175, alpha);
	public static final Color medBlue = new Color(23, 167, 227, alpha);
	public static final Color brightBlue = new Color(113, 204, 237, alpha);
	public static final Color brightestBlue = new Color(207, 238, 249, alpha);
	public static final Color[] blues = {darkestBlue, darkBlue, medBlue, brightBlue, brightestBlue};
	
	// greys
	public static final Color darkestGrey = new Color(50, 50, 50, alpha);
	public static final Color darkGrey = new Color(100, 100, 100, alpha);
	public static final Color medGrey = new Color(128, 128, 128, alpha);
	public static final Color brightGrey = new Color(150, 150, 150, alpha);
	public static final Color brightestGrey = new Color(200, 200, 200, alpha);
	public static final Color[] greys = {darkestGrey, darkGrey, medGrey, brightGrey, brightestGrey};

	public static final int PRESET_GREEN = 0, PRESET_RED = 1, PRESET_BLUE = 2, PRESET_GREY = 3, PRESET_RAINBOW = 4;

	private Color faceX, faceY, faceZ, edgeX, edgeY, edgeZ, forward, backward, silhouette;
	
	public IsoColours() {
		faceX = white;
		faceY = white;
		faceZ = white;
		edgeX = black;
		edgeY = black;
		edgeZ = black;
		forward = black;
		backward = black;
		silhouette = black;
	}
	
	public IsoColours(int preset) {
		this();
		switch(preset) {
		case PRESET_GREEN:
			faceX = greens[1];
			faceY = greens[2];
			faceZ = greens[3];
			edgeX = greens[3];
			edgeY = greens[1];
			edgeZ = greens[1];
			forward = greens[4];
			backward = greens[0];
			silhouette = black;
			break;
		case PRESET_RED:
			faceX = reds[1];
			faceY = reds[2];
			faceZ = reds[3];
			edgeX = reds[3];
			edgeY = reds[1];
			edgeZ = reds[1];
			forward = reds[4];
			backward = reds[0];
			silhouette = black;
			break;
		case PRESET_BLUE:
			faceX = blues[1];
			faceY = blues[2];
			faceZ = blues[3];
			edgeX = blues[3];
			edgeY = blues[1];
			edgeZ = blues[1];
			forward = blues[4];
			backward = blues[0];
			silhouette = black;
			break;
		case PRESET_GREY:
			faceX = greys[1];
			faceY = greys[2];
			faceZ = greys[3];
			edgeX = greys[3];
			edgeY = greys[1];
			edgeZ = greys[1];
			forward = greys[4];
			backward = greys[0];
			silhouette = black;
			break;
		case PRESET_RAINBOW:
			faceX = new Color(50,50,50);
			faceY = new Color(75,75,75);
			faceZ = new Color(100,100,100);
			edgeX = medGreen;
			edgeY = medGreen;
			edgeZ = medGreen;
			forward = Color.yellow;
			backward = medBlue;
			silhouette = Color.red;
			break;
		}		
	}

	public Color getFaceX() {
		return faceX;
	}

	public void setFaceX(Color faceX) {
		this.faceX = new Colour(faceX);
	}

	public Color getFaceY() {
		return faceY;
	}

	public void setFaceY(Color faceY) {
		this.faceY = new Colour(faceY);;
	}

	public Color getFaceZ() {
		return faceZ;
	}

	public void setFaceZ(Color faceZ) {
		this.faceZ = new Colour(faceZ);
	}

	public Color getEdgeX() {
		return edgeX;
	}

	public void setEdgeX(Color edgeX) {
		this.edgeX = new Colour(edgeX);
	}

	public Color getEdgeY() {
		return edgeY;
	}

	public void setEdgeY(Color edgeY) {
		this.edgeY = new Colour(edgeY);
	}

	public Color getEdgeZ() {
		return edgeZ;
	}

	public void setEdgeZ(Color edgeZ) {
		this.edgeZ = new Colour(edgeZ);
	}

	public Color getForward() {
		return forward;
	}

	public void setForward(Color forward) {
		this.forward = new Colour(forward);
	}

	public Color getBackward() {
		return backward;
	}

	public void setBackward(Color backward) {
		this.backward = new Colour(backward);
	}

	public Color getSilhouette() {
		return silhouette;
	}

	public void setSilhouette(Color silhouette) {
		this.silhouette = new Colour(silhouette);
	}
}
