package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Paint;

public class ActionModeComboBox extends JPanel {

	public static final int FRONT = 0, BACK = 1, HIDE = 2, SWAP = 3, FACE_FORWARD = 4;
	public static final String[] modeNames = {"Bring to front", "Send to back", "Hide", "Swap", "Bring face forward"};
	private JLabel label = new JLabel("Action: ");
	private JComboBox<String> combo;
	
	public ActionModeComboBox(ActionListener listener) {
		super();
		combo = new JComboBox<String>(modeNames);
		combo.setSelectedIndex(0);
		combo.addActionListener(listener);
		add(label);
		add(combo);
		combo.setFocusable(false);
	}
	
	public JComboBox getComboBox() {
		return combo;
	}
	
	public int getMaxSelectable() {
		return (getModeSelected() == SWAP) ? 2 : 1;
	}
	
	public int getModeSelected() {
		return combo.getSelectedIndex();
	}

}
