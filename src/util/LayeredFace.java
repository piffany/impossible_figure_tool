package util;

import java.util.Vector;

import cubeManip.CubeIndex;

// same position but with multiple layer of faces

public class LayeredFace {

	Vector<Face> layers;
	public static final int LEFT_FACE = 10, RIGHT_FACE = 11;
	private int orientation = -1;
	private CubeIndex position;
	
	public LayeredFace() {
		layers = new Vector<Face>();
		position = new CubeIndex(0,0,0);
	}
	
	public LayeredFace(LayeredFace other) {
		layers = new Vector<Face>();
		for(int i=0; i<other.getNumLayers(); i++) {
			layers.add(other.get(i).clone());
		}
		orientation = other.getOrientation();
		position = other.getPosition().clone();
	}
	
	public void replaceFaces(LayeredFace other) {
		for(int i=0; i<other.getNumLayers(); i++) {
			layers.get(i).replaceContent(other.get(i));
		}
	}
	
	public LayeredFace clone() {
		return new LayeredFace(this);
	}
	
	public void swapFaceOrder(int i, int j) {
		if(i>=0 && i<layers.size() && j>=0 && j<layers.size() && i!=j) {
			Face face = layers.remove(Math.max(i, j));
			layers.add(Math.min(i, j), face);
		}
	}
	
	public void bringForward(int i) {
		if(i>=0 && i<layers.size()-1) {
			Face face = layers.remove(i);
			layers.add(i+1, face);
		}
	}
	
	public void sendBackward(int i) {
		//System.out.println("send backward: " + i);
		if(i>=1 && i<layers.size()) {
			Face face = layers.remove(i);
			layers.add(i-1, face);
		}
	}
	
	public int getFirstFaceBelongingToIndex(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getCubeIndex().equals(ci)) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean contains(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getCubeIndex().equals(ci)) {
				return true;
			}
		}
		return false;
	}
	
	public Face get(int i) {
		if(i>=0 && i<layers.size()) return layers.get(i);
		else return null;
	}

	public int getNumLayers() {
		return layers.size();
	}
	
	private int getOrientation() {
		return orientation;
	}
	
	private CubeIndex getPosition() {
		return position.clone();
	}

	public void add(Face face) {
		if(orientation==-1) orientation = face.getOrientation();
		if(position==null) position = face.getPosition().clone();
		layers.add(face.clone());
	}
	
	public void add(CubeIndex ci, int type, boolean selected, boolean hidden) {
		add(new Face(ci, type, selected, hidden));
	}
	
	public Face getTop() {
		for(int i=layers.size()-1; i>=0; i--) {
			if(!layers.get(i).isHidden()) return layers.get(i);
		}
		return null;
		//else return layers.get(layers.size()-1);
		//return layers.get(0);
	}
	
	public void setAsTop(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getCubeIndex().equals(ci)) {
				Face face = layers.remove(i);
				layers.add(face);
				return;
			}
		}
	}
	
	public void setAsSecondToTop(CubeIndex ci) {
		Face top = getTop();
		CubeIndex topIndex = (top!=null) ? top.getCubeIndex() : null;
		setAsTop(ci);
		if(topIndex!=null) setAsTop(topIndex);
	}
	
	public void setAsBottom(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getCubeIndex().equals(ci)) {
				Face face = layers.remove(i);
				layers.add(0,face);
				return;
			}
		}
	}

	public void setHidden(CubeIndex ci, boolean hidden) {
		//System.out.println("hide : " + ci);
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				face.setHidden(hidden);
			}
		}
	}

	public boolean isSelected(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				return face.isSelected();
			}
		}
		return false;
	}
	
	public boolean isUnderCursor(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				return face.isUnderCursor();
			}
		}
		return false;
	}

	public void toggleSelection(CubeIndex ci) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				face.toggleSelection();
			}
		}
	}

	public void setSelected(CubeIndex ci, boolean selected) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				face.setSelected(selected);
			}
		}
	}
	
	public void setUnderCursor(CubeIndex ci, boolean underCursor) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				face.setUnderCursor(underCursor);
			}
		}
	}

	public void setType(CubeIndex ci, int type) {
		for(int i=0; i<layers.size(); i++) {
			Face face = layers.get(i);
			if(face.getCubeIndex().equals(ci)) {
				face.setType(type);
			}
		}
	}

	public boolean isTopSelected(CubeIndex ci) {
		if(layers.isEmpty() || getTop()==null) return false;
		else return getTop().isSelected();
	}
	
	public boolean isHidden() {
		for(int i=0; i<layers.size(); i++) {
			if(!layers.get(i).isHidden()) return false;
		}
		return true;
	}

	public boolean isInFrontOf(CubeIndex ci1, CubeIndex ci2) {
		int ind1 = getFirstFaceBelongingToIndex(ci1);
		if(ind1!=-1 && layers.get(ind1).isHidden()) ind1=-1;
		int ind2 = getFirstFaceBelongingToIndex(ci2);
		if(ind2!=-1 && layers.get(ind2).isHidden()) ind2=-1;
		return (ind1!=-1 && ind2!=-1 && (ind1 > ind2));
	}
	
	public boolean isDirectlyInFrontOf(CubeIndex ci1, CubeIndex ci2) {
		int ind1 = getFirstFaceBelongingToIndex(ci1);
		if(ind1!=-1 && layers.get(ind1).isHidden()) ind1=-1;
		int ind2 = getFirstFaceBelongingToIndex(ci2);
		if(ind2!=-1 && layers.get(ind2).isHidden()) ind2=-1;
		if(ind1!=-1 && ind2!=-1 && ind1 > ind2) {
			for(int i=ind2+1; i<ind1; i++) {
				if(!this.get(i).isHidden()) return false;
			}
			return true;
		}
		else return false;
	}

	
}
