package util;

import cubeManip.CubeIndex;

public class EdgeValidator {

	TriangularEdgeArray edgeTypeArray;
	
	public EdgeValidator(TriangularEdgeArray edgeTypeArray) {
		this.edgeTypeArray = edgeTypeArray;
	}

	public boolean validateCube(CubeIndex ci) {
		boolean valid = true;
		if(!validateAroundVertex(ci, ci)) valid = false;
		if(!validateAroundVertex(ci.getPosXNeighbour(), ci)) valid = false;
		if(!validateAroundVertex(ci.getPosYNeighbour(), ci)) valid = false;
		if(!validateAroundVertex(ci.getPosZNeighbour(), ci)) valid = false;
		if(!validateAroundVertex(ci.getNegXNeighbour(), ci)) valid = false;
		if(!validateAroundVertex(ci.getNegYNeighbour(), ci)) valid = false;
		if(!validateAroundVertex(ci.getNegZNeighbour(), ci)) valid = false;
		return valid;
	}
	
	public boolean validateAroundVertex(CubeIndex vert, CubeIndex cube) {
		boolean valid = true;
		if(!vert.equals(cube)) {
			if(oneInconsistentSilhouette(vert, cube)) valid = false;
		}
		return valid;
	}
	
	private boolean oneInconsistentSilhouette(CubeIndex vert, CubeIndex cube) {
		int bentDepth = getCCWDepthChangeOfBentEdgesAroundVertex(vert);
		int[] silDepth = getCCWDepthChangeOfSilhouetteEdgesAroundVertex(vert);
		int firstSilDepth = 0;
		for(int i=0; i<silDepth.length; i++) {
			if(silDepth[i]!=0) {
				firstSilDepth = silDepth[i];
				break;
			}
		}
		if(firstSilDepth!=0) System.out.println(bentDepth + ", " + firstSilDepth);
		/*
		if(vert.equals(cube.getNegZNeighbour())) {
		System.out.println("bent: " + bentDepth);
		System.out.println("sil: " + silDepth[0] + ", " + silDepth[1] + ", " + silDepth[2] + ", " + silDepth[3] + ", " + silDepth[4] + ", " + silDepth[5]);
		}
		*/
	
		return (bentDepth*firstSilDepth>0);
		/*
		System.out.println("bent: " + bentDepth);
		System.out.println("sil: " + silDepth[0] + ", " + silDepth[1] + ", " + silDepth[2] + ", " + silDepth[3] + ", " + silDepth[4] + ", " + silDepth[5]);
		
		return true;
		*/
	}
	
	// silhouette = 0
	// -1 forward (toward screen), +1 backward (away from screen)
	private int getCCWDepthChangeOfPosXEdge(CubeIndex ci) {
		int edgeType = edgeTypeArray.getPosXEdgeType(ci)[0];
		if(edgeType==Face.EDGE_FORWARD) return -1;
		else if(edgeType==Face.EDGE_BACKWARD) return 1;
		else return 0;
	}
	
	private int getCCWDepthChangeOfPosYEdge(CubeIndex ci) {
		int edgeType = edgeTypeArray.getPosYEdgeType(ci)[0];
		if(edgeType==Face.EDGE_FORWARD) return -1;
		else if(edgeType==Face.EDGE_BACKWARD) return 1;
		else return 0;
	}
	
	// silhouette = 0
	// -1 forward (toward screen), +1 backward (away from screen)
	private int getCCWDepthChangeOfBentEdgesAroundVertex(CubeIndex ci) {
		int[] edgeTypes = {	edgeTypeArray.getPosXEdgeType(ci)[0],
							edgeTypeArray.getPosXEdgeType(ci.getNegXNeighbour())[0],
							edgeTypeArray.getPosYEdgeType(ci)[0],
							edgeTypeArray.getPosYEdgeType(ci.getNegYNeighbour())[0],
							edgeTypeArray.getPosZEdgeType(ci)[0],
							edgeTypeArray.getPosZEdgeType(ci.getNegZNeighbour())[0] };
		int sum = 0;
		for(int i=0; i<edgeTypes.length; i++) {
			switch(edgeTypes[i]) {
			case Face.EDGE_FORWARD:	 sum-=1; break;
			case Face.EDGE_BACKWARD: sum+=1; break;
			}
		}
		return sum;
	}
	
	private int[] getCCWDepthChangeOfSilhouetteEdgesAroundVertex(CubeIndex ci) {
		int[][] edgeTypes = {	edgeTypeArray.getPosXEdgeType(ci),
								edgeTypeArray.getPosXEdgeType(ci.getNegXNeighbour()),
								edgeTypeArray.getPosYEdgeType(ci),
								edgeTypeArray.getPosYEdgeType(ci.getNegYNeighbour()),
								edgeTypeArray.getPosZEdgeType(ci),
								edgeTypeArray.getPosZEdgeType(ci.getNegZNeighbour()) };
		int[] depths = {0,0,0,0,0,0};
		int i = 0;
		// posX
		i = 0;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			switch(edgeTypes[i][1]) {
			/*
			case Face.FACE_X: depths[i] = 0; break;
			case Face.FACE_Y: depths[i] = -1; break;
			case Face.FACE_Z: depths[i] = +1; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = -1; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = 0; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = +1; break;
			*/
			}
		}
		// negX
		i = 1;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			/*
			switch(edgeTypes[i][1]) {
			case Face.FACE_X: depths[i] = 0; break;
			case Face.FACE_Y: depths[i] = +1; break;
			case Face.FACE_Z: depths[i] = -1; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = +1; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = 0; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = -1; break;
			}
			*/
		}
		// posY
		i = 2;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			switch(edgeTypes[i][1]) {
			/*
			case Face.FACE_X: depths[i] = +1; break;
			case Face.FACE_Y: depths[i] = 0; break;
			case Face.FACE_Z: depths[i] = -1; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = +1; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = -1; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = 0; break;
			*/
			}
		}
		// negY
		i = 3;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			switch(edgeTypes[i][1]) {
			case Face.FACE_X: depths[i] = -1; break;
			case Face.FACE_Y: depths[i] = 0; break;
			case Face.FACE_Z: depths[i] = +1; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = -1; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = +1; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = 0; break;
			}
		}
		// posZ
		i = 4;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			switch(edgeTypes[i][1]) {
			/*
			case Face.FACE_X: depths[i] = -1; break;
			case Face.FACE_Y: depths[i] = +1; break;
			case Face.FACE_Z: depths[i] = 0; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = 0; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = +1; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = -1; break;
			*/
			}
		}
		// negZ
		i = 5;
		if(edgeTypes[i][0]==Face.EDGE_SILHOUETTE) {
			switch(edgeTypes[i][1]) {
			/*
			case Face.FACE_X: depths[i] = +1; break;
			case Face.FACE_Y: depths[i] = -1; break;
			case Face.FACE_Z: depths[i] = 0; break;
			case Face.FACE_XY: case Face.FACE_YX: depths[i] = 0; break;
			case Face.FACE_YZ: case Face.FACE_ZY: depths[i] = -1; break;
			case Face.FACE_XZ: case Face.FACE_ZX: depths[i] = +1; break;
			*/
			}
		}
		return depths;
	}
}
