package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.MyCanvas;
import main.Paint;

public class PickingModeComboBox extends JPanel {

	//MyCanvas.PICK_VISIBLE, PICK_CENTRE
	//public static final int FRONT = 0, BACK = 1, HIDE = 2, SWAP = 3;
	public static final int[] modes = {MyCanvas.PICK_CENTRE, MyCanvas.PICK_VISIBLE};//, MyCanvas.PICK_EDGE};
	public static final String[] modeNames = {"Pick cube by centre", "Pick cube by face"};//, "Pick edge"};
	private JLabel label = new JLabel("Picking Mode: ");
	private JComboBox<String> combo;
	
	public PickingModeComboBox(ActionListener listener) {
		super();
		combo = new JComboBox<String>(modeNames);
		combo.setSelectedIndex(0);
		combo.addActionListener(listener);
		add(label);
		add(combo);
		combo.setFocusable(false);
	}
	
	public JComboBox getComboBox() {
		return combo;
	}
	
	public int getModeSelected() {
		return modes[combo.getSelectedIndex()];
	}

}
