package util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;

import colours.Colour;
import cubeManip.CubeIndex;

public class FaceDraw {

	private int[] origin = {0,0};
	private int side;//, height;
	public int[] vx = new int[2] , vy = new int[2], vz = new int[2];
	/* height and slope */
	//private double posSlope = 0.245;//, negSlope = -posSlope;
	/*
	public double[] mx = getMx(negSlope, posSlope),
					my = getMy(negSlope, posSlope),
					mz = getMz(negSlope, posSlope);
	*/
	//private double angleElev = Math.acos(posSlope);
	//private double angleElev = Math.PI*(60.0/180.0);
	/*
	public double[] mx = getMx(angleElev),
					my = getMy(angleElev),
					mz = getMz(angleElev);
	*/
	//public double[] mz = {0,-1}, mx = {-0.5, 0.16}, my = {-mz[0]-mx[0], -mz[1]-mx[1]};
	public double 	angle1 = 60.0/180.0*Math.PI,
					angle2 = 45.0/180.0*Math.PI;
	public double[] mx = getMx(angle1, angle2),
					my = getMy(angle1, angle2),
					mz = getMz(angle1, angle2);

	public EdgeDraw edgeDraw;
	public DrawHelper drawHelper;

	private IsoColours selectedColours, unselectedColours;
	public static Color NORMAL_BACKGROUND = Color.white, DEBUG_BACKGROUND = Color.black;
	public Color backgroundColour = NORMAL_BACKGROUND;
	private int fillStyle = DrawHelper.FILL_COLOUR;

	public FaceDraw(int x, int y, int side) {
		this(x, y, side, false, DrawHelper.FILL_COLOUR);
	}

	public FaceDraw(int x, int y, int side, boolean debug, int fillStyle) {
		origin[0] = x;
		origin[1] = y;
		this.side = side;
		//this.height = DrawHelper.getHeight(side);
		double totalX = -mx[0]+my[0];
		double totalY = mx[1]+my[1]-mz[1];
		double hypotenuse = -mz[1];
		//System.out.println(mx[0] + ", " + mx[1] + " : " + my[0] + ", " + my[1] + " : " + mz[0] + ", " + mz[1]);

		vx[0] = (int) Math.round(mx[0]/hypotenuse*(double)side);
		vx[1] = (int) Math.round(mx[1]/hypotenuse*(double)side);
		vy[0] = (int) Math.round(my[0]/hypotenuse*(double)side);
		vy[1] = (int) Math.round(my[1]/hypotenuse*(double)side);
		vz[0] = (int) Math.round(mz[0]/hypotenuse*(double)side);
		vz[1] = (int) Math.round(mz[1]/hypotenuse*(double)side);
		//System.out.println("B: vx = " + vx[0] + ", " + vx[1]);
		//System.out.println("vx = " + vx[0] + ", " + vx[1]);
		//System.out.println("vy = " + vy[0] + ", " + vy[1]);
		//System.out.println("vz = " + vz[0] + ", " + vz[1]);
		setFillStyle(fillStyle);
		drawHelper = new DrawHelper(side, mx, my, mz, fillStyle);
		edgeDraw = new EdgeDraw(x, y, side, mx, my, mz, drawHelper);
		//System.out.println(edgeDraw.getPosition(new CubeIndex(0,0,0))[0] + edgeDraw.getPosition(new CubeIndex(0,0,0))[1]);
		if(debug) {
			selectedColours = new IsoColours(IsoColours.PRESET_RED);
			unselectedColours = new IsoColours(IsoColours.PRESET_RAINBOW);
			backgroundColour = DEBUG_BACKGROUND;			
		}
		else {
			selectedColours = new IsoColours(IsoColours.PRESET_RED);
			unselectedColours = new IsoColours(IsoColours.PRESET_GREEN);
			backgroundColour = NORMAL_BACKGROUND;
		}
	}

	private static double[][] computeMatrix(double negSlope, double posSlope) {
		double angle1 = Math.acos(Math.sqrt(-posSlope*negSlope));
		double angle2 = Math.PI/4.0;
		double[][] M = new double[3][3];
		M[0][1] = Math.cos(angle2);
		M[0][0] = -Math.sin(angle2);
		M[0][2] = 0;
		M[1][1] = Math.cos(angle1)*Math.sin(angle2);
		M[1][0] = Math.cos(angle1)*Math.cos(angle2);
		M[1][2] = -Math.sin(angle1);
		
		return M;
	}

	private static double[] getMy(double angle1, double angle2) {
		double 	sin1 = Math.sin(angle1),
				cos1 = Math.cos(angle1),
				sin2 = Math.sin(angle2),
				cos2 = Math.cos(angle2);
		double[] Mx = {	sin2*cos2/cos1, sin2*sin2 };
		return Mx;
	}

	private static double[] getMx(double angle1, double angle2) {
		double 	sin1 = Math.sin(angle1),
				cos1 = Math.cos(angle1),
				sin2 = Math.sin(angle2),
				cos2 = Math.cos(angle2);
		double[] My = {	-sin2*cos2/cos1, cos2*cos2 };
		return My;
	}
	
	private static double[] getMz(double angle1, double angle2) {
		double 	sin1 = Math.sin(angle1),
				cos1 = Math.cos(angle1),
				sin2 = Math.sin(angle2),
				cos2 = Math.cos(angle2);
		double[] Mx = {	0, -1 };
		return Mx;
	}


	
	private static double[] getMx0(double angle) {
		double angle1 = angle;
		double angle2 = Math.PI/4.0;
		double[] M = new double[2];
		M[0] = -Math.sin(angle2);
		M[1] = Math.cos(angle1)*Math.cos(angle2);
		return M;
	}
	
	private static double[] getMx(double angle) {
		double[] Mx = getMx0(angle);
		double[] My = getMy0(angle);
		double scale = 1.0 / (Mx[1]+My[1]);
		Mx[0]*=scale; Mx[1]*=scale;
		return Mx;
	}
	
	private static double[] getMy0(double angle) {
		double angle1 = angle;
		double angle2 = Math.PI/4.0;
		double[] M = new double[2];
		M[0] = Math.cos(angle2);
		M[1] = Math.cos(angle1)*Math.sin(angle2);
		return M;
	}
	
	private static double[] getMy(double angle) {
		double[] Mx = getMx0(angle);
		double[] My = getMy0(angle);
		double scale = 1.0 / (Mx[1]+My[1]);
		My[0]*=scale; My[1]*=scale;
		return My;
	}
	
	private static double[] getMz0(double angle) {
		double angle1 = angle;
		double angle2 = Math.PI/4.0;
		double[] M = new double[2];
		M[0] = 0;
		M[1] = -Math.sin(angle1);
		return M;
	}
	
	private static double[] getMz(double angle) {
		double[] M = {0,-1};
		return M;
	}
	
	public int[][] getXYZVectors() {
		int[][] vectors = {vx, vy, vz};
		return vectors;
	}

	public int[] getPosition(CubeIndex ci) {
		return edgeDraw.getPosition(ci);
	}

	public void addPattern(Graphics2D g, CubeIndex ci, FaceArray faces, int style) {
		// right face (i.e. yz-face)
		Face faceYz = faces.getFaceYz(ci).getTop();		
		if(faceYz!=null) {
			addPatternToFace(g, ci, faceYz.getType(), faceYz.isHidden(), style);
		}
	}

	public void drawLeftRightFaces(Graphics2D g, CubeIndex ci, FaceArray faces, int fillStyle) {
		switch(fillStyle) {
		case DrawHelper.FILL_COLOUR: case DrawHelper.FILL_COLOUR_AND_PATTERN:
			drawLeftRightFacesWithColour(g, ci, faces);
			break;
		case DrawHelper.FILL_TEXTURE:
			drawLeftRightFacesWithTexture(g, ci, faces);
			break;
		}
	}

	private void drawLeftRightFacesWithTexture(Graphics2D g, CubeIndex ci, FaceArray faces) {
		if(!faces.getFaceXz(ci).isHidden()) {
		for(int i=0; i<faces.getFaceXz(ci).getNumLayers(); i++) {
			// left face (i.e. xz-face)
			Face faceXz = faces.getFaceXz(ci).get(i);
			if(faceXz!=null && !faceXz.isHidden()) {
				//Color cXz = getFaceColor(faceXz.getType(), faceXz.isHidden());
				//drawXzFace(g, ci, cXz);
				drawLeftFaceWithTexture(g, ci, faceXz.getType(), faceXz.isHidden());
			}
		}
		}
		// right face (i.e. yz-face)
		if(!faces.getFaceYz(ci).isHidden()) {
		for(int i=0; i<faces.getFaceYz(ci).getNumLayers(); i++) {
			Face faceYz = faces.getFaceYz(ci).get(i);		
			if(faceYz!=null && !faceYz.isHidden()) {
				//Color cYz = getFaceColor(faceYz.getType(), faceYz.isHidden());
				drawRightFaceWithTexture(g, ci, faceYz.getType(), faceYz.isHidden());
			}
		}
		}
	}

	private void drawLeftRightFacesWithColour(Graphics2D g, CubeIndex ci, FaceArray faces) {
		// left face (i.e. xz-face)
		Face faceXz = faces.getFaceXz(ci).getTop();
		if(faceXz!=null) {
			//if(!faceXz.isHidden()) 	System.out.println(getPosition(ci)[0] + ", " + getPosition(ci)[1]);

			drawLeftFaceWithColour(g, ci, faceXz.getType(), faceXz.isHidden());
		}
		// right face (i.e. yz-face)
		Face faceYz = faces.getFaceYz(ci).getTop();		
		if(faceYz!=null) {
			//Color cYz = getFaceColor(faceYz.getType(), faceYz.isHidden());
			drawRightFaceWithColour(g, ci, faceYz.getType(), faceYz.isHidden());
		}
	}

	public void drawLeftRightFaceMasks(Graphics2D g, CubeIndex ci, FaceArray faces, boolean randomColours) {
		// left face (i.e. xz-face)
		Face faceXz = faces.getFaceXz(ci).getTop();
		if(faceXz!=null && faceXz.getType() != Face.FACE_NONE) {
			CubeIndex ciXz = faceXz.getCubeIndex();
			Color cXz = randomColours ?
					Colour.getRandomColour(ciXz.getX(), ciXz.getY(), ciXz.getZ()) :
						new Color(ciXz.getX(), ciXz.getY(), ciXz.getZ());
					drawLeftFaceWithColour(g, ci, cXz, faceXz.getType());
					//edgeDraw.drawPosXEdge(g, ci);
					//edgeDraw.drawPosYEdge(g, ci.getNegYNeighbour());
					//edgeDraw.drawPosZEdge(g, ci.getPosXNeighbour());
		}
		// right face (i.e. xz-face)
		Face faceYz = faces.getFaceYz(ci).getTop();
		if(faceYz!=null && faceYz.getType() != Face.FACE_NONE) {
			CubeIndex ciYz = faceYz.getCubeIndex();
			Color cYz = randomColours ?
					Colour.getRandomColour(ciYz.getX(), ciYz.getY(), ciYz.getZ()) :
						new Color(ciYz.getX(), ciYz.getY(), ciYz.getZ());
					drawRightFaceWithColour(g, ci, cYz, faceYz.getType());
					//edgeDraw.drawPosXEdge(g, ci.getNegXNeighbour());
					//edgeDraw.drawPosYEdge(g, ci);
					//edgeDraw.drawPosZEdge(g, ci.getPosYNeighbour());
		}
	}

	public Color getFaceColor(int type, boolean hidden) {
		if(hidden) return backgroundColour;
		IsoColours colours = unselectedColours;//selected ? selectedColours : unselectedColours;
		switch(type) {
		case Face.FACE_XY: case Face.FACE_XZ: return colours.getFaceX();
		case Face.FACE_YX: case Face.FACE_YZ: return colours.getFaceY();
		case Face.FACE_ZX: case Face.FACE_ZY: return colours.getFaceZ();
		} 
		return backgroundColour;
	}

	public void drawVertex(Graphics2D g, int[] pos) {
		g.fillRect(pos[0], pos[1], 1, 1);
	}

	public void drawVertex(Graphics2D g, CubeIndex ci) {
		drawVertex(g, getPosition(ci));
	}

	/*
	public void drawXyFace(Graphics2D g, CubeIndex ci, Color c) {
		CubeIndex ci2 = ci.getPosXNeighbour();
		int[] pos = getPosition(ci2);
		drawHelper.drawRightTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}

	public void drawXzFace(Graphics2D g, CubeIndex ci, Color c) {
		int[] pos = getPosition(ci);
		drawHelper.drawLeftTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}
	 */

	private void drawLeftFaceWithTexture(Graphics2D g, CubeIndex ci, int faceType, boolean faceHidden) {
		int[] pos = getPosition(ci);
		drawHelper.drawLeftTriangularFaceWithTexture(g, pos[0], pos[1], side, faceType);
	}

	private void drawRightFaceWithTexture(Graphics2D g, CubeIndex ci, int faceType, boolean faceHidden) {
		int[] pos = getPosition(ci);
		drawHelper.drawRightTriangularFaceWithTexture(g, pos[0], pos[1], side, faceType);
	}

	private void drawLeftFaceWithColour(Graphics2D g, CubeIndex ci, int faceType, boolean faceHidden) {
		int[] pos = getPosition(ci);
		Color c = getFaceColor(faceType, faceHidden);
		drawHelper.drawLeftTriangularFaceWithColour(g, pos[0], pos[1], side, c, faceType);
		//System.out.println(ci + " : " + pos[0] + ", " + pos[1]);
	}

	private void drawRightFaceWithColour(Graphics2D g, CubeIndex ci, int faceType, boolean faceHidden) {
		int[] pos = getPosition(ci);
		Color c = getFaceColor(faceType, faceHidden);
		drawHelper.drawRightTriangularFaceWithColour(g, pos[0], pos[1], side, c, faceType);
	}
	
	private void drawLeftFaceWithColour(Graphics2D g, CubeIndex ci, Color c, int faceType) {
		int[] pos = getPosition(ci);
		drawHelper.drawLeftTriangularFaceWithColour(g, pos[0], pos[1], side, c, faceType);
	}

	private void drawRightFaceWithColour(Graphics2D g, CubeIndex ci, Color c, int faceType) {
		int[] pos = getPosition(ci);
		drawHelper.drawRightTriangularFaceWithColour(g, pos[0], pos[1], side, c, faceType);
	}
	
	private void addPatternToFace(Graphics2D g, CubeIndex ci, int faceType, boolean faceHidden, int style) {
		int[] pos = getPosition(ci);
		//Color c = getFaceColor(faceType, faceHidden);
		drawHelper.addPatternToFace(g, pos[0], pos[1], side, faceType, style);
	}

	/*
	public void drawYxFace(Graphics2D g, CubeIndex ci, Color c) {
		CubeIndex ci2 = ci.getPosYNeighbour();
		int[] pos = getPosition(ci2);
		drawHelper.drawLeftTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}

	public void drawYzFace(Graphics2D g, CubeIndex ci, Color c) {
		int[] pos = getPosition(ci);
		drawHelper.drawRightTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}


	public void drawZxFace(Graphics2D g, CubeIndex ci, Color c) {
		CubeIndex ci2 = ci.getPosXNeighbour().getPosZNeighbour();
		int[] pos = getPosition(ci2);
		drawHelper.drawRightTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}

	public void drawZyFace(Graphics2D g, CubeIndex ci, Color c) {
		CubeIndex ci2 = ci.getPosYNeighbour().getPosZNeighbour();
		int[] pos = getPosition(ci2);
		drawHelper.drawLeftTriangularFaceWithColour(g, pos[0], pos[1], side, c);
	}
	 */

	public void drawShadedEdges(Graphics2D g, CubeIndex ci, FaceArray faces, int type) {
		int[] types = {type};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdges(Graphics2D g, CubeIndex ci, FaceArray faces, int[] types) {
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdgesFlatInterior(Graphics2D g, CubeIndex ci, FaceArray faces) {
		int[] types = {Face.EDGE_FLAT_INTERIOR};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdgesFlatBoundary(Graphics2D g, CubeIndex ci, FaceArray faces) {
		int[] types = {Face.EDGE_FLAT_BOUNDARY};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdgesBent(Graphics2D g, CubeIndex ci, FaceArray faces) {
		int[] types = {Face.EDGE_BACKWARD, Face.EDGE_FORWARD};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdgesSilhouette(Graphics2D g, CubeIndex ci, FaceArray faces) {
		int[] types = {Face.EDGE_SILHOUETTE, Face.EDGE_IMPOSSIBLE};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawShadedEdgesNull(Graphics2D g, CubeIndex ci, FaceArray faces) {
		int[] types = {Face.EDGE_NULL};
		drawShadedEdgesHelper(g, ci, faces, types);
	}

	public void drawVoronoiCell(Graphics2D g, CubeIndex ci, boolean[] extremes) {
		int[] pos = getPosition(ci);
		drawHelper.drawVoronoiCell(g, pos[0], pos[1], side, extremes);
	}

	public Polygon getVoronoiClippingMask(Graphics2D g, int[] dims, Color background) {
		int[] pos = getPosition(new CubeIndex(0,0,0));
		return drawHelper.getVoronoiClippingMask(g, pos[0], pos[1], dims, background);
	}

	private boolean isEdgeFlat(int edgeType) {
		return edgeType==Face.EDGE_FLAT_BOUNDARY || edgeType==Face.EDGE_FLAT_INTERIOR;
	}

	private int[][] getAllEdgeTypes(Graphics2D g, CubeIndex ci, FaceArray faces) {
		return faces.getAllEdgeTypes(ci, faces);
		/*
		int[] posXType = faces.getPosXEdgeAndFaceType(ci);
		int[] negXType = faces.getNegXEdgeAndFaceType(ci);
		int[] posYType = faces.getPosYEdgeAndFaceType(ci);
		int[] negYType = faces.getNegYEdgeAndFaceType(ci);
		int[] posZType = faces.getPosZEdgeAndFaceType(ci);
		int[] negZType = faces.getNegZEdgeAndFaceType(ci);

		int[][] edgeTypesPrelim = {posXType, negXType, posYType, negYType, posZType, negZType};
		int[][] edgeTypes = fixSilhouetteEdgeProblems(edgeTypesPrelim);
		//int[][] edgeTypes = {posXType, negXType, posYType, negYType, posZType, negZType};
		return edgeTypes;
		 */
	}

	private void drawShadedEdgesHelper(Graphics2D g, CubeIndex ci, FaceArray faces, int[] types) {
		int[][] edgeTypes = getAllEdgeTypes(g, ci, faces);

		int[] posXType = edgeTypes[0];
		int[] negXType = edgeTypes[1];
		int[] posYType = edgeTypes[2];
		int[] negYType = edgeTypes[3];
		int[] posZType = edgeTypes[4];
		int[] negZType = edgeTypes[5];

		CubeIndex ciNegX = ci.getNegXNeighbour();
		CubeIndex ciNegY = ci.getNegYNeighbour();
		CubeIndex ciNegZ = ci.getNegZNeighbour();

		Color cPosX = getEdgeShading(posXType, EdgeDraw.DIR_X, faces.isPosXEdgeSelected(ci));
		Color cNegX = getEdgeShading(negXType, EdgeDraw.DIR_X, faces.isNegXEdgeSelected(ci));
		Color cPosY = getEdgeShading(posYType, EdgeDraw.DIR_Y, faces.isPosYEdgeSelected(ci));
		Color cNegY = getEdgeShading(negYType, EdgeDraw.DIR_Y, faces.isNegYEdgeSelected(ci));
		Color cPosZ = getEdgeShading(posZType, EdgeDraw.DIR_Z, faces.isPosZEdgeSelected(ci));
		Color cNegZ = getEdgeShading(negZType, EdgeDraw.DIR_Z, faces.isNegZEdgeSelected(ci));

		for(int i=0; i<types.length; i++) {
			int type = types[i];
			if(posXType[0]==type) { g.setColor(cPosX); edgeDraw.drawPosXEdge(g, ci);}
			if(negXType[0]==type) { g.setColor(cNegX); edgeDraw.drawPosXEdge(g, ciNegX);}
			if(posYType[0]==type) { g.setColor(cPosY); edgeDraw.drawPosYEdge(g, ci);}
			if(negYType[0]==type) { g.setColor(cNegY); edgeDraw.drawPosYEdge(g, ciNegY);}
			if(posZType[0]==type) { g.setColor(cPosZ); edgeDraw.drawPosZEdge(g, ci);}
			if(negZType[0]==type) { g.setColor(cNegZ); edgeDraw.drawPosZEdge(g, ciNegZ);}
		}
	}

	/*
	private int[] getPosXEdgeType(Graphics2D g, CubeIndex ci, FaceArray faces) {
		Face faceXy = faces.getFaceXy(ci).getTop();
		Face faceXz = faces.getFaceXz(ci).getTop();
		int typeXy = (faces.getFaceXy(ci).isHidden() || faceXy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXy.getType());
		int typeXz = (faces.getFaceXz(ci).isHidden() || faceXz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceXz.getType());
		int edgeType = getXEdgeType(typeXz, typeXy);
		int[] edgeFaceTypes = {edgeType, (typeXy==typeXz) ? typeXy : Face.FACE_NONE};
		return edgeFaceTypes;
	}

	private int[] getPosYEdgeType(Graphics2D g, CubeIndex ci, FaceArray faces) {
		Face faceYx = faces.getFaceYx(ci).getTop();
		Face faceYz = faces.getFaceYz(ci).getTop();
		int typeYx = (faces.getFaceYx(ci).isHidden() || faceYx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYx.getType());
		int typeYz = (faces.getFaceYz(ci).isHidden() || faceYz==null) ? Face.FACE_NONE : Face.getFullFaceType(faceYz.getType());
		int edgeType = getYEdgeType(typeYz, typeYx);
		int[] edgeFaceTypes = {edgeType, (typeYx==typeYz) ? typeYx : Face.FACE_NONE};
		return edgeFaceTypes;
	}

	private int[] getPosZEdgeType(Graphics2D g, CubeIndex ci, FaceArray faces) {
		Face faceZx = faces.getFaceZx(ci).getTop(); 
		Face faceZy = faces.getFaceZy(ci).getTop();
		int typeZx = (faces.getFaceZx(ci).isHidden() || faceZx==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZx.getType());
		int typeZy = (faces.getFaceZy(ci).isHidden() || faceZy==null) ? Face.FACE_NONE : Face.getFullFaceType(faceZy.getType());
		int edgeType = getZEdgeType(typeZx, typeZy);
		int[] edgeFaceTypes = {edgeType, (typeZx==typeZy) ? typeZx : Face.FACE_NONE};
		return edgeFaceTypes;
	}
	 */

	private Color getEdgeShading(int[] edgeFaceTypes, int edgeDir, boolean selected) {
		int edgeType = edgeFaceTypes[0];
		int faceType = edgeFaceTypes[1];
		IsoColours colours = selected ? selectedColours : unselectedColours;
		switch(edgeType) {
		case Face.EDGE_NULL: return backgroundColour;
		case Face.EDGE_FLAT_INTERIOR:
			switch(faceType) {
			case Face.FACE_X: return colours.getFaceX();
			case Face.FACE_Y: return colours.getFaceY();
			case Face.FACE_Z: return colours.getFaceZ();
			}
			break;
		case Face.EDGE_FLAT_BOUNDARY:
			switch(faceType) {
			case Face.FACE_X: return colours.getEdgeX();
			case Face.FACE_Y: return colours.getEdgeY();
			case Face.FACE_Z: return colours.getEdgeZ();
			}
			break;
		case Face.EDGE_SILHOUETTE: return colours.getSilhouette();
		case Face.EDGE_FORWARD: return colours.getForward();
		case Face.EDGE_BACKWARD: return colours.getBackward();
		case Face.EDGE_IMPOSSIBLE: return Color.magenta;
		}
		return new Color(0,0,0,0);
	}

	public int getFillStyle() {
		return fillStyle;
	}

	public void setFillStyle(int fillStyle) {
		this.fillStyle = fillStyle;
	}

	/*
	private int getXEdgeType(int typeAbove, int typeBelow) {
		if(typeAbove==Face.FACE_NONE && typeBelow==Face.FACE_NONE) return EDGE_NULL;
		else if (typeAbove==Face.FACE_NONE || typeBelow==Face.FACE_NONE) return EDGE_SILHOUETTE;
		else if(typeAbove==typeBelow) {
			return (typeAbove==Face.FACE_X) ? EDGE_FLAT_INTERIOR : EDGE_FLAT_BOUNDARY;
		}
		else if(typeAbove==Face.FACE_X || typeBelow==Face.FACE_X) return EDGE_SILHOUETTE;
		else if(typeAbove==Face.FACE_Z && typeBelow==Face.FACE_Y) return EDGE_FORWARD;
		else if(typeAbove==Face.FACE_Y && typeBelow==Face.FACE_Z) return EDGE_BACKWARD;
		else return EDGE_IMPOSSIBLE;
	}

	private int getYEdgeType(int typeAbove, int typeBelow) {
		if(typeAbove==Face.FACE_NONE && typeBelow==Face.FACE_NONE) return EDGE_NULL;
		else if (typeAbove==Face.FACE_NONE || typeBelow==Face.FACE_NONE) return EDGE_SILHOUETTE;
		else if(typeAbove==typeBelow) {
			return (typeAbove==Face.FACE_Y) ? EDGE_FLAT_INTERIOR : EDGE_FLAT_BOUNDARY;
		}
		else if(typeAbove==Face.FACE_Y || typeBelow==Face.FACE_Y) return EDGE_SILHOUETTE;
		else if(typeAbove==Face.FACE_Z && typeBelow==Face.FACE_X) return EDGE_FORWARD;
		else if(typeAbove==Face.FACE_X && typeBelow==Face.FACE_Z) return EDGE_BACKWARD;
		else return EDGE_IMPOSSIBLE;
	}

	private int getZEdgeType(int typeLeft, int typeRight) {
		if(typeLeft==Face.FACE_NONE && typeRight==Face.FACE_NONE) return EDGE_NULL;
		else if (typeLeft==Face.FACE_NONE || typeRight==Face.FACE_NONE) return EDGE_SILHOUETTE;
		else if(typeLeft==typeRight) {
			return (typeLeft==Face.FACE_Z) ? EDGE_FLAT_INTERIOR : EDGE_FLAT_BOUNDARY;
		}
		else if(typeLeft==Face.FACE_Z || typeRight==Face.FACE_Z) return EDGE_SILHOUETTE;
		else if(typeLeft==Face.FACE_X && typeRight==Face.FACE_Y) return EDGE_FORWARD;
		else if(typeLeft==Face.FACE_Y && typeRight==Face.FACE_X) return EDGE_BACKWARD;
		else return EDGE_IMPOSSIBLE;
	}
	 */
}
