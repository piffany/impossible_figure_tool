package util;

import javax.swing.JLabel;

import cubeManip.CubeData;

import cubeManip.CubeIndex;

public class LabelManager {

	public static void updateCursorLabel(JLabel label, int[] p) {
		if(p==null || (p[0]==-1 && p[1]==-1)) label.setText("Cursor = ");
		else label.setText("Cursor = (" + p[0] + ", "+ p[1] + ")");
	}
	
	public static void updateCubeLabel(JLabel label, CubeIndex p) {
		if(p==null) label.setText("Cube = ");
		else label.setText("Cube = (" + p.getX() + ", "+ p.getY() + ", " + p.getZ() + ")");
	}
	
	public static void updateSelectionLabel(JLabel label, CubeData data) {
		if(data==null) return;
		int nSelected = data.getNumSelected();
		int maxSelectable = data.getMaxSelectable();
		String maxSelectString = (maxSelectable==-1) ? "\u221E" : (maxSelectable+"");
		label.setText("Selection = " + nSelected + " / " + maxSelectString);
	}
}
