package util;

import java.awt.Color;
import java.awt.Graphics2D;

import cubeManip.CubeIndex;

public class EdgeDraw {

	private int[] origin = {0,0};
	private int side;//, height;
	public int[] vx = new int[2] , vy = new int[2], vz = new int[2];
	public double[] mx = new double[2] , my = new double[2], mz = new double[2];
	public static int DIR_X = 0, DIR_Y = 1, DIR_Z = 2;
	DrawHelper drawHelper;

	public EdgeDraw(int x, int y, int side, double[] mx, double[] my, double[] mz, DrawHelper drawHelper) {
		origin[0] = x;
		origin[1] = y;
		this.side = side;
		//this.height = DrawHelper.getHeight(side);
		this.mx[0] = mx[0]; this.mx[1] = mx[1];
		this.my[0] = mx[0]; this.my[1] = my[1];
		this.mz[0] = mx[0]; this.mz[1] = mz[1];
		double totalX = -mx[0]+my[0];
		double totalY = mx[1]+my[1]-mz[1];
		double hypotenuse = -mz[1];
		vx[0] = (int) Math.round(mx[0]/hypotenuse*(double)side);
		vx[1] = (int) Math.round(mx[1]/hypotenuse*(double)side);
		vy[0] = (int) Math.round(my[0]/hypotenuse*(double)side);
		vy[1] = (int) Math.round(my[1]/hypotenuse*(double)side);
		vz[0] = (int) Math.round(mz[0]/hypotenuse*(double)side);
		vz[1] = (int) Math.round(mz[1]/hypotenuse*(double)side);
		this.drawHelper = drawHelper;
		/*
		vx[0] = -2*side; vx[1] = side;
		vy[0] = 2*side; vy[1] = side;
		vz[0] = 0; vz[1] = -height;
		*/
	}
	
	public int[] getPosition(CubeIndex ci) {
		int i = ci.getX(), j = ci.getY(), k = ci.getZ();
		int x = origin[0];
		int y = origin[1];
		/*
		x -= i*(2*side);
		y += i*(side);
		x += j*(2*side);
		y += j*(side);
		y -= k*(height);
		*/
		//System.out.println("mx : " + mx[0] + ", " + mx[1]);
		//System.out.println("my : " + my[0] + ", " + my[1]);
		//System.out.println("mz : " + mz[0] + ", " + mz[1]);
		
		if(i==0) {
			x += j*vy[0] + k*vz[0];
			y += j*vy[1] + k*vz[1];
		}
		else if(j==0) {
			x += i*vx[0] + k*vz[0];
			y += i*vx[1] + k*vz[1];
		}
		else if(k==0) {
			x += i*vx[0] + j*vy[0];
			y += i*vx[1] + j*vy[1];
		}
		int[] pos = {x,y};
		return pos;
	}
	
	public void drawPosXEdge(Graphics2D g, int[] pos) {
		drawHelper.drawXEdge(g, pos[0], pos[1], 1);
	}
	
	public void drawPosXEdge(Graphics2D g, CubeIndex ci) {
		drawPosXEdge(g, getPosition(ci));
	}
	
	public void drawNegXEdge(Graphics2D g, int[] pos) {
		drawHelper.drawXEdge(g, pos[0], pos[1], -1);
	}
	
	public void drawNegXEdge(Graphics2D g, CubeIndex ci) {
		drawNegXEdge(g, getPosition(ci));
	}
	
	public void drawPosYEdge(Graphics2D g, int[] pos) {
		drawHelper.drawYEdge(g, pos[0], pos[1], 1);
	}
	
	public void drawPosYEdge(Graphics2D g, CubeIndex ci) {
		drawPosYEdge(g, getPosition(ci));
	}
	
	public void drawNegYEdge(Graphics2D g, int[] pos) {
		drawHelper.drawYEdge(g, pos[0], pos[1], -1);
	}
	
	public void drawNegYEdge(Graphics2D g, CubeIndex ci) {
		drawNegYEdge(g, getPosition(ci));
	}
	
	public void drawPosZEdge(Graphics2D g, int[] pos) {
		drawHelper.drawZEdge(g, pos[0], pos[1], 1);
	}
	
	public void drawPosZEdge(Graphics2D g, CubeIndex ci) {
		drawPosZEdge(g, getPosition(ci));
	}
	
	public void drawNegZEdge(Graphics2D g, int[] pos) {
		drawHelper.drawZEdge(g, pos[0], pos[1], -1);
	}
	
	public void drawNegZEdge(Graphics2D g, CubeIndex ci) {
		drawNegZEdge(g, getPosition(ci));
	}
	
	public void drawPosEdges(Graphics2D g, CubeIndex ci) {
		drawPosEdges(g, getPosition(ci));
	}
	
	public void drawOuterEdges(Graphics2D g, CubeIndex ci) {
		int[] posXY = getPosition(ci.getPosXNeighbour().getPosYNeighbour());
		drawNegXEdge(g, posXY); drawNegYEdge(g, posXY);
		int[] posXZ = getPosition(ci.getPosXNeighbour().getPosZNeighbour());
		drawNegXEdge(g, posXZ); drawNegZEdge(g, posXZ);
		int[] posYZ = getPosition(ci.getPosYNeighbour().getPosZNeighbour());
		drawNegYEdge(g, posYZ); drawNegZEdge(g, posYZ);
	}
	
	public void drawInnerEdges(Graphics2D g, CubeIndex ci) {
		int[] pos = getPosition(ci);
		drawNegXEdge(g, pos); drawNegYEdge(g, pos); drawNegZEdge(g, pos);
	}
	
	public void drawPosEdges(Graphics2D g, int[] pos) {
		drawPosXEdge(g, pos);
		drawPosYEdge(g, pos);
		drawPosZEdge(g, pos);
	}
	
	public void drawNegEdges(Graphics2D g, CubeIndex ci) {
		drawNegEdges(g, getPosition(ci));
	}

	public void drawNegEdges(Graphics2D g, int[] pos) {
		drawNegXEdge(g, pos);
		drawNegYEdge(g, pos);
		drawNegZEdge(g, pos);
	}
	
	public void drawAllEdges(Graphics2D g, int[] pos) {
		drawPosEdges(g, pos); drawNegEdges(g, pos);
	}
	
	public void drawAllEdges(Graphics2D g, CubeIndex ci) {
		drawAllEdges(g, getPosition(ci));
	}

	public void drawLabeledAxes(Graphics2D g, int[] pos, int[] offset, Color backgroundColour, Color lineColour) {
		g.translate(offset[0], offset[1]);
		g.setColor(backgroundColour);
		g.fillRect(0, 0, 2*pos[0], 2*pos[1]);
		g.setColor(lineColour);
		drawPosXEdge(g, pos);
		drawPosYEdge(g, pos);
		drawPosZEdge(g, pos);
		drawNegXEdge(g, pos);
		drawNegYEdge(g, pos);
		drawNegZEdge(g, pos);
		int dx = -8, dy = 5;
		g.drawString("+ x", pos[0]+vx[0]*14/10+dx, pos[1]+vx[1]*14/10+dy);
		g.drawString("+ y", pos[0]+vy[0]*14/10+dx, pos[1]+vy[1]*14/10+dy);
		g.drawString("+ z", pos[0]+vz[0]*14/10+dx, pos[1]+vz[1]*14/10+dy);
		g.drawString("- x", pos[0]-vx[0]*14/10+dx, pos[1]-vx[1]*14/10+dy);
		g.drawString("- y", pos[0]-vy[0]*14/10+dx, pos[1]-vy[1]*14/10+dy);
		g.drawString("- z", pos[0]-vz[0]*14/10+dx, pos[1]-vz[1]*14/10+dy);
		g.translate(-offset[0], -offset[1]);
	}

}
