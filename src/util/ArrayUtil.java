package util;

public class ArrayUtil {

	
	public static double matrixSum(double[][] A) {
		int w = A.length, h = (w==0) ? 0 : A[0].length;
		double sum = 0;
		for(int i=0; i<w; i++) {
			for(int j=0; j<h; j++) {
				sum += A[i][j];
			}
		}
		return sum;
	}
	
	public static double[][] matrixProduct(double[][] A, double[][] B) {
		int w1 = A.length, h1 = (w1==0) ? 0 : A[0].length;
		int w2 = B.length, h2 = (w2==0) ? 0 : B[0].length;
		assert(h1==w2);
		double[][] AB = new double[w1][h2];
		for(int i=0; i<w1; i++) {
			for(int j=0; j<h2; j++) {
				double sum = 0;
				for(int k=0; k<h1; k++) {
					sum += A[i][k]*B[k][j];
				}
				AB[i][j] = sum;
			}
		}
		return AB;
	}
	
	public static double[][] componentWiseProduct(double[][] A, double[][] B) {
		int w1 = A.length, h1 = (w1==0) ? 0 : A[0].length;
		int w2 = B.length, h2 = (w2==0) ? 0 : B[0].length;
		assert(w1==w2 && h1==h2);
		double[][] AB = new double[w1][h1];
		for(int i=0; i<w1; i++) {
			for(int j=0; j<h1; j++) {
				AB[i][j] = A[i][j]*B[i][j];
			}
		}
		return AB;
	}
	
	public static double[][] getSubArray(double[][] A, int iMin, int iMax, int jMin, int jMax) {
		int w = A.length, h = (w==0) ? 0 : A[0].length;
		double[][] B = new double[iMax-iMin+1][jMax-jMin+1];
		for(int i=iMin; i<=iMax; i++) {
			for(int j=jMin; j<=jMax; j++) {
				B[i-iMin][j-jMin] = A[i][j];
			}
		}
		return B;
	}
	
	public static void addArray(int[][] M1, int[][] M2) {
		int w2 = M2.length;
		int h2 = (w2==0) ? 0 : M2[0].length;
		for(int i=0; i<w2; i++) {
			for(int j=0; j<h2; j++) {
				M1[i][j] += M2[i][j];
			}
		}		
	}

	public static int[][] transform(int[][] M, boolean transpose, boolean reflectX, boolean reflectY) {
		int[][] M2 = transpose ? transpose(M) : copyArray(M);
		int[][] M3 = reflectX ? reflectX(M2) : copyArray(M2);
		int[][] M4 = reflectY ? reflectX(M3) : copyArray(M3);
		return M4;
	}
	
	public static int[][] copyArray(int[][] M) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		int[][] newM = new int[w][h];
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				newM[x][y] = M[x][y];
			}
		}
		return newM;
	}
	
	public static int[][] transpose(int[][] M) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		int[][] newM = new int[h][w];
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				newM[y][x] = M[x][y];
			}
		}
		return newM;
	}
	
	public static int[][] reflectX(int[][] M) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		int[][] newM = new int[w][h];
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				newM[x][y] = M[w-1-x][y];
			}
		}
		return newM;
	}
	
	public static int[][] reflectY(int[][] M) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		int[][] newM = new int[w][h];
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				newM[x][y] = M[x][h-1-y];
			}
		}
		return newM;
	}
	
	public static void printArray(int[][] M, String s, int x1, int x2, int y1, int y2) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		System.out.println(s);
		for(int x=Math.max(0, x1); x<=Math.min(w, x2); x++) {
			for(int y=Math.max(0, y1); y<=Math.min(h, y2); y++) {
				System.out.print(M[x][y] + " ");
			}
			System.out.println();
		}
	}
	
	public static void printArray(boolean[][] M, String s, int x1, int x2, int y1, int y2) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		System.out.println(s);
		for(int x=Math.max(0, x1); x<=Math.min(w, x2); x++) {
			for(int y=Math.max(0, y1); y<=Math.min(h, y2); y++) {
				System.out.print((M[x][y]?1:0) + " ");
			}
			System.out.println();
		}
	}
	
	public static void addIntArrayToBooleanArray(boolean[][] M1, int[][] M2, int x, int y) {
		int w1 = M1.length;
		int h1 = (w1==0) ? 0 : M1[0].length;
		int w2 = M2.length;
		int h2 = (w2==0) ? 0 : M2[0].length;
		for(int i=0; i<w2; i++) {
			if(x+i>=w1) break;
			for(int j=0; j<h2; j++) {
				if(y+j>=h1) break;
				if(M2[i][j]>0) M1[x+i][y+j] = true;
			}
		}		
	}
	
	public static void printArray(int[][] M, String s) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		System.out.println(s);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				System.out.print(M[x][y] + " ");
			}
			System.out.println();
		}
	}
	
	public static void printArrayTranspose(int[][] M, String s) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		System.out.println(s);
		for(int y=0; y<h; y++) {
			for(int x=0; x<w; x++) {
				System.out.print(M[x][y] + " ");
			}
			System.out.println();
		}
	}

	public static void printArray(boolean[][] M, String s) {
		int w = M.length;
		int h = (w==0) ? 0 : M[0].length;
		System.out.println(s);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				System.out.print((M[x][y]?1:0) + " ");
			}
			System.out.println();
		}
	}
}
