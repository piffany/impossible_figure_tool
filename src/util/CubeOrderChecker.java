package util;

import cubeManip.CubeIndex;

public class CubeOrderChecker {

	private FaceArray faces;

	public CubeOrderChecker(FaceArray faces) {
		this.faces = faces;
	}

	public void checkCubeOrderingWithNeighbours(CubeIndex ci) {
		checkCubeOrdering(ci, ci.getPosXNeighbour());
		checkCubeOrdering(ci, ci.getPosYNeighbour());
		checkCubeOrdering(ci, ci.getPosZNeighbour());
		checkCubeOrdering(ci, ci.getNegXNeighbour());
		checkCubeOrdering(ci, ci.getNegYNeighbour());
		checkCubeOrdering(ci, ci.getNegZNeighbour());
	}

	private void checkCubeOrdering(CubeIndex ci1, CubeIndex ci2) {
		LayeredFace[] sharedFaces = faces.getSharedFaces(ci1, ci2);
		if(sharedFaces.length!=2) return;
		else {
			setEdgesToOcclude(ci1, ci2, isInFrontOf(ci1, ci2), isDirectlyInFrontOf(ci1, ci2));
			setEdgesToOcclude(ci2, ci1, isInFrontOf(ci2, ci1), isDirectlyInFrontOf(ci2, ci1));		}
	}

	// make ci1 occlude ci2 if not already
	// i.e. if there's one silhouette, then also make the other a silhouette edge
	private void setEdgesToOcclude(CubeIndex ci1, CubeIndex ci2, boolean inFront, boolean directlyInFront) {
		if(inFront & !directlyInFront) {
			if(ci1.equals(ci2.getPosXNeighbour())) {
				// check negY edge of ci2
				if(faces.getFaceXz(ci2).getTop().belongsTo(ci1) && faces.getFaceZx(ci2).getTop().belongsTo(ci2)) {
					faces.setNegYEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check negZ edge of ci2
				if(faces.getFaceXy(ci2).getTop().belongsTo(ci1) && faces.getFaceYx(ci2).getTop().belongsTo(ci2)) {
					faces.setNegZEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getNegYEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegZEdgeAndFaceType(ci2)[0])) {
				faces.setNegZEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
			if(faces.getNegZEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegYEdgeAndFaceType(ci2)[0])) {
				faces.setNegYEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
			else if(ci1.equals(ci2.getPosYNeighbour())) {
				// check negX edge of ci2
				if(faces.getFaceYz(ci2).getTop().belongsTo(ci1) && faces.getFaceZy(ci2).getTop().belongsTo(ci2)) {
					faces.setNegXEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check negZ edge of ci2
				if(faces.getFaceYx(ci2).getTop().belongsTo(ci1) && faces.getFaceXy(ci2).getTop().belongsTo(ci2)) {
					faces.setNegZEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getNegXEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegZEdgeAndFaceType(ci2)[0])) {
				faces.setNegZEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
			if(faces.getNegZEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegXEdgeAndFaceType(ci2)[0])) {
				faces.setNegXEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
			else if(ci1.equals(ci2.getPosZNeighbour())) {
				// check negX edge of ci2
				if(faces.getFaceZy(ci2).getTop().belongsTo(ci1) && faces.getFaceYz(ci2).getTop().belongsTo(ci2)) {
					faces.setNegXEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check negY edge of ci2
				if(faces.getFaceZx(ci2).getTop().belongsTo(ci1) && faces.getFaceXz(ci2).getTop().belongsTo(ci2)) {
					faces.setNegYEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getNegYEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegXEdgeAndFaceType(ci2)[0])) {
				faces.setNegXEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
			if(faces.getNegXEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getNegYEdgeAndFaceType(ci2)[0])) {
				faces.setNegYEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
		}
		if(inFront) {
			if(ci1.equals(ci2.getNegXNeighbour())) {
				// check posY edge of ci2
				if(faces.getFaceYz(ci2).getTop().belongsTo(ci1) && faces.getFaceYx(ci2).getTop().belongsTo(ci2)) {
					faces.setPosYEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check posZ edge of ci2
				if(faces.getFaceZy(ci2).getTop().belongsTo(ci1) && faces.getFaceZx(ci2).getTop().belongsTo(ci2)) {
					faces.setPosZEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getPosYEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosZEdgeAndFaceType(ci2)[0])) {
				faces.setPosZEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
			if(faces.getPosZEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosYEdgeAndFaceType(ci2)[0])) {
				faces.setPosYEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
			else if(ci1.equals(ci2.getNegYNeighbour())) {
				// check posX edge of ci2
				if(faces.getFaceXz(ci2).getTop().belongsTo(ci1) && faces.getFaceXy(ci2).getTop().belongsTo(ci2)) {
					faces.setPosXEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check posZ edge of ci2
				if(faces.getFaceZx(ci2).getTop().belongsTo(ci1) && faces.getFaceZy(ci2).getTop().belongsTo(ci2)) {
					faces.setPosZEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getPosXEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosZEdgeAndFaceType(ci2)[0])) {
				faces.setPosZEdgeType(ci2, Face.EDGE_SILHOUETTE);
				System.out.println(ci1 + ", " + ci2);
			}
			if(faces.getPosZEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosXEdgeAndFaceType(ci2)[0])) {
				faces.setPosXEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
			else if(ci1.equals(ci2.getNegZNeighbour())) {
				// check posX edge of ci2
				if(faces.getFaceXy(ci2).getTop().belongsTo(ci1) && faces.getFaceXz(ci2).getTop().belongsTo(ci2)) {
					faces.setPosXEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				// check posY edge of ci2
				if(faces.getFaceYx(ci2).getTop().belongsTo(ci1) && faces.getFaceYz(ci2).getTop().belongsTo(ci2)) {
					faces.setPosYEdgeType(ci2, Face.EDGE_SILHOUETTE);
				}
				/*
			if(faces.getPosYEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosXEdgeAndFaceType(ci2)[0])) {
				faces.setPosXEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
			if(faces.getPosXEdgeAndFaceType(ci2)[0] == Face.EDGE_SILHOUETTE && edgeIsAmbiguous(faces.getPosYEdgeAndFaceType(ci2)[0])) {
				faces.setPosYEdgeType(ci2, Face.EDGE_SILHOUETTE);
			}
				 */
			}
		}
	}

	private boolean edgeIsAmbiguous(int type) {
		return (type == Face.EDGE_FLAT_BOUNDARY);// || (type == Face.EDGE_BACKWARD);
	}

	// true if ci1 and ci2 are neighbours AND if ci1 in front of ci2
	// false if ci2 is hidden
	// determined by looking at the two shared faces
	private boolean isInFrontOf(CubeIndex ci1, CubeIndex ci2) {
		LayeredFace[] sharedFaces = faces.getSharedFaces(ci1, ci2);
		if(sharedFaces.length!=2) return false;
		else {
			boolean inFront1 = sharedFaces[0].isInFrontOf(ci1, ci2);
			boolean inFront2 = sharedFaces[1].isInFrontOf(ci1, ci2);
			return inFront1 && inFront2;
		}
	}

	// true if ci1 and ci2 are neighbours AND if ci1 in front of ci2
	// false if ci2 is hidden
	// determined by looking at the two shared faces
	private boolean isDirectlyInFrontOf(CubeIndex ci1, CubeIndex ci2) {
		LayeredFace[] sharedFaces = faces.getSharedFaces(ci1, ci2);
		if(sharedFaces.length!=2) return false;
		else {
			boolean inFront1 = sharedFaces[0].isDirectlyInFrontOf(ci1, ci2);
			boolean inFront2 = sharedFaces[1].isDirectlyInFrontOf(ci1, ci2);
			return inFront1 && inFront2;
		}
	}
}
